<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homepage.homepage');
});

Route::get('/service', function () {
    return view('service.service');
});

Route::get('/gallery', function () {
    return view('gallery.gallery');
});

Route::get('/store', function () {
    return view('store.store');
});

Route::get('/blogs', function () {
    return view('blog.blog');
});

Route::get('/contact-us', function () {
    return view('contact-us.contact-us');
});

Route::get('/product-category/ao-dai', function () {
    return view('product-category.ao-dai.aodai');
});

Route::get('/product-category/my-pham', function () {
    return view('product-category.my-pham.mypham');
});

Route::get('/cart', function () {
    return view('cart.cart');
});