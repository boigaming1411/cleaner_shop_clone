<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\HomePageController;
use App\Http\Controllers\Postcontroller;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\SettingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/posts-all',[Postcontroller::class,'index']);
Route::get('/post-detail/{post_id}',[Postcontroller::class,'show']);
Route::get('/product-all',[ProductController::class,'index']);
Route::get('/product-detail/{id}',[ProductController::class,'show']);
Route::get('/category-all',[CategoryController::class,'index']);
Route::get('/get-product-by-category-name/{name}',[CategoryController::class,'getProductByCategoryName']);
Route::get('/get-product-by-cate-id/{id}',[CategoryController::class,'getProductByCategoryID']);
Route::get('/get-setting-of-page',[SettingController::class,'index']);
Route::get('/get_list_service',[ServiceController::class,'index2']);
Route::get('/get_gallery_data',[GalleryController::class,'index']);
Route::get('/get_product_on_page',[ProductController::class,'getProductOnPage']);
Route::get('/get_related_product/{id}',[ProductController::class,'get_Related_Product']);
Route::get('/get_search_product',[ProductController::class,'search_product']);



