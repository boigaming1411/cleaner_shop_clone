<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    private string $url_host = "https://staging.dndcleaners.ca/";

    private $number_not_negative_regex = '/[0-9]+/';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url = $this->url_host."wp-json/wc/v3/products/categories";
        $data = $this->getApiWoocomere($url);
        return  json_decode($data);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function getProductByCategoryName(Request $request)
    {


        $name = $request->name;
        if (!$this->check_category_name_is_exist($name)){
                return response()->json(['message' => 'Category Name not Found '], 404);
        }

        $url_get_theo_ten = "https://staging.dndcleaners.ca/wp-json/wc/v3/products/categories?search=".$name."&per_page=1";
        $data_theo_ten = json_decode($this->getApiWoocomere($url_get_theo_ten));
        $id_category = $data_theo_ten[0]->id;
        $url_product = "https://staging.dndcleaners.ca/wp-json/wc/v3/products?category=".$id_category;
        $data_product = $this->getApiWoocomere($url_product);
        return json_decode($data_product);

    }

    public function getProductByCategoryID(Request $request)
    {
        $regex = "/[0-9]+/";
        if (!preg_match($regex,$request->id)){
            return response()->json(['message' => 'Parameter ID Must is a Number'], 400);
        }
        $id = $request->id;
        if($this->check_category_is_exist($id)){
            return response()->json(['message' => 'Category ID not found'], 404);
        }



        $url_product = "https://staging.dndcleaners.ca/wp-json/wc/v3/products?category=".$id;
        $data_product = $this->getApiWoocomere($url_product);
        return json_decode($data_product);
    }

    private function  check_category_name_is_exist($category_name):bool
    {
        $url = $this->url_host."wp-json/wc/v3/products/categories";
        $list_category = json_decode($this->getApiWoocomere($url));
        $check = false;
        foreach ($list_category as $item){
           if ($item->name == $category_name){
               $check = true;
               break;
           }
        }
        return  $check;


    }





    private function  check_category_is_exist($id_product){
        $url = "https://www.staging.balooworld.ca/wp-json/wc/v3/products/categories/".$id_product;
        $consumer_key = 'ck_3aacf618ef5a2e9c70aa2608c02740489ee49ad0';
        $consumer_secret = 'cs_b204adf2a666763de8ec813e97580dd189bdc956';

        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key.':'.$consumer_secret )
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
        $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);

        if ($status_code == 200){
            return true;
        }
        else{
            return  false;
        }

    }





    // This Api Will Call For Some Api Require Auth
    private function authGetApi($url, $method = 'GET')
    {
        $consumer_key = 'boigaming1411@gmail.com';
        $consumer_secret = 'PJgn pX16 PvMG Kuk8 hUvV xd5U';

        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key.':'.$consumer_secret )
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
//       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        return $resp;


    }

    private  function getApi($url, $method = 'GET'){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $resp = curl_exec($curl);
        curl_close($curl);
        return $resp;
    }


    private function getApiWoocomere($url, $method = 'GET'){



        $consumer_key = 'ck_423afcd53f7ab7a1833ab30c2c3b216caac669a5';
        $consumer_secret = 'cs_f4140eca526b2a28f5937d929595ac36ec6b3cfa';

        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key.':'.$consumer_secret )
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
//       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        return $resp;
    }




}
