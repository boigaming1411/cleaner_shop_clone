<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Postcontroller extends Controller
{
    private string $url_host = "https://staging.dndcleaners.ca/";
    private $number_not_negative_regex = '/[0-9]+/';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $url = $this->url_host."wp-json/wp/v2/posts";
        return json_decode($this->getApi($url));
    }

    public function show($post_id)
    {
        if(!preg_match($this->number_not_negative_regex,$post_id)){
            return response()->json(['message' => 'post id is must be a number'], 400);
        }
        if (!$this->check_post_is_exist($post_id)){
            return response()->json(['message' => 'post id is not found'], 404);
        }

        $url = $this->url_host."wp-json/wp/v2/posts/".$post_id;

         return json_decode($this->getApi($url));
    }

    private function  check_post_is_exist($id_post){
        $url = "https://staging.dndcleaners.ca/wp-json/wp/v2/posts/".$id_post;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ($status_code == 200){
            return true;
        }
        else{
            return  false;
        }

    }








    // This Api Will Call For Some Api Require Auth
    private function authGetApi($url, $method = 'GET')
    {
        $consumer_key = 'boigaming1411@gmail.com';
        $consumer_secret = 'PJgn pX16 PvMG Kuk8 hUvV xd5U';

        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key.':'.$consumer_secret )
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
//       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        return $resp;


    }

    private  function getApi($url, $method = 'GET'){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $resp = curl_exec($curl);
        curl_close($curl);
        return $resp;
    }


    private function getApiWoocomere($url, $method = 'GET'){



        $consumer_key = 'ck_d735ddb5634e7c46d1289ffb23d89b4ca422aee1';
        $consumer_secret = 'cs_d0ad191bc1c05169cd8d78bf014a14400fa4fbe7';

        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key.':'.$consumer_secret )
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
//       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        return $resp;
    }

}
