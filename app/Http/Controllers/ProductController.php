<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    private string $url_host = "https://staging.dndcleaners.ca/";
    private $number_not_negative_regex = '/[0-9]+/';


    public function search_product(Request $request)
    {
        $query = "";
        if (!is_null($request->s)){
            $query.= "?per_page=100&search=".$request->s;
        }elseif (empty($request->s)){
            $query.= "?per_page=100&search=";
        }
//        $orderby =  $request->orderby;
//        $order = 'desc';
//        $arr_order_result_check = ['do_lien_quan','pho_bien','moi_nhat','gia_thap_den_cao','gia_cao_den_thap'];
//       if (empty($orderby)){
//           dd("temp");
//       }
//       else if (!in_array($orderby,$arr_order_result_check)){
//            return response()->json(['message' => 'orderby parameter is not avaiable'], 404);
//        }
//        else{
//            if ($orderby == $arr_order_result_check[0]){
//                $orderby =   "price";
//                $order = 'asc';
//
//            }
//            elseif($orderby == $arr_order_result_check[1]){
//                $orderby =   "popularity";
//            }
//            elseif ($orderby == $arr_order_result_check[2]){
//                $orderby =   "id";
//            }
//            elseif ($orderby == $arr_order_result_check[3]){
//                $orderby =   "price";
//                $order = 'asc';
//                // Thấp đến cao
//
//            }
//            elseif ($orderby == $arr_order_result_check[4]){
//                $orderby =   "price";
//                $order = 'desc';
//                // Cao đến thấp
//            }
//
//        }
//




        $url = 'https://staging.dndcleaners.ca/wp-json/wc/v3/products'.$query;

        $data = json_decode($this->getApiWoocomere($url));
        return $data;



    }

    public function getProductOnPage(Request $request)
    {
        if (!preg_match($this->number_not_negative_regex,$request->page)){
            return response()->json(['message' => 'page id must be is a number'], 400);
        }

        $name = ($request->category_name) ? $request->category_name : "áo dài";
        $page = ($request->page) ? $request->page : 1;
        $perpage = 9;
        $orderby =  $request->orderby;
        $order = 'desc';
        $arr_order_result_check = ['mac_dinh','pho_bien','moi_nhat','gia_thap_den_cao','gia_cao_den_thap'];
        if (!in_array($orderby,$arr_order_result_check)){
            return response()->json(['message' => 'orderby parameter is not avaiable'], 404);
        }
        else{
            if ($orderby == $arr_order_result_check[0]){
                $orderby =   "price";
                $order = 'asc';

            }
            elseif($orderby == $arr_order_result_check[1]){
                $orderby =   "popularity";
            }
            elseif ($orderby == $arr_order_result_check[2]){
                $orderby =   "id";
            }
            elseif ($orderby == $arr_order_result_check[3]){
                $orderby =   "price";
                $order = 'asc';
                // Thấp đến cao

            }
            elseif ($orderby == $arr_order_result_check[4]){
                $orderby =   "price";
                $order = 'desc';
                // Cao đến thấp
            }

        }

        $url_get_theo_ten = "https://staging.dndcleaners.ca/wp-json/wc/v3/products/categories?search=".$name."&per_page=1";
        $data_theo_ten = json_decode($this->getApiWoocomere($url_get_theo_ten));
        $id_category = $data_theo_ten[0]->id;
        $url_product = "https://staging.dndcleaners.ca/wp-json/wc/v3/products?&order=".$order."&page=".$page."&per_page=".$perpage."&orderby=".$orderby."&category=".$id_category;
        $data_product = $this->getApiWoocomere($url_product);
        return (json_decode($data_product));

    }

    public function get_Related_Product(Request $request)
    {
        if (!preg_match($this->number_not_negative_regex,$request->id)){
            return response()->json(['message' => 'Product id must be a number'], 400);
        }

        if (!$this->check_id_product_is_exist($request->id)){
            return response()->json(['message' => 'Product not found'], 404);
        }

        $id = $request->id;



        $url = $this->url_host."wp-json/wc/v3/products/".$id;
        $product = json_decode($this->getApiWoocomere($url));
        $data_related = $product->related_ids;

       $temp = array_rand($data_related,3);
        for ($j = 0 ; $j<count($temp);$j++){
            $list_related[] = $data_related[$temp[$j]];
        }



        $str_query = '';
        for($i = 0; $i<count($list_related)-1;$i++) {
            if ($i+1 == count($list_related)){
                $str_query.= $list_related[$i];
            }
            else{
                $str_query.= $list_related[$i].",";
            }
        }

        $url_related = $this->url_host."wp-json/wc/v3/products?include=".$str_query;
        $related = json_decode($this->getApiWoocomere($url_related));
        return $related;


    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url = $this->url_host."wp-json/wc/v3/products";
        $data = json_decode($this->getApiWoocomere($url));
        $url_2 = $this->url_host."wp-json/wc/v3/products?category=145";
        $data_2 = json_decode($this->getApiWoocomere($url_2));
        foreach ($data_2 as  $value){
            $data[] = $value;
        }
        return  $data;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response
     */
    public function show($id)
    {
//        https://staging.dndcleaners.ca/wp-json/wc/v3/products/1805

        if (!preg_match($this->number_not_negative_regex,$id)){
            return response()->json(['message' => 'Product Id Must be a Number '], 400);
        }
        if (!$this->check_id_product_is_exist($id)){
            return response()->json(['message' => 'Not found your product '], 400);
        }


        $url = $this->url_host."wp-json/wc/v3/products/".$id;
        $data = $this->getApiWoocomere($url);
        return  json_decode($data);


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    private function  check_id_product_is_exist($id_product){
    $url = "https://www.staging.dndcleaners.ca/wp-json/wc/v3/products/".$id_product;

        $consumer_key = 'ck_423afcd53f7ab7a1833ab30c2c3b216caac669a5';
        $consumer_secret = 'cs_f4140eca526b2a28f5937d929595ac36ec6b3cfa';

        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key.':'.$consumer_secret )
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);


    if ($status_code == 200){
        return true;
    }
    else{
        return  false;
    }

        }



    // This Api Will Call For Some Api Require Auth
    private function authGetApi($url, $method = 'GET')
    {
        $consumer_key = 'boigaming1411@gmail.com';
        $consumer_secret = 'PJgn pX16 PvMG Kuk8 hUvV xd5U';

        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key.':'.$consumer_secret )
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
//       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        return $resp;


    }

    private  function getApi($url, $method = 'GET'){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $resp = curl_exec($curl);
        curl_close($curl);
        return $resp;
    }


    private function getApiWoocomere($url, $method = 'GET'){



        $consumer_key = 'ck_423afcd53f7ab7a1833ab30c2c3b216caac669a5';
        $consumer_secret = 'cs_f4140eca526b2a28f5937d929595ac36ec6b3cfa';

        $headers = array(
            'Authorization' => 'Basic ' . base64_encode($consumer_key.':'.$consumer_secret )
        );


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, "$consumer_key:$consumer_secret");
        $resp = curl_exec($curl);
//       $status_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        curl_close($curl);
        return $resp;
    }


}
