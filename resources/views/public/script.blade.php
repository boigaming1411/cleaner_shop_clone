<!-- same -->
        <script>
            $(".wpcf7-form").on("submit", function () {
                var get_name = $(".summary > .product_title.entry-title").text();
                console.log($(".summary > .product_title.entry-title").text());
                $("[id*='dat-hang-truoc'] input[name=product-name]").val(get_name);
            });

            //Tự động click khi load xong web
            $(window).ready(function () {
                setTimeout(function () {
                    $('html[lang="en-US"] #tab-title-test_tab_en > a').click();
                }, 800);
            });
        </script>
        <div
            id="pum-876"
            class="pum pum-overlay pum-theme-867 pum-theme-default-theme popmake-overlay pum-click-to-close auto_open click_open"
            data-popmake='{"id":876,"slug":"popup-ngon-ngu-khi-load-trang","theme_id":867,"cookies":[{"event":"on_popup_close","settings":{"name":"pum-876","time":"30 minutes","path":true}}],"triggers":[{"type":"auto_open","settings":{"cookie_name":["pum-876"],"delay":"500"}},{"type":"click_open","settings":{"extra_selectors":"","cookie_name":null}}],"mobile_disabled":null,"tablet_disabled":null,"meta":{"display":{"stackable":false,"overlay_disabled":false,"scrollable_content":false,"disable_reposition":false,"size":"normal","responsive_min_width":"0%","responsive_min_width_unit":false,"responsive_max_width":"100%","responsive_max_width_unit":false,"custom_width":"640px","custom_width_unit":false,"custom_height":"380px","custom_height_unit":false,"custom_height_auto":false,"location":"center top","position_from_trigger":false,"position_top":"100","position_left":"0","position_bottom":"0","position_right":"0","position_fixed":false,"animation_type":"fade","animation_speed":"350","animation_origin":"center top","overlay_zindex":false,"zindex":"1999999999"},"close":{"text":"fa fa-times","button_delay":"0","overlay_click":"1","esc_press":false,"f4_press":false},"click_open":[]}}'
            role="dialog"
            aria-hidden="true"
            aria-labelledby="pum_popup_title_876"
        >
            <div
                id="popmake-876"
                class="pum-container popmake theme-867 pum-responsive pum-responsive-normal responsive size-normal"
            >
                <div id="pum_popup_title_876" class="pum-title popmake-title">
                    VUI LÒNG CHỌN NGÔN NGỮ/PLEASE CHOOSE A LANGUAGE
                </div>

                <div class="pum-content popmake-content" tabindex="0">
                    <p style="display: flex; justify-content: center">
                        <a
                            lang="vi"
                            style="margin: 10px; width: 100px; height: 100px; text-align: center"
                            href="/"
                            hreflang="vi"
                            ><img
                                loading="lazy"
                                class="alignnone size-medium wp-image-885"
                                src="/wp-content/uploads/2021/11/flag-vietnam-300x300.png"
                                alt=""
                                width="300"
                                height="300"
                            /><br />
                            Tiếng Việt</a
                        ><br />
                        <a
                            lang="en-US"
                            style="margin: 10px; width: 100px; height: 100px; text-align: center"
                            href="/en/homepage/"
                            hreflang="en-US"
                            ><img
                                loading="lazy"
                                class="alignnone size-medium wp-image-884"
                                src="/wp-content/uploads/2021/11/flag-USA-300x300.png"
                                alt=""
                                width="300"
                                height="300"
                            /><br />
                            <span style="vertical-align: inherit"
                                ><span style="vertical-align: inherit">English</span></span
                            ></a
                        >
                    </p>
                </div>

                <button type="button" class="pum-close popmake-close" aria-label="Đóng">
                    <i class="fa fa-times"></i>
                </button>
            </div>
        </div>
        <!-- same -->

         <script type="text/javascript">
            (function () {
                var c = document.body.className;
                c = c.replace(/woocommerce-no-js/, "woocommerce-js");
                document.body.className = c;
            })();
        </script>
        <script type="text/javascript" src="//stats.wp.com/w.js?ver=202252" id="jp-tracks-js"></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/jetpack/jetpack_vendor/automattic/jetpack-connection/src/js/tracks-callables.js?ver=1.0.0"
            id="jp-tracks-functions-js"
        ></script>
        <script type="text/javascript" id="et-builder-modules-global-functions-script-js-extra">
            /* <![CDATA[ */
            var et_builder_utils_params = {
                condition: { diviTheme: true, extraTheme: false },
                scrollLocations: ["app", "top"],
                builderScrollLocations: { desktop: "app", tablet: "app", phone: "app" },
                onloadScrollLocation: "app",
                builderType: "fe",
            };
            /* ]]> */
        </script>
        <script
            type="text/javascript"
            src="/wp-content/themes/Divi/includes/builder/frontend-builder/build/frontend-builder-global-functions.js?ver=4.9.0"
            id="et-builder-modules-global-functions-script-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/dist/vendor/regenerator-runtime.min.js?ver=0.13.9"
            id="regenerator-runtime-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/dist/vendor/wp-polyfill.min.js?ver=3.15.0"
            id="wp-polyfill-js"
        ></script>
        <script type="text/javascript" id="contact-form-7-js-extra">
            /* <![CDATA[ */
            var wpcf7 = {
                api: { root: "httpsstaging.dndcleaners.ca\/wp-json\/", namespace: "contact-form-7\/v1" },
                cached: "1",
            };
            /* ]]> */
        </script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/contact-form-7/includes/js/index.js?ver=5.5.6.1"
            id="contact-form-7-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.7.0-wc.6.3.1"
            id="jquery-blockui-js"
        ></script>
        <script type="text/javascript" id="wc-add-to-cart-js-extra">
            /* <![CDATA[ */
            var wc_add_to_cart_params = {
                ajax_url: "\/wp-admin\/admin-ajax.php",
                wc_ajax_url: "\/?wc-ajax=%%endpoint%%",
                i18n_view_cart: "Xem giỏ hàng",
                cart_url: "httpsstaging.dndcleaners.ca\/cart\/",
                is_cart: "",
                cart_redirect_after_add: "no",
            };
            /* ]]> */
        </script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=6.3.1"
            id="wc-add-to-cart-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4-wc.6.3.1"
            id="js-cookie-js"
        ></script>
        <script type="text/javascript" id="woocommerce-js-extra">
            /* <![CDATA[ */
            var woocommerce_params = { ajax_url: "\/wp-admin\/admin-ajax.php", wc_ajax_url: "\/?wc-ajax=%%endpoint%%" };
            /* ]]> */
        </script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js?ver=6.3.1"
            id="woocommerce-js"
        ></script>
        <script type="text/javascript" id="wc-cart-fragments-js-extra">
            /* <![CDATA[ */
            var wc_cart_fragments_params = {
                ajax_url: "\/wp-admin\/admin-ajax.php",
                wc_ajax_url: "\/?wc-ajax=%%endpoint%%",
                cart_hash_key: "wc_cart_hash_55e61cac548e49cf221c551b1a85d752",
                fragment_name: "wc_fragments_55e61cac548e49cf221c551b1a85d752",
                request_timeout: "5000",
            };
            /* ]]> */
        </script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=6.3.1"
            id="wc-cart-fragments-js"
        ></script>
        <script type="text/javascript" id="wc-cart-fragments-js-after">
            jQuery("body").bind("wc_fragments_refreshed", function () {
                var jetpackLazyImagesLoadEvent;
                try {
                    jetpackLazyImagesLoadEvent = new Event("jetpack-lazy-images-load", {
                        bubbles: true,
                        cancelable: true,
                    });
                } catch (e) {
                    jetpackLazyImagesLoadEvent = document.createEvent("Event");
                    jetpackLazyImagesLoadEvent.initEvent("jetpack-lazy-images-load", true, true);
                }
                jQuery("body").get(0).dispatchEvent(jetpackLazyImagesLoadEvent);
            });
        </script>
        <script
            type="text/javascript"
            src="/wp-content/themes/Divi/includes/builder/scripts/ext/jquery.mobile.custom.min.js?ver=4.9.0"
            id="et-jquery-touch-mobile-js"
        ></script>
        <script type="text/javascript" id="divi-custom-script-js-extra">
            /* <![CDATA[ */
            var DIVI = { item_count: "%d Item", items_count: "%d Items" };
            /* ]]> */
        </script>
        <script
            type="text/javascript"
            src="/wp-content/themes/Divi/js/custom.js?ver=4.9.0"
            id="divi-custom-script-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/themes/Divi/js/smoothscroll.js?ver=4.9.0"
            id="smooth-scroll-js"
        ></script>

        <script
            type="text/javascript"
            src="/wp-content/themes/Divi/includes/builder/frontend-builder/build/frontend-builder-scripts.js?ver=4.9.0"
            id="et-builder-modules-script-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/plugins/image-carousel-divi/scripts/frontend-bundle.min.js?ver=1.0"
            id="lwp-image-carousel-frontend-bundle-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-includes/js/jquery/ui/core.min.js?ver=1.13.1"
            id="jquery-ui-core-js"
        ></script>
        <script type="text/javascript" id="popup-maker-site-js-extra">
            /* <![CDATA[ */
            var pum_vars = {
                version: "1.16.7",
                pm_dir_url: "http:\/\/staging.dndcleaners.ca\/wp-content\/plugins\/popup-maker\/",
                ajaxurl: "httpsstaging.dndcleaners.ca\/wp-admin\/admin-ajax.php",
                restapi: "httpsstaging.dndcleaners.ca\/wp-json\/pum\/v1",
                rest_nonce: null,
                default_theme: "867",
                debug_mode: "",
                disable_tracking: "",
                home_url: "\/",
                message_position: "top",
                core_sub_forms_enabled: "1",
                popups: [],
                analytics_route: "analytics",
                analytics_api: "httpsstaging.dndcleaners.ca\/wp-json\/pum\/v1",
            };
            var pum_sub_vars = {
                ajaxurl: "httpsstaging.dndcleaners.ca\/wp-admin\/admin-ajax.php",
                message_position: "top",
            };
            var pum_popups = {
                "pum-876": {
                    triggers: [{ type: "auto_open", settings: { cookie_name: ["pum-876"], delay: "500" } }],
                    cookies: [
                        { event: "on_popup_close", settings: { name: "pum-876", time: "30 minutes", path: true } },
                    ],
                    disable_on_mobile: false,
                    disable_on_tablet: false,
                    atc_promotion: null,
                    explain: null,
                    type_section: null,
                    theme_id: "867",
                    size: "normal",
                    responsive_min_width: "0%",
                    responsive_max_width: "100%",
                    custom_width: "640px",
                    custom_height_auto: false,
                    custom_height: "380px",
                    scrollable_content: false,
                    animation_type: "fade",
                    animation_speed: "350",
                    animation_origin: "center top",
                    open_sound: "none",
                    custom_sound: "",
                    location: "center top",
                    position_top: "100",
                    position_bottom: "0",
                    position_left: "0",
                    position_right: "0",
                    position_from_trigger: false,
                    position_fixed: false,
                    overlay_disabled: false,
                    stackable: false,
                    disable_reposition: false,
                    zindex: "1999999999",
                    close_button_delay: "0",
                    fi_promotion: null,
                    close_on_form_submission: true,
                    close_on_form_submission_delay: "0",
                    close_on_overlay_click: true,
                    close_on_esc_press: false,
                    close_on_f4_press: false,
                    disable_form_reopen: true,
                    disable_accessibility: false,
                    theme_slug: "default-theme",
                    id: 876,
                    slug: "popup-ngon-ngu-khi-load-trang",
                },
            };
            /* ]]> */
        </script>
        <script
            type="text/javascript"
            src="/wp-content/uploads/pum/pum-site-scripts.js?defer&#038;generated=1653281024&#038;ver=1.16.7"
            id="popup-maker-site-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/themes/Divi/includes/builder/scripts/ext/jquery.fitvids.js?ver=4.9.0"
            id="divi-fitvids-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/themes/Divi/includes/builder/scripts/ext/waypoints.min.js?ver=4.9.0"
            id="waypoints-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/themes/Divi/includes/builder/scripts/ext/jquery.magnific-popup.js?ver=4.9.0"
            id="magnific-popup-js"
        ></script>
        <script
            type="text/javascript"
            src="/wp-content/themes/Divi/core/admin/js/common.js?ver=4.9.0"
            id="et-core-common-js"
        ></script>
        <script type="text/javascript">
            (function () {
                var expirationDate = new Date();
                expirationDate.setTime(expirationDate.getTime() + 31536000 * 1000);
                document.cookie = "pll_language=vi; expires=" + expirationDate.toUTCString() + "; path=/; SameSite=Lax";
            })();
        </script>
        <!-- same -->