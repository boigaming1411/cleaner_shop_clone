 <nav id="top-menu-nav">
                            <ul id="top-menu" class="nav">
                                <li
                                    @if($current=='homepage')
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-142 current_page_item menu-item-368"
                                    @else
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-368"
                                    @endif
                                    id="menu-item-368"
                                >
                                    <a href="/" aria-current="page">Trang chủ</a>
                                </li>
                                <li
                                    id="menu-item-822"
                                    @if($current=='service')
                                        class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-809 current_page_item menu-item-822"
                                    @else
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-822"
                                    @endif
                                >
                                    <a href="/service/">Dịch vụ</a>
                                </li>
                                <li
                                    id="menu-item-365"
                                    @if($current=='gallery')
                                        class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-335 current_page_item menu-item-365"
                                    @else
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-365"
                                    @endif
                                >
                                    <a href="/gallery/">Bộ sưu tập</a>
                                </li>
                                <li
                                    id="menu-item-1212"
                                    @if($current=='store')
                                        class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-1186 current_page_item menu-item-has-children menu-item-1212"
                                    @else
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-1212"
                                    @endif
                                >
                                    <a href="/store/">Sản phẩm</a>
                                    <ul class="sub-menu">
                                        <li
                                            id="menu-item-1822"
                                            class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1822"
                                        >
                                            <a href="/product-category/ao-dai/">Áo dài</a>
                                        </li>
                                        <li
                                            id="menu-item-1823"
                                            class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-1823"
                                        >
                                            <a href="/product-category/my-pham/">Mỹ phẩm</a>
                                        </li>
                                    </ul>
                                </li>
                                <li
                                    id="menu-item-762"
                                    @if($current=='blogs')
                                        class="menu-item menu-item-type-taxonomy menu-item-object-category current-menu-item menu-item-762"
                                    @else
                                        class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-762"
                                    @endif
                                >
                                    <a href="/blogs/">Tin tức</a>
                                </li>
                                <li
                                    id="menu-item-367"
                                    @if($current=='contact-us')
                                        class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-294 current_page_item menu-item-367"
                                    @else
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-367"
                                    @endif
                                >
                                    <a href="/contact-us/">Liên hệ chúng tôi</a>
                                </li>
                                <li
                                    id="menu-item-781-vi"
                                    class="lang-item lang-item-11 lang-item-vi current-lang lang-item-first menu-item menu-item-type-custom menu-item-object-custom current_page_item menu-item-home menu-item-781-vi"
                                >
                                    <a href="/" hreflang="vi" lang="vi"
                                        ><img
                                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAMAAABBPP0LAAAATlBMVEX+AAD2AADvAQH/eXn+cXL9amr8YmL9Wlr8UlL7TkvoAAD8d0f6Pz/3ODf2Ly/0KSf6R0f6wTv60T31IBz6+jr4+Cv3QybzEhL4bizhAADgATv8AAAAW0lEQVR4AQXBgU3DQBRAMb+7jwKVUPefkQEQTYJqByBENpKUGoZslXoN5LPONH8G9WWZ7pGlOn6XZmaGRce1J/seei4dl+7dPWDqkk7+58e3+igdlySPcYbwBG+lPhCjrtt9EgAAAABJRU5ErkJggg=="
                                            alt="Tiếng Việt"
                                            width="16"
                                            height="11"
                                            style="width: 16px; height: 11px"
                                    /></a>
                                </li>
                                <li
                                    id="menu-item-781-en"
                                    class="lang-item lang-item-13 lang-item-en menu-item menu-item-type-custom menu-item-object-custom menu-item-781-en"
                                >
                                    <a href="/en/homepage/" hreflang="en-US" lang="en-US"
                                        ><img
                                            src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAMAAABBPP0LAAAAmVBMVEViZsViZMJiYrf9gnL8eWrlYkjgYkjZYkj8/PujwPybvPz4+PetraBEgfo+fvo3efkydfkqcvj8Y2T8UlL8Q0P8MzP9k4Hz8/Lu7u4DdPj9/VrKysI9fPoDc/EAZ7z7IiLHYkjp6ekCcOTk5OIASbfY/v21takAJrT5Dg6sYkjc3Nn94t2RkYD+y8KeYkjs/v7l5fz0dF22YkjWvcOLAAAAgElEQVR4AR2KNULFQBgGZ5J13KGGKvc/Cw1uPe62eb9+Jr1EUBFHSgxxjP2Eca6AfUSfVlUfBvm1Ui1bqafctqMndNkXpb01h5TLx4b6TIXgwOCHfjv+/Pz+5vPRw7txGWT2h6yO0/GaYltIp5PT1dEpLNPL/SdWjYjAAZtvRPgHJX4Xio+DSrkAAAAASUVORK5CYII="
                                            alt="English"
                                            width="16"
                                            height="11"
                                            style="width: 16px; height: 11px"
                                    /></a>
                                </li>
                            </ul>
                        </nav>