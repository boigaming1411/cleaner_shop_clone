<!DOCTYPE html>
<html dir="ltr" lang="vi" prefix="og: https://ogp.me/ns#">
    <head>
       
        <title>Mỹ phẩm - DND Cleaners</title>

        <!-- All in One SEO 4.2.0 -->
        <meta name="robots" content="noindex, nofollow, max-image-preview:large" />
        <link rel="canonical" href="/product-category/san-pham/my-pham/" />
        <script type="application/ld+json" class="aioseo-schema">
            {
                "@context": "https:\/\/schema.org",
                "@graph": [
                    {
                        "@type": "WebSite",
                        "@id": "httpsstaging.dndcleaners.ca\/#website",
                        "url": "httpsstaging.dndcleaners.ca\/",
                        "name": "DND Cleaners",
                        "description": "Professional Alternations & Repairs",
                        "inLanguage": "vi",
                        "publisher": { "@id": "httpsstaging.dndcleaners.ca\/#organization" }
                    },
                    {
                        "@type": "Organization",
                        "@id": "httpsstaging.dndcleaners.ca\/#organization",
                        "name": "DND Cleaners",
                        "url": "httpsstaging.dndcleaners.ca\/"
                    },
                    {
                        "@type": "BreadcrumbList",
                        "@id": "httpsstaging.dndcleaners.ca\/product-category\/san-pham\/my-pham\/#breadcrumblist",
                        "itemListElement": [
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/#listItem",
                                "position": 1,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/",
                                    "name": "Home",
                                    "description": "D&D Cleaners chuyên may, cho thuê áo cưới, sửa áo quần, giặt ủi, bán dầu gió, thực phẩm chức năng tại Vaughan, Ontario, Canada",
                                    "url": "httpsstaging.dndcleaners.ca\/"
                                },
                                "nextItem": "httpsstaging.dndcleaners.ca\/product-category\/san-pham\/#listItem"
                            },
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/product-category\/san-pham\/#listItem",
                                "position": 2,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/product-category\/san-pham\/",
                                    "name": "Sản phẩm",
                                    "url": "httpsstaging.dndcleaners.ca\/product-category\/san-pham\/"
                                },
                                "nextItem": "httpsstaging.dndcleaners.ca\/product-category\/san-pham\/my-pham\/#listItem",
                                "previousItem": "httpsstaging.dndcleaners.ca\/#listItem"
                            },
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/product-category\/san-pham\/my-pham\/#listItem",
                                "position": 3,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/product-category\/san-pham\/my-pham\/",
                                    "name": "Mỹ phẩm",
                                    "url": "httpsstaging.dndcleaners.ca\/product-category\/san-pham\/my-pham\/"
                                },
                                "previousItem": "httpsstaging.dndcleaners.ca\/product-category\/san-pham\/#listItem"
                            }
                        ]
                    },
                    {
                        "@type": "CollectionPage",
                        "@id": "httpsstaging.dndcleaners.ca\/product-category\/san-pham\/my-pham\/#collectionpage",
                        "url": "httpsstaging.dndcleaners.ca\/product-category\/san-pham\/my-pham\/",
                        "name": "Mỹ phẩm - DND Cleaners",
                        "inLanguage": "vi",
                        "isPartOf": { "@id": "httpsstaging.dndcleaners.ca\/#website" },
                        "breadcrumb": {
                            "@id": "httpsstaging.dndcleaners.ca\/product-category\/san-pham\/my-pham\/#breadcrumblist"
                        }
                    }
                ]
            }
        </script>
        <!-- All in One SEO -->

        <link rel="alternate" href="/product-category/san-pham/my-pham/" hreflang="vi" />
        <link rel="alternate" href="/en/product-category/products/cosmetics/" hreflang="en" />
       @include('public.header')
    </head>
    {{$current='my-pham'}}
    <body
        class="archive tax-product_cat term-my-pham term-145 theme-Divi woocommerce woocommerce-page woocommerce-no-js et_pb_button_helper_class et_fixed_nav et_show_nav et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_header_style_left et_pb_footer_columns4 et_cover_background et_pb_gutter et_pb_gutters3 et_smooth_scroll et_right_sidebar et_divi_theme et-db"
    >
        <div id="page-container">
            <header id="main-header" data-height-onload="66">
                <div class="container clearfix et_menu_container">
                    <div class="logo_container">
                        <span class="logo_helper"></span>
                        <a href="/">
                            <img
                                src="/wp-content/uploads/2021/03/DD-cleaners.jpg"
                                alt="DND Cleaners"
                                id="logo"
                                data-height-percentage="87"
                            />
                        </a>
                    </div>
                    <div id="et-top-navigation" data-height="66" data-fixed-height="40">
                        @include('public.navbar')

                        <a href="/cart/" class="et-cart-info">
                            <span></span>
                        </a>

                        <div id="et_mobile_nav_menu">
                            <div class="mobile_nav closed">
                                <span class="select_page">Chọn trang</span>
                                <span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
                            </div>
                        </div>
                    </div>
                    <!-- #et-top-navigation -->
                </div>
                <!-- .container -->
                <div class="et_search_outer">
                    <div class="container et_search_form_container">
                        <form
                            role="search"
                            method="get"
                            class="et-search-form"
                            action="https://staging.dndcleaners.ca/"
                        >
                            <input
                                type="search"
                                class="et-search-field"
                                placeholder="Tìm kiếm &hellip;"
                                value=""
                                name="s"
                                title="Tìm:"
                            />
                        </form>
                        <span class="et_close_search_field"></span>
                    </div>
                </div>
            </header>
            <!-- #main-header -->
            <div id="et-main-area">
                <div id="main-content">
                    <div class="container">
                        <div id="content-area" class="clearfix">
                            <div id="left-area">
                                <nav class="woocommerce-breadcrumb">
                                    <a href="/">Trang chủ</a>&nbsp;&#47;&nbsp;<a href="/product-category/san-pham/"
                                        >Sản phẩm</a
                                    >&nbsp;&#47;&nbsp;Mỹ phẩm
                                </nav>
                                <header class="woocommerce-products-header">
                                    <h1 class="woocommerce-products-header__title page-title">Mỹ phẩm</h1>
                                </header>
                                <div class="woocommerce-notices-wrapper"></div>
                                <p class="woocommerce-result-count">Hiển thị tất cả 7 kết quả</p>
                                <form class="woocommerce-ordering" method="get">
                                    <select name="orderby" class="orderby" aria-label="Đơn hàng của cửa hàng">
                                        <option value="menu_order" selected>Thứ tự mặc định</option>
                                        <option value="popularity">Thứ tự theo mức độ phổ biến</option>
                                        <option value="date">Mới nhất</option>
                                        <option value="price">Thứ tự theo giá: thấp đến cao</option>
                                        <option value="price-desc">Thứ tự theo giá: cao xuống thấp</option>
                                    </select>
                                    <input type="hidden" name="paged" value="1" />
                                </form>
                                <ul class="products columns-3">
                                    <li
                                        class="product type-product post-1140 status-publish first instock product_cat-my-pham product_cat-san-pham has-post-thumbnail shipping-taxable product-type-simple"
                                    >
                                        <a
                                            href="/product/dau-goi-thao-duoc-atomy-herbal-hair-shampoo/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                            ><span class="et_shop_image"
                                                ><img
                                                    width="300"
                                                    height="300"
                                                    src="/wp-content/uploads/2022/04/herbal-hair-shampoo-300x300.jpg"
                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                    alt=""
                                                    loading="lazy" /><span class="et_overlay"></span
                                            ></span>
                                            <h2 class="woocommerce-loop-product__title">
                                                Dầu Gội Thảo Dược Atomy Herbal Hair Shampoo
                                            </h2>
                                        </a>
                                    </li>
                                    <li
                                        class="product type-product post-1141 status-publish instock product_cat-my-pham product_cat-san-pham has-post-thumbnail shipping-taxable product-type-simple"
                                    >
                                        <a
                                            href="/product/dau-xa-han-quoc-atomy-herbal-hair-conditioner/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                            ><span class="et_shop_image"
                                                ><img
                                                    width="300"
                                                    height="300"
                                                    src="/wp-content/uploads/2022/04/herbal-hair-conditioner-300x300.jpg"
                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                    alt=""
                                                    loading="lazy" /><span class="et_overlay"></span
                                            ></span>
                                            <h2 class="woocommerce-loop-product__title">
                                                Dầu xả Hàn Quốc Atomy Herbal Hair Conditioner
                                            </h2>
                                        </a>
                                    </li>
                                    <li
                                        class="product type-product post-1138 status-publish last instock product_cat-my-pham product_cat-san-pham has-post-thumbnail shipping-taxable product-type-simple"
                                    >
                                        <a
                                            href="/product/dung-dich-ve-sinh-thao-moc-atomy-body-care-aidam-cleanesr-200/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                            ><span class="et_shop_image"
                                                ><img
                                                    width="300"
                                                    height="300"
                                                    src="/wp-content/uploads/2022/04/body-care-aidam-cleanser3-300x300.jpg"
                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                    alt=""
                                                    loading="lazy" /><span class="et_overlay"></span
                                            ></span>
                                            <h2 class="woocommerce-loop-product__title">
                                                Dung Dịch Vệ Sinh Thảo Mộc ATOMY Body Care Aidam Cleanser 200ml
                                            </h2>
                                        </a>
                                    </li>
                                    <li
                                        class="product type-product post-1182 status-publish first instock product_cat-my-pham product_cat-san-pham has-post-thumbnail shipping-taxable product-type-simple"
                                    >
                                        <a
                                            href="/product/mat-na-linh-chi-duong-am-mitsuwa/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                            ><span class="et_shop_image"
                                                ><img
                                                    width="300"
                                                    height="300"
                                                    src="/wp-content/uploads/2022/04/intense-moisturizing-300x300.jpg"
                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                    alt=""
                                                    loading="lazy" /><span class="et_overlay"></span
                                            ></span>
                                            <h2 class="woocommerce-loop-product__title">
                                                Mặt Nạ Dưỡng Ẩm Miracle Reishi Moisturizing Serum
                                            </h2>
                                        </a>
                                    </li>
                                    <li
                                        class="product type-product post-1139 status-publish instock product_cat-my-pham product_cat-san-pham has-post-thumbnail shipping-taxable product-type-simple"
                                    >
                                        <a
                                            href="/product/mat-na-duong-da-atomy-daily-mask/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                            ><span class="et_shop_image"
                                                ><img
                                                    width="300"
                                                    height="300"
                                                    src="/wp-content/uploads/2022/04/daily-mask-300x300.jpg"
                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                    alt=""
                                                    loading="lazy" /><span class="et_overlay"></span
                                            ></span>
                                            <h2 class="woocommerce-loop-product__title">
                                                Mặt Nạ Dưỡng Da Atomy Daily Mask
                                            </h2>
                                        </a>
                                    </li>
                                    <li
                                        class="product type-product post-1142 status-publish last instock product_cat-my-pham product_cat-san-pham has-post-thumbnail shipping-taxable product-type-simple"
                                    >
                                        <a
                                            href="/product/sua-rua-mat-atomy-evening-care/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                            ><span class="et_shop_image"
                                                ><img
                                                    width="300"
                                                    height="300"
                                                    src="/wp-content/uploads/2022/04/lovercare-goats-milk-300x300.jpg"
                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                    alt=""
                                                    loading="lazy" /><span class="et_overlay"></span
                                            ></span>
                                            <h2 class="woocommerce-loop-product__title">
                                                Sữa Rửa Mặt Atomy Evening Care
                                            </h2>
                                        </a>
                                    </li>
                                    <li
                                        class="product type-product post-1135 status-publish first instock product_cat-my-pham product_cat-san-pham has-post-thumbnail shipping-taxable product-type-simple"
                                    >
                                        <a
                                            href="/product/xit-khoang-duong-am-da-atomy-3-seconds-beauty-water/"
                                            class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                            ><span class="et_shop_image"
                                                ><img
                                                    width="300"
                                                    height="300"
                                                    src="/wp-content/uploads/2022/04/3seconds-beauty-water-300x300.jpg"
                                                    class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                    alt=""
                                                    loading="lazy" /><span class="et_overlay"></span
                                            ></span>
                                            <h2 class="woocommerce-loop-product__title">
                                                Xịt khoáng dưỡng ẩm da Atomy 3 Seconds Beauty Water
                                            </h2>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- #left-area -->
                            <div id="sidebar">
                                <div id="block-4" class="et_pb_widget widget_block">
                                    <h2>Tìm sản phẩm khác</h2>
                                </div>
                                <!-- end .et_pb_widget -->
                                <div id="block-2" class="et_pb_widget widget_block">
                                    <div
                                        data-block-name="woocommerce/product-search"
                                        data-label=""
                                        data-form-id="wc-block-product-search-2"
                                        class="wc-block-product-search wp-block-woocommerce-product-search"
                                    >
                                        <form role="search" method="get" action="https://staging.dndcleaners.ca/">
                                            <label
                                                for="wc-block-search__input-1"
                                                class="wc-block-product-search__label"
                                            ></label>
                                            <div class="wc-block-product-search__fields">
                                                <input
                                                    type="search"
                                                    id="wc-block-search__input-1"
                                                    class="wc-block-product-search__field"
                                                    placeholder="Tìm kiếm sản phẩm..."
                                                    name="s"
                                                /><button
                                                    type="submit"
                                                    class="wc-block-product-search__button"
                                                    aria-label="Tìm kiếm"
                                                >
                                                    <svg
                                                        aria-hidden="true"
                                                        role="img"
                                                        focusable="false"
                                                        class="dashicon dashicons-arrow-right-alt2"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        width="20"
                                                        height="20"
                                                        viewbox="0 0 20 20"
                                                    >
                                                        <path d="M6 15l5-5-5-5 1-2 7 7-7 7z"></path>
                                                    </svg>
                                                </button>
                                                <input type="hidden" name="post_type" value="product" />
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- end .et_pb_widget -->
                                <div id="block-5" class="et_pb_widget widget_block">
                                    <h2>Danh mục sản phẩm</h2>
                                </div>
                                <!-- end .et_pb_widget -->
                                <div id="block-3" class="et_pb_widget widget_block">
                                    <div
                                        data-block-name="woocommerce/product-categories"
                                        class="wp-block-woocommerce-product-categories wc-block-product-categories is-list"
                                    >
                                        <ul
                                            class="wc-block-product-categories-list wc-block-product-categories-list--depth-0"
                                        >
                                            <li class="wc-block-product-categories-list-item">
                                                <a href="/product-category/san-pham/">Sản phẩm</a>
                                                <span class="wc-block-product-categories-list-item-count"
                                                    ><span aria-hidden="true">40</span
                                                    ><span class="screen-reader-text">40 sản phẩm</span></span
                                                >
                                                <ul
                                                    class="wc-block-product-categories-list wc-block-product-categories-list--depth-1"
                                                >
                                                    <li class="wc-block-product-categories-list-item">
                                                        <a href="/product-category/san-pham/ao-dai/">Áo dài</a>
                                                        <span class="wc-block-product-categories-list-item-count"
                                                            ><span aria-hidden="true">33</span
                                                            ><span class="screen-reader-text">33 sản phẩm</span></span
                                                        >
                                                    </li>
                                                    <li class="wc-block-product-categories-list-item">
                                                        <a href="/product-category/san-pham/my-pham/">Mỹ phẩm</a>
                                                        <span class="wc-block-product-categories-list-item-count"
                                                            ><span aria-hidden="true">7</span
                                                            ><span class="screen-reader-text">7 sản phẩm</span></span
                                                        >
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- end .et_pb_widget -->
                            </div>
                            <!-- end #sidebar -->
                        </div>
                        <!-- #content-area -->
                    </div>
                    <!-- .container -->
                </div>
                <!-- #main-content -->
                <span class="et_pb_scroll_top et-pb-icon"></span>

                <footer id="main-footer">
                    <div id="footer-bottom">
                        <div class="container clearfix">
                            <div id="footer-info">D&amp;D CLEANERS | Copyright © 2021. All Rights Reserved.</div>
                        </div>
                        <!-- .container -->
                    </div>
                </footer>
                <!-- #main-footer -->
            </div>
            <!-- #et-main-area -->
        </div>
        <!-- #page-container -->

        
        <script type="application/ld+json">
            {
                "@context": "https:\/\/schema.org\/",
                "@type": "BreadcrumbList",
                "itemListElement": [
                    {
                        "@type": "ListItem",
                        "position": 1,
                        "item": { "name": "Trang chủ", "@id": "httpsstaging.dndcleaners.ca\/" }
                    },
                    {
                        "@type": "ListItem",
                        "position": 2,
                        "item": {
                            "name": "Sản phẩm",
                            "@id": "httpsstaging.dndcleaners.ca\/product-category\/san-pham\/"
                        }
                    },
                    {
                        "@type": "ListItem",
                        "position": 3,
                        "item": {
                            "name": "Mỹ phẩm",
                            "@id": "httpsstaging.dndcleaners.ca\/product-category\/san-pham\/my-pham\/"
                        }
                    }
                ]
            }
        </script>
        <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="pswp__bg"></div>
            <div class="pswp__scroll-wrap">
                <div class="pswp__container">
                    <div class="pswp__item"></div>
                    <div class="pswp__item"></div>
                    <div class="pswp__item"></div>
                </div>
                <div class="pswp__ui pswp__ui--hidden">
                    <div class="pswp__top-bar">
                        <div class="pswp__counter"></div>
                        <button class="pswp__button pswp__button--close" aria-label="Đóng (Esc)"></button>
                        <button class="pswp__button pswp__button--share" aria-label="Chia sẻ"></button>
                        <button
                            class="pswp__button pswp__button--fs"
                            aria-label="Bật/tắt chế độ toàn màn hình"
                        ></button>
                        <button class="pswp__button pswp__button--zoom" aria-label="Phóng to/ thu nhỏ"></button>
                        <div class="pswp__preloader">
                            <div class="pswp__preloader__icn">
                                <div class="pswp__preloader__cut">
                                    <div class="pswp__preloader__donut"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                        <div class="pswp__share-tooltip"></div>
                    </div>
                    <button
                        class="pswp__button pswp__button--arrow--left"
                        aria-label="Ảnh trước (mũi tên trái)"
                    ></button>
                    <button
                        class="pswp__button pswp__button--arrow--right"
                        aria-label="Ảnh tiếp (mũi tên phải)"
                    ></button>
                    <div class="pswp__caption">
                        <div class="pswp__caption__center"></div>
                    </div>
                </div>
            </div>
        </div>
        
        <script type="text/javascript" id="et-builder-modules-script-js-extra">
            /* <![CDATA[ */
            var et_frontend_scripts = { builderCssContainerPrefix: "#et-boc", builderCssLayoutPrefix: "#et-boc .et-l" };
            var et_pb_custom = {
                ajaxurl: "httpsstaging.dndcleaners.ca\/wp-admin\/admin-ajax.php",
                images_uri: "httpsstaging.dndcleaners.ca\/wp-content\/themes\/Divi\/images",
                builder_images_uri: "httpsstaging.dndcleaners.ca\/wp-content\/themes\/Divi\/includes\/builder\/images",
                et_frontend_nonce: "4e47d1f20d",
                subscription_failed: "Làm ơn, kiểm tra đồng dưới chắc chắn để các ông gõ đúng thông tin.",
                et_ab_log_nonce: "0f432c9a55",
                fill_message: "Xin vui lòng, điền vào các lĩnh vực sau đây:",
                contact_error_message: "Xin hãy sửa những lỗi sau:",
                invalid: "Không hợp lệ email",
                captcha: "Captcha",
                prev: "Trước",
                previous: "Trước",
                next: "Tiếp theo",
                wrong_captcha: "Bạn đã nhập sai số captcha.",
                wrong_checkbox: "Checkbox",
                ignore_waypoints: "no",
                is_divi_theme_used: "1",
                widget_search_selector: ".widget_search",
                ab_tests: [],
                is_ab_testing_active: "",
                page_id: "1140",
                unique_test_id: "",
                ab_bounce_rate: "5",
                is_cache_plugin_active: "no",
                is_shortcode_tracking: "",
                tinymce_uri: "",
            };
            var et_pb_box_shadow_elements = [];
            var et_pb_motion_elements = { desktop: [], tablet: [], phone: [] };
            var et_pb_sticky_elements = [];
            /* ]]> */
        </script>
        @include('public.script')
    </body>

    <!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/

Page Caching using disk: enhanced 

Served from: staging.dndcleaners.ca @ 2022-12-27 15:12:48 by W3 Total Cache
--></html>
