<!DOCTYPE html>
<html dir="ltr" lang="vi" prefix="og: https://ogp.me/ns#">
    <head>
       
        <title>Dịch vụ - DND Cleaners</title>

        <!-- All in One SEO 4.2.0 -->
        <meta
            name="description"
            content="Dịch vụ Dịch vụ Sửa quần áo Sửa và may lại các lỗi quần áo để giúp trang phục có thể sử dụng như bình thường Tư vấn khách hàng sửa chữa quần áo theo nhiều cách Thợ sửa chữa áo quần chuyên nghiệp, có kỹ năng về may đo và kiến thức về các"
        />
       
        <link rel="canonical" href="/dndcleaner/" />
        <meta property="og:locale" content="vi_VN" />
        <meta property="og:site_name" content="DND Cleaners - Professional Alternations &amp; Repairs" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="Dịch vụ - DND Cleaners" />
        <meta
            property="og:description"
            content="Dịch vụ Dịch vụ Sửa quần áo Sửa và may lại các lỗi quần áo để giúp trang phục có thể sử dụng như bình thường Tư vấn khách hàng sửa chữa quần áo theo nhiều cách Thợ sửa chữa áo quần chuyên nghiệp, có kỹ năng về may đo và kiến thức về các"
        />
        <meta property="og:url" content="/dndcleaner/" />
        <meta property="article:published_time" content="2021-11-15T06:54:08+00:00" />
        <meta property="article:modified_time" content="2022-06-28T14:08:01+00:00" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="Dịch vụ - DND Cleaners" />
        <meta
            name="twitter:description"
            content="Dịch vụ Dịch vụ Sửa quần áo Sửa và may lại các lỗi quần áo để giúp trang phục có thể sử dụng như bình thường Tư vấn khách hàng sửa chữa quần áo theo nhiều cách Thợ sửa chữa áo quần chuyên nghiệp, có kỹ năng về may đo và kiến thức về các"
        />
        <script type="application/ld+json" class="aioseo-schema">
            {
                "@context": "https:\/\/schema.org",
                "@graph": [
                    {
                        "@type": "WebSite",
                        "@id": "httpsstaging.dndcleaners.ca\/#website",
                        "url": "httpsstaging.dndcleaners.ca\/",
                        "name": "DND Cleaners",
                        "description": "Professional Alternations & Repairs",
                        "inLanguage": "vi",
                        "publisher": { "@id": "httpsstaging.dndcleaners.ca\/#organization" }
                    },
                    {
                        "@type": "Organization",
                        "@id": "httpsstaging.dndcleaners.ca\/#organization",
                        "name": "DND Cleaners",
                        "url": "httpsstaging.dndcleaners.ca\/"
                    },
                    {
                        "@type": "BreadcrumbList",
                        "@id": "httpsstaging.dndcleaners.ca\/dndcleaner\/#breadcrumblist",
                        "itemListElement": [
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/#listItem",
                                "position": 1,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/",
                                    "name": "Home",
                                    "description": "D&D Cleaners chuyên may, cho thuê áo cưới, sửa áo quần, giặt ủi, bán dầu gió, thực phẩm chức năng tại Vaughan, Ontario, Canada",
                                    "url": "httpsstaging.dndcleaners.ca\/"
                                },
                                "nextItem": "httpsstaging.dndcleaners.ca\/dndcleaner\/#listItem"
                            },
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/dndcleaner\/#listItem",
                                "position": 2,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/dndcleaner\/",
                                    "name": "Dịch vụ",
                                    "description": "Dịch vụ Dịch vụ Sửa quần áo Sửa và may lại các lỗi quần áo để giúp trang phục có thể sử dụng như bình thường Tư vấn khách hàng sửa chữa quần áo theo nhiều cách Thợ sửa chữa áo quần chuyên nghiệp, có kỹ năng về may đo và kiến thức về các",
                                    "url": "httpsstaging.dndcleaners.ca\/dndcleaner\/"
                                },
                                "previousItem": "httpsstaging.dndcleaners.ca\/#listItem"
                            }
                        ]
                    },
                    {
                        "@type": "WebPage",
                        "@id": "httpsstaging.dndcleaners.ca\/dndcleaner\/#webpage",
                        "url": "httpsstaging.dndcleaners.ca\/dndcleaner\/",
                        "name": "Dịch vụ - DND Cleaners",
                        "description": "Dịch vụ Dịch vụ Sửa quần áo Sửa và may lại các lỗi quần áo để giúp trang phục có thể sử dụng như bình thường Tư vấn khách hàng sửa chữa quần áo theo nhiều cách Thợ sửa chữa áo quần chuyên nghiệp, có kỹ năng về may đo và kiến thức về các",
                        "inLanguage": "vi",
                        "isPartOf": { "@id": "httpsstaging.dndcleaners.ca\/#website" },
                        "breadcrumb": { "@id": "httpsstaging.dndcleaners.ca\/dndcleaner\/#breadcrumblist" },
                        "datePublished": "2021-11-15T06:54:08+00:00",
                        "dateModified": "2022-06-28T14:08:01+00:00"
                    }
                ]
            }
        </script>
        <!-- All in One SEO -->

        
       
        <!-- End Google Analytics snippet added by Site Kit -->
        @include('public.header')
    </head>
    {{$current='service'}}
    <body
        class="page-template-default page page-id-809 theme-Divi woocommerce-no-js et_pb_button_helper_class et_fixed_nav et_show_nav et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_header_style_left et_pb_footer_columns4 et_cover_background et_pb_gutter windows et_pb_gutters3 et_pb_pagebuilder_layout et_smooth_scroll et_no_sidebar et_divi_theme et-db"
    >
        <div id="page-container">
            <header id="main-header" data-height-onload="66">
                <div class="container clearfix et_menu_container">
                    <div class="logo_container">
                        <span class="logo_helper"></span>
                        <a href="/">
                            <img
                                src="/wp-content/uploads/2021/03/DD-cleaners.jpg"
                                alt="DND Cleaners"
                                id="logo"
                                data-height-percentage="87"
                            />
                        </a>
                    </div>
                    <div id="et-top-navigation" data-height="66" data-fixed-height="40">
                        @include('public.navbar')

                        <a href="/cart/" class="et-cart-info">
                            <span></span>
                        </a>

                        <div id="et_mobile_nav_menu">
                            <div class="mobile_nav closed">
                                <span class="select_page">Chọn trang</span>
                                <span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
                            </div>
                        </div>
                    </div>
                    <!-- #et-top-navigation -->
                </div>
                <!-- .container -->
                <div class="et_search_outer">
                    <div class="container et_search_form_container">
                        <form
                            role="search"
                            method="get"
                            class="et-search-form"
                            action="https://staging.dndcleaners.ca/"
                        >
                            <input
                                type="search"
                                class="et-search-field"
                                placeholder="Tìm kiếm &hellip;"
                                value=""
                                name="s"
                                title="Tìm:"
                            />
                        </form>
                        <span class="et_close_search_field"></span>
                    </div>
                </div>
            </header>
            <!-- #main-header -->
            <div id="et-main-area">
                <div id="main-content">
                    <article id="post-809" class="post-809 page type-page status-publish hentry">
                        <div class="entry-content">
                            <div id="et-boc" class="et-boc">
                                <div class="et-l et-l--post">
                                    <div class="et_builder_inner_content et_pb_gutters3">
                                        <div
                                            class="et_pb_section et_pb_section_4 et_pb_with_background et_section_regular"
                                        >
                                            <div class="et_pb_row et_pb_row_3">
                                                <div
                                                    class="et_pb_column et_pb_column_4_4 et_pb_column_10 et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_6 et_pb_text_align_left et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner"><h2>Dịch vụ</h2></div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                        </div>
                                        <!-- .et_pb_section -->
                                        <div class="et_pb_section et_pb_section_5 et_section_regular">
                                            <div class="et_pb_row et_pb_row_4">
                                                <div
                                                    class="et_pb_column et_pb_column_1_3 et_pb_column_11 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div
                                                        class="et_pb_with_border et_pb_module et_pb_blurb et_pb_blurb_6 et_pb_text_align_center et_pb_blurb_position_top et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_blurb_content">
                                                            <div class="et_pb_blurb_container">
                                                                <h3 class="et_pb_module_header">
                                                                    <span>Dịch vụ Sửa quần áo</span>
                                                                </h3>
                                                                <div class="et_pb_blurb_description">
                                                                    <ul>
                                                                        <li style="text-align: left">
                                                                            Sửa và may lại các lỗi quần áo để giúp trang
                                                                            phục có thể sử dụng như bình thường
                                                                        </li>
                                                                        <li style="text-align: left">
                                                                            Tư vấn khách hàng sửa chữa quần áo theo
                                                                            nhiều cách
                                                                        </li>
                                                                        <li style="text-align: left">
                                                                            Thợ sửa chữa áo quần chuyên nghiệp, có kỹ
                                                                            năng về may đo và kiến thức về các loại vải
                                                                            và trang phục, việc am hiểu này sẽ giúp cho
                                                                            việc sửa chữa trở nên tốt hơn
                                                                        </li>
                                                                        <li style="text-align: left">
                                                                            Kiểm duyệt lại trang phục sau khi sửa chữa
                                                                            trước khi giao khách hàng
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- .et_pb_blurb_content -->
                                                    </div>
                                                    <!-- .et_pb_blurb -->
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_3 et_pb_column_12 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div
                                                        class="et_pb_with_border et_pb_module et_pb_blurb et_pb_blurb_7 et_pb_text_align_center et_pb_blurb_position_top et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_blurb_content">
                                                            <div class="et_pb_blurb_container">
                                                                <h3 class="et_pb_module_header">
                                                                    <span>Dịch vụ Cho thuê/bán áo dài cưới</span>
                                                                </h3>
                                                                <div class="et_pb_blurb_description">
                                                                    <ul>
                                                                        <li style="text-align: left">
                                                                            Cung cấp nhiều mẫu áo cưới, áo dài cưới tại
                                                                            Vaughan, Canada
                                                                        </li>
                                                                        <li style="text-align: left">
                                                                            Dịch vụ may đo hiệu chỉnh từng chiếc váy
                                                                            cưới, ào dài cưới đẹp, phù hợp với mọi vóc
                                                                            dáng cho cô dâu chú rể
                                                                        </li>
                                                                        <li style="text-align: left">
                                                                            Đầy đủ kiểu dáng mẫu mã trang phục truyền
                                                                            thống của Việt Nam đến hiện đại
                                                                        </li>
                                                                        <li style="text-align: left">
                                                                            Phục vụ tận tình, tận tâm, am hiểu văn hoá
                                                                            trang phục áo cưới, áo dài
                                                                        </li>
                                                                        <li style="text-align: left">
                                                                            Tư vấn miễn phí trang phục cưới
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- .et_pb_blurb_content -->
                                                    </div>
                                                    <!-- .et_pb_blurb -->
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_3 et_pb_column_13 et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div
                                                        class="et_pb_with_border et_pb_module et_pb_blurb et_pb_blurb_8 et_pb_text_align_center et_pb_blurb_position_top et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_blurb_content">
                                                            <div class="et_pb_blurb_container">
                                                                <h3 class="et_pb_module_header">
                                                                    <span>Dịch vụ Cho thuê mâm quả cưới</span>
                                                                </h3>
                                                                <div class="et_pb_blurb_description">
                                                                    <ul>
                                                                        <li style="text-align: left">
                                                                            Dịch vụ cho thuê mâm quả cưới tại Canada
                                                                        </li>
                                                                        <li style="text-align: left">
                                                                            Cung cấp đầy đủ các lễ vật: mâm cau trầu,
                                                                            mâm trà rượu, mâm bánh phu thê, mâm lễ vật,
                                                                            mâm bánh trái ngủ quả
                                                                        </li>
                                                                        <li style="text-align: left">
                                                                            Thiết kế mâm quả cưới theo nhu cầu khách
                                                                            hàng Việt Nam tại Vaughan, Canada theo vùng
                                                                            miền Bắc Trung Nam
                                                                        </li>
                                                                        <li style="text-align: left">
                                                                            Tư vấn mâm quả cưới cho gia đình Việt tại
                                                                            Canada
                                                                        </li>
                                                                        <li style="text-align: left">
                                                                            Dịch vụ chất lượng, tận tâm, nhiệt tình.
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- .et_pb_blurb_content -->
                                                    </div>
                                                    <!-- .et_pb_blurb -->
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                            <div class="et_pb_row et_pb_row_5">
                                                <div
                                                    class="et_pb_column et_pb_column_1_3 et_pb_column_14 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div
                                                        class="et_pb_with_border et_pb_module et_pb_blurb et_pb_blurb_9 et_pb_text_align_center et_pb_blurb_position_top et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_blurb_content">
                                                            <div class="et_pb_blurb_container">
                                                                <h3 class="et_pb_module_header">
                                                                    <span>Dịch vụ Chuyển tiền về Việt Nam</span>
                                                                </h3>
                                                                <div class="et_pb_blurb_description">
                                                                    <ul>
                                                                        <li>
                                                                            Giúp quý khách gởi tiền từ Canada về Việt
                                                                            Nam cho người thân và gia đình nhanh nhất
                                                                            nhanh chóng và tiện lợi
                                                                        </li>
                                                                        <li>
                                                                            Không mất thời gian chờ khi đến ngân hàng
                                                                        </li>
                                                                        <li>Có sẵn tại Vaughan, Ontario Canada</li>
                                                                        <li>
                                                                            Quý khách sẽ trả chi phí chuyền tiền thấp
                                                                            nhất, và tỷ giá nhận tiền cao nhất mà không
                                                                            có những ẩn phí khác.
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- .et_pb_blurb_content -->
                                                    </div>
                                                    <!-- .et_pb_blurb -->
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_3 et_pb_column_15 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div
                                                        class="et_pb_with_border et_pb_module et_pb_blurb et_pb_blurb_10 et_pb_text_align_center et_pb_blurb_position_top et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_blurb_content">
                                                            <div class="et_pb_blurb_container">
                                                                <h3 class="et_pb_module_header">
                                                                    <span>Dịch vụ bán quần áo</span>
                                                                </h3>
                                                                <div class="et_pb_blurb_description">
                                                                    <ul>
                                                                        <li style="text-align: left">
                                                                            Chuyên bán các loại trang phục may đo, thiết
                                                                            kế tại Vaughan
                                                                        </li>
                                                                        <li style="text-align: left">
                                                                            Cung cấp đủ các kiểu dáng thời trang
                                                                        </li>
                                                                        <li style="text-align: left">
                                                                            Địa điểm bán áo quần giá tốt
                                                                        </li>
                                                                        <li style="text-align: left">
                                                                            Cung cấp các loại trang phục phù hợp với
                                                                            thời tiết và khí hậu tại Canada
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- .et_pb_blurb_content -->
                                                    </div>
                                                    <!-- .et_pb_blurb -->
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_3 et_pb_column_16 et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div
                                                        class="et_pb_with_border et_pb_module et_pb_blurb et_pb_blurb_11 et_pb_text_align_center et_pb_blurb_position_top et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_blurb_content">
                                                            <div class="et_pb_blurb_container">
                                                                <h3 class="et_pb_module_header">
                                                                    <span>Các dịch vụ khác</span>
                                                                </h3>
                                                                <div class="et_pb_blurb_description">
                                                                    <ul>
                                                                        <li style="text-align: left">
                                                                            Cung cấp các loại dầu gió xanh
                                                                        </li>
                                                                        <li style="text-align: left">
                                                                            Địa điểm chuyên bán các loại thực phẩm chức
                                                                            năng
                                                                        </li>
                                                                        <li style="text-align: left">
                                                                            Cung cấp đa dạng các loại thực phẩm chức
                                                                            năng chăm sóc sức khỏe
                                                                        </li>
                                                                        <li style="text-align: left">
                                                                            Địa điểm bán sữa uy tín chất lượng và giá
                                                                            tốt
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- .et_pb_blurb_content -->
                                                    </div>
                                                    <!-- .et_pb_blurb -->
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                        </div>
                                        <!-- .et_pb_section -->
                                        <div class="et_pb_section et_pb_section_7 et_section_specialty">
                                            <div class="et_pb_row">
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_17 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div class="et_pb_module et_pb_image et_pb_image_1">
                                                        <span class="et_pb_image_wrap"
                                                            ><img
                                                                src="/wp-content/uploads/2021/03/DD-cleaners.jpg"
                                                                alt=""
                                                                title="D&amp;D-cleaners"
                                                                height="auto"
                                                                width="auto"
                                                                srcset="
                                                                    /wp-content/uploads/2021/03/DD-cleaners.jpg         800w,
                                                                    /wp-content/uploads/2021/03/DD-cleaners-480x235.jpg 480w
                                                                "
                                                                sizes="(min-width: 0px) and (max-width: 480px) 480px, (min-width: 481px) 800px, 100vw"
                                                                class="wp-image-425"
                                                        /></span>
                                                    </div>
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_18 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_7 et_clickable et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <h4><span style="color: #000000">Time</span></h4>
                                                            <p>
                                                                <span style="color: #000000"
                                                                    ><strong>Mon:</strong> 10:00am – 6:00pm</span
                                                                ><br /><span style="color: #000000">
                                                                    <strong>Tue:</strong> 10:00am – 6:00pm</span
                                                                ><br /><span style="color: #000000"
                                                                    ><strong>Wed:</strong> 10:00am – 7:00pm</span
                                                                ><br /><span style="color: #000000">
                                                                    <strong>Thu:</strong> 10:00am – 7:00pm</span
                                                                ><br /><span style="color: #000000"
                                                                    ><strong>Fri:</strong> 10:00am – 7:00pm</span
                                                                ><br /><span style="color: #000000"
                                                                    ><strong>Saturday: </strong>10:00am – 6:00pm<strong
                                                                        ><br /></strong></span
                                                                ><span style="color: #000000"
                                                                    ><strong>Sunday:</strong> 11:00am – 3:00pm</span
                                                                >
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_8 et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <h4>Contact</h4>
                                                            <p><a href="tel:9057389991">(905) 738-9991</a></p>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_9 et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <p>
                                                                <a href="mailto:dndcleaners.ca@gmail.com"
                                                                    >dndcleaners.ca@gmail.com</a
                                                                >
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_2 et_pb_column_19 et_pb_specialty_column et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div class="et_pb_row_inner et_pb_row_inner_2">
                                                        <div
                                                            class="et_pb_column et_pb_column_1_4 et_pb_column_inner et_pb_column_inner_3"
                                                        >
                                                            <div
                                                                class="et_pb_module et_pb_text et_pb_text_10 et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                            >
                                                                <div class="et_pb_text_inner">
                                                                    <h4><span style="color: #000000">Address</span></h4>
                                                                    <p>
                                                                        <span style="color: #000000"
                                                                            >3255 Rutherford Road, unit J19, Vaughan,
                                                                            ON, L4K 5Y5</span
                                                                        >
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <!-- .et_pb_text -->
                                                        </div>
                                                        <!-- .et_pb_column -->
                                                        <div
                                                            class="et_pb_column et_pb_column_1_4 et_pb_column_inner et_pb_column_inner_4 et-last-child"
                                                        >
                                                            <div
                                                                class="et_pb_module et_pb_text et_pb_text_11 et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                            >
                                                                <div class="et_pb_text_inner"><h4>Follow us</h4></div>
                                                            </div>
                                                            <!-- .et_pb_text -->
                                                            <ul
                                                                class="et_pb_module et_pb_social_media_follow et_pb_social_media_follow_1 clearfix et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                            >
                                                                <li
                                                                    class="et_pb_social_media_follow_network_2 et_pb_social_icon et_pb_social_network_link et-social-facebook et_pb_social_media_follow_network_2"
                                                                >
                                                                    <a
                                                                        href="https://www.facebook.com/ddcleanersvaughan/"
                                                                        class="icon et_pb_with_border"
                                                                        title="Theo dõi trên Facebook"
                                                                        target="_blank"
                                                                        ><span
                                                                            class="et_pb_social_media_follow_network_name"
                                                                            aria-hidden="true"
                                                                            >Theo dõi</span
                                                                        ></a
                                                                    >
                                                                </li>
                                                                <li
                                                                    class="et_pb_social_media_follow_network_3 et_pb_social_icon et_pb_social_network_link et-social-instagram et_pb_social_media_follow_network_3"
                                                                >
                                                                    <a
                                                                        href="https://www.instagram.com/dndcleaners/?fbclid=IwAR0Yj-nlZCUJA1omDZp5MbnGZqSKttQj9Bp77Fm2raYdHePUMit2LBmaL94"
                                                                        class="icon et_pb_with_border"
                                                                        title="Theo dõi trên Instagram"
                                                                        target="_blank"
                                                                        ><span
                                                                            class="et_pb_social_media_follow_network_name"
                                                                            aria-hidden="true"
                                                                            >Theo dõi</span
                                                                        ></a
                                                                    >
                                                                </li>
                                                            </ul>
                                                            <!-- .et_pb_counters -->
                                                        </div>
                                                        <!-- .et_pb_column -->
                                                    </div>
                                                    <!-- .et_pb_row_inner -->
                                                    <div class="et_pb_row_inner et_pb_row_inner_3">
                                                        <div
                                                            class="et_pb_column et_pb_column_4_4 et_pb_column_inner et_pb_column_inner_5 et-last-child"
                                                        >
                                                            <div
                                                                class="et_pb_with_border et_pb_module et_pb_signup_1 et_pb_newsletter_layout_top_bottom et_pb_newsletter et_pb_subscribe clearfix et_pb_text_align_left et_pb_bg_layout_dark et_pb_no_bg et_pb_newsletter_description_no_title et_pb_newsletter_description_no_content"
                                                            >
                                                                <div
                                                                    class="et_pb_newsletter_description et_multi_view_hidden"
                                                                ></div>

                                                                <div class="et_pb_newsletter_form">
                                                                    <form method="post">
                                                                        <div
                                                                            class="et_pb_newsletter_result et_pb_newsletter_error"
                                                                        ></div>
                                                                        <div
                                                                            class="et_pb_newsletter_result et_pb_newsletter_success"
                                                                        >
                                                                            <h2>Thông báo Thành công</h2>
                                                                        </div>
                                                                        <div class="et_pb_newsletter_fields">
                                                                            <p
                                                                                class="et_pb_newsletter_field et_pb_contact_field_last et_pb_contact_field_last_tablet et_pb_contact_field_last_phone"
                                                                            >
                                                                                <label
                                                                                    class="et_pb_contact_form_label"
                                                                                    for="et_pb_signup_email"
                                                                                    style="display: none"
                                                                                    >Email</label
                                                                                >
                                                                                <input
                                                                                    id="et_pb_signup_email"
                                                                                    class="input"
                                                                                    type="text"
                                                                                    placeholder="Email"
                                                                                    name="et_pb_signup_email"
                                                                                />
                                                                            </p>

                                                                            <p class="et_pb_newsletter_button_wrap">
                                                                                <a
                                                                                    class="et_pb_newsletter_button et_pb_button"
                                                                                    href="#"
                                                                                    data-icon=""
                                                                                >
                                                                                    <span
                                                                                        class="et_subscribe_loader"
                                                                                    ></span>
                                                                                    <span
                                                                                        class="et_pb_newsletter_button_text"
                                                                                        >Đăng ký</span
                                                                                    >
                                                                                </a>
                                                                            </p>
                                                                        </div>

                                                                        <input
                                                                            type="hidden"
                                                                            value="mailchimp"
                                                                            name="et_pb_signup_provider"
                                                                        />
                                                                        <input
                                                                            type="hidden"
                                                                            value="1ea2bbd026"
                                                                            name="et_pb_signup_list_id"
                                                                        />
                                                                        <input
                                                                            type="hidden"
                                                                            value="elegantthemestest"
                                                                            name="et_pb_signup_account_name"
                                                                        />
                                                                        <input
                                                                            type="hidden"
                                                                            value="true"
                                                                            name="et_pb_signup_ip_address"
                                                                        /><input
                                                                            type="hidden"
                                                                            value="c31b809bb99343fbd7464e3628d08cb0"
                                                                            name="et_pb_signup_checksum"
                                                                        />
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- .et_pb_column -->
                                                    </div>
                                                    <!-- .et_pb_row_inner -->
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                        </div>
                                        <!-- .et_pb_section -->
                                    </div>
                                    <!-- .et_builder_inner_content -->
                                </div>
                                <!-- .et-l -->
                            </div>
                            <!-- #et-boc -->
                        </div>
                        <!-- .entry-content -->
                    </article>
                    <!-- .et_pb_post -->
                </div>
                <!-- #main-content -->

                <span class="et_pb_scroll_top et-pb-icon"></span>

                <footer id="main-footer">
                    <div id="footer-bottom">
                        <div class="container clearfix">
                            <div id="footer-info">D&amp;D CLEANERS | Copyright © 2021. All Rights Reserved.</div>
                        </div>
                        <!-- .container -->
                    </div>
                </footer>
                <!-- #main-footer -->
            </div>
            <!-- #et-main-area -->
        </div>
        <!-- #page-container -->

        
        
        <script type="text/javascript" id="et-builder-modules-script-js-extra">
            /* <![CDATA[ */
            var et_frontend_scripts = { builderCssContainerPrefix: "#et-boc", builderCssLayoutPrefix: "#et-boc .et-l" };
            var et_pb_custom = {
                ajaxurl: "httpsstaging.dndcleaners.ca\/wp-admin\/admin-ajax.php",
                images_uri: "httpsstaging.dndcleaners.ca\/wp-content\/themes\/Divi\/images",
                builder_images_uri: "httpsstaging.dndcleaners.ca\/wp-content\/themes\/Divi\/includes\/builder\/images",
                et_frontend_nonce: "e724749a30",
                subscription_failed: "Làm ơn, kiểm tra đồng dưới chắc chắn để các ông gõ đúng thông tin.",
                et_ab_log_nonce: "25383f29c3",
                fill_message: "Xin vui lòng, điền vào các lĩnh vực sau đây:",
                contact_error_message: "Xin hãy sửa những lỗi sau:",
                invalid: "Không hợp lệ email",
                captcha: "Captcha",
                prev: "Trước",
                previous: "Trước",
                next: "Tiếp theo",
                wrong_captcha: "Bạn đã nhập sai số captcha.",
                wrong_checkbox: "Checkbox",
                ignore_waypoints: "no",
                is_divi_theme_used: "1",
                widget_search_selector: ".widget_search",
                ab_tests: [],
                is_ab_testing_active: "",
                page_id: "809",
                unique_test_id: "",
                ab_bounce_rate: "5",
                is_cache_plugin_active: "no",
                is_shortcode_tracking: "",
                tinymce_uri: "",
            };
            var et_pb_box_shadow_elements = [];
            var et_pb_motion_elements = { desktop: [], tablet: [], phone: [] };
            var et_pb_sticky_elements = [];
            /* ]]> */
        </script>
       @include('public.script')
        @include('service.style')
    </body>

    <!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/

Page Caching using disk: enhanced 

Served from: staging.dndcleaners.ca @ 2022-12-27 07:02:28 by W3 Total Cache
--></html>
