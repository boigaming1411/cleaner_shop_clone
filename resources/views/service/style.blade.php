<style id="et-builder-module-design-809-cached-inline-styles">
            div.et_pb_section.et_pb_section_0 {
                background-image: linear-gradient(180deg, rgba(43, 135, 218, 0.47) 0%, rgba(140, 41, 196, 0.6) 100%),
                    url(/wp-content/uploads/2021/03/1449-scaled.jpg) !important;
            }
            .et_pb_text_1.et_pb_text {
                color: RGBA(0, 0, 0, 0) !important;
            }
            .et_pb_text_6.et_pb_text {
                color: RGBA(0, 0, 0, 0) !important;
            }
            .et_pb_text_0.et_pb_text {
                color: RGBA(0, 0, 0, 0) !important;
            }
            .et_pb_text_9.et_pb_text {
                color: RGBA(0, 0, 0, 0) !important;
            }
            .et_pb_text_10.et_pb_text {
                color: RGBA(0, 0, 0, 0) !important;
            }
            .et_pb_text_11.et_pb_text {
                color: RGBA(0, 0, 0, 0) !important;
            }
            .et_pb_text_5.et_pb_text {
                color: RGBA(0, 0, 0, 0) !important;
            }
            .et_pb_text_4.et_pb_text {
                color: RGBA(0, 0, 0, 0) !important;
            }
            .et_pb_text_3.et_pb_text {
                color: RGBA(0, 0, 0, 0) !important;
            }
            .et_pb_text_7.et_pb_text {
                color: RGBA(0, 0, 0, 0) !important;
            }
            .et_pb_text_6 h1 {
                text-transform: uppercase;
                font-size: 50px;
            }
            .et_pb_text_0 h1 {
                text-transform: uppercase;
                font-size: 50px;
            }
            .et_pb_text_6 h2 {
                font-family: "Lato", Helvetica, Arial, Lucida, sans-serif;
                font-weight: 700;
                text-transform: uppercase;
                font-size: 55px;
                color: #ffffff !important;
                text-align: center;
                text-shadow: 0.08em 0.08em 0.08em rgba(0, 0, 0, 0.4);
            }
            .et_pb_text_0 h2 {
                font-family: "Lato", Helvetica, Arial, Lucida, sans-serif;
                font-weight: 700;
                text-transform: uppercase;
                font-size: 55px;
                color: #ffffff !important;
                text-align: center;
                text-shadow: 0.08em 0.08em 0.08em rgba(0, 0, 0, 0.4);
            }
            .et_pb_text_6 {
                background-color: RGBA(0, 0, 0, 0);
                position: relative;
                padding-top: 3em !important;
                padding-bottom: 3em !important;
            }
            .et_pb_text_0 {
                background-color: RGBA(0, 0, 0, 0);
                position: relative;
                padding-top: 3em !important;
                padding-bottom: 3em !important;
            }
            .et_pb_blurb_0.et_pb_blurb .et_pb_module_header,
            .et_pb_blurb_0.et_pb_blurb .et_pb_module_header a {
                font-family: "Lato", Helvetica, Arial, Lucida, sans-serif;
                font-weight: 700;
                text-transform: uppercase;
                font-size: 30px;
                color: #0c71c3 !important;
                letter-spacing: 1px;
                text-align: center;
            }
            .et_pb_blurb_6.et_pb_blurb .et_pb_module_header,
            .et_pb_blurb_6.et_pb_blurb .et_pb_module_header a {
                font-family: "Lato", Helvetica, Arial, Lucida, sans-serif;
                font-weight: 700;
                text-transform: uppercase;
                font-size: 30px;
                color: #0c71c3 !important;
                letter-spacing: 1px;
                text-align: center;
            }
            .et_pb_blurb_1.et_pb_blurb {
                font-size: 17px;
                border-radius: 5px 5px 5px 5px;
                overflow: hidden;
                border-width: 1px;
                padding-top: 1em !important;
                padding-right: 1em !important;
                padding-bottom: 1em !important;
                padding-left: 1em !important;
            }
            .et_pb_blurb_11.et_pb_blurb {
                font-size: 17px;
                border-radius: 5px 5px 5px 5px;
                overflow: hidden;
                border-width: 1px;
                padding-top: 1em !important;
                padding-right: 1em !important;
                padding-bottom: 1em !important;
                padding-left: 1em !important;
            }
            .et_pb_blurb_7.et_pb_blurb {
                font-size: 17px;
                border-radius: 5px 5px 5px 5px;
                overflow: hidden;
                border-width: 1px;
                padding-top: 1em !important;
                padding-right: 1em !important;
                padding-bottom: 1em !important;
                padding-left: 1em !important;
            }
            .et_pb_blurb_9.et_pb_blurb {
                font-size: 17px;
                border-radius: 5px 5px 5px 5px;
                overflow: hidden;
                border-width: 1px;
                padding-top: 1em !important;
                padding-right: 1em !important;
                padding-bottom: 1em !important;
                padding-left: 1em !important;
            }
            .et_pb_blurb_5.et_pb_blurb {
                font-size: 17px;
                border-radius: 5px 5px 5px 5px;
                overflow: hidden;
                border-width: 1px;
                padding-top: 1em !important;
                padding-right: 1em !important;
                padding-bottom: 1em !important;
                padding-left: 1em !important;
            }
            .et_pb_blurb_6.et_pb_blurb {
                font-size: 17px;
                border-radius: 5px 5px 5px 5px;
                overflow: hidden;
                border-width: 1px;
                padding-top: 1em !important;
                padding-right: 1em !important;
                padding-bottom: 1em !important;
                padding-left: 1em !important;
            }
            .et_pb_blurb_10.et_pb_blurb {
                font-size: 17px;
                border-radius: 5px 5px 5px 5px;
                overflow: hidden;
                border-width: 1px;
                padding-top: 1em !important;
                padding-right: 1em !important;
                padding-bottom: 1em !important;
                padding-left: 1em !important;
            }
            .et_pb_blurb_0.et_pb_blurb {
                font-size: 17px;
                border-radius: 5px 5px 5px 5px;
                overflow: hidden;
                border-width: 1px;
                padding-top: 1em !important;
                padding-right: 1em !important;
                padding-bottom: 1em !important;
                padding-left: 1em !important;
            }
            .et_pb_blurb_2.et_pb_blurb {
                font-size: 17px;
                border-radius: 5px 5px 5px 5px;
                overflow: hidden;
                border-width: 1px;
                padding-top: 1em !important;
                padding-right: 1em !important;
                padding-bottom: 1em !important;
                padding-left: 1em !important;
            }
            .et_pb_blurb_4.et_pb_blurb {
                font-size: 17px;
                border-radius: 5px 5px 5px 5px;
                overflow: hidden;
                border-width: 1px;
                padding-top: 1em !important;
                padding-right: 1em !important;
                padding-bottom: 1em !important;
                padding-left: 1em !important;
            }
            .et_pb_blurb_3.et_pb_blurb {
                font-size: 17px;
                border-radius: 5px 5px 5px 5px;
                overflow: hidden;
                border-width: 1px;
                padding-top: 1em !important;
                padding-right: 1em !important;
                padding-bottom: 1em !important;
                padding-left: 1em !important;
            }
            .et_pb_blurb_8.et_pb_blurb {
                font-size: 17px;
                border-radius: 5px 5px 5px 5px;
                overflow: hidden;
                border-width: 1px;
                padding-top: 1em !important;
                padding-right: 1em !important;
                padding-bottom: 1em !important;
                padding-left: 1em !important;
            }
            .et_pb_blurb_9 {
                box-shadow: 0px 12px 18px -6px rgba(0, 0, 0, 0.3);
            }
            .et_pb_blurb_10 {
                box-shadow: 0px 12px 18px -6px rgba(0, 0, 0, 0.3);
            }
            .et_pb_blurb_11 {
                box-shadow: 0px 12px 18px -6px rgba(0, 0, 0, 0.3);
            }
            .et_pb_blurb_1 {
                box-shadow: 0px 12px 18px -6px rgba(0, 0, 0, 0.3);
            }
            .et_pb_blurb_8 {
                box-shadow: 0px 12px 18px -6px rgba(0, 0, 0, 0.3);
            }
            .et_pb_blurb_7 {
                box-shadow: 0px 12px 18px -6px rgba(0, 0, 0, 0.3);
            }
            .et_pb_blurb_6 {
                box-shadow: 0px 12px 18px -6px rgba(0, 0, 0, 0.3);
            }
            .et_pb_blurb_5 {
                box-shadow: 0px 12px 18px -6px rgba(0, 0, 0, 0.3);
            }
            .et_pb_blurb_4 {
                box-shadow: 0px 12px 18px -6px rgba(0, 0, 0, 0.3);
            }
            .et_pb_blurb_3 {
                box-shadow: 0px 12px 18px -6px rgba(0, 0, 0, 0.3);
            }
            .et_pb_blurb_2 {
                box-shadow: 0px 12px 18px -6px rgba(0, 0, 0, 0.3);
            }
            .et_pb_blurb_0 {
                box-shadow: 0px 12px 18px -6px rgba(0, 0, 0, 0.3);
            }
            .et_pb_blurb_9.et_pb_blurb .et_pb_module_header,
            .et_pb_blurb_9.et_pb_blurb .et_pb_module_header a {
                font-family: "Lato", Helvetica, Arial, Lucida, sans-serif;
                font-weight: 700;
                text-transform: uppercase;
                font-size: 30px;
                color: #0c71c3 !important;
                text-align: center;
            }
            .et_pb_blurb_8.et_pb_blurb .et_pb_module_header,
            .et_pb_blurb_8.et_pb_blurb .et_pb_module_header a {
                font-family: "Lato", Helvetica, Arial, Lucida, sans-serif;
                font-weight: 700;
                text-transform: uppercase;
                font-size: 30px;
                color: #0c71c3 !important;
                text-align: center;
            }
            .et_pb_blurb_7.et_pb_blurb .et_pb_module_header,
            .et_pb_blurb_7.et_pb_blurb .et_pb_module_header a {
                font-family: "Lato", Helvetica, Arial, Lucida, sans-serif;
                font-weight: 700;
                text-transform: uppercase;
                font-size: 30px;
                color: #0c71c3 !important;
                text-align: center;
            }
            .et_pb_blurb_10.et_pb_blurb .et_pb_module_header,
            .et_pb_blurb_10.et_pb_blurb .et_pb_module_header a {
                font-family: "Lato", Helvetica, Arial, Lucida, sans-serif;
                font-weight: 700;
                text-transform: uppercase;
                font-size: 30px;
                color: #0c71c3 !important;
                text-align: center;
            }
            .et_pb_blurb_11.et_pb_blurb .et_pb_module_header,
            .et_pb_blurb_11.et_pb_blurb .et_pb_module_header a {
                font-family: "Lato", Helvetica, Arial, Lucida, sans-serif;
                font-weight: 700;
                text-transform: uppercase;
                font-size: 30px;
                color: #0c71c3 !important;
                text-align: center;
            }
            .et_pb_blurb_2.et_pb_blurb .et_pb_module_header,
            .et_pb_blurb_2.et_pb_blurb .et_pb_module_header a {
                font-family: "Lato", Helvetica, Arial, Lucida, sans-serif;
                font-weight: 700;
                text-transform: uppercase;
                font-size: 30px;
                color: #0c71c3 !important;
                text-align: center;
            }
            .et_pb_blurb_1.et_pb_blurb .et_pb_module_header,
            .et_pb_blurb_1.et_pb_blurb .et_pb_module_header a {
                font-family: "Lato", Helvetica, Arial, Lucida, sans-serif;
                font-weight: 700;
                text-transform: uppercase;
                font-size: 30px;
                color: #0c71c3 !important;
                text-align: center;
            }
            .et_pb_blurb_3.et_pb_blurb .et_pb_module_header,
            .et_pb_blurb_3.et_pb_blurb .et_pb_module_header a {
                font-family: "Lato", Helvetica, Arial, Lucida, sans-serif;
                font-weight: 700;
                text-transform: uppercase;
                font-size: 30px;
                color: #0c71c3 !important;
                text-align: center;
            }
            .et_pb_blurb_4.et_pb_blurb .et_pb_module_header,
            .et_pb_blurb_4.et_pb_blurb .et_pb_module_header a {
                font-family: "Lato", Helvetica, Arial, Lucida, sans-serif;
                font-weight: 700;
                text-transform: uppercase;
                font-size: 30px;
                color: #0c71c3 !important;
                text-align: center;
            }
            .et_pb_blurb_5.et_pb_blurb .et_pb_module_header,
            .et_pb_blurb_5.et_pb_blurb .et_pb_module_header a {
                font-family: "Lato", Helvetica, Arial, Lucida, sans-serif;
                font-weight: 700;
                text-transform: uppercase;
                font-size: 30px;
                color: #0c71c3 !important;
                text-align: center;
            }
            .et_pb_blurb_3.et_pb_blurb .et_pb_blurb_description {
                text-align: left;
            }
            .et_pb_blurb_9.et_pb_blurb .et_pb_blurb_description {
                text-align: left;
            }
            .et_pb_section_6 > .et_pb_row {
                max-width: 1200px;
            }
            .et_pb_section_2 > .et_pb_row {
                max-width: 1200px;
            }
            .et_pb_section_2.et_section_specialty > .et_pb_row {
                max-width: 1200px;
            }
            .et_pb_section_3 > .et_pb_row {
                max-width: 1200px;
            }
            .et_pb_section_3.et_section_specialty > .et_pb_row {
                max-width: 1200px;
            }
            .et_pb_section_6.et_section_specialty > .et_pb_row {
                max-width: 1200px;
            }
            .et_pb_section_7 > .et_pb_row {
                max-width: 1200px;
            }
            .et_pb_section_7.et_section_specialty > .et_pb_row {
                max-width: 1200px;
            }
            .et_pb_image_1 {
                text-align: left;
                margin-left: 0;
            }
            .et_pb_image_0 {
                text-align: left;
                margin-left: 0;
            }
            .et_pb_text_9 p {
                line-height: 2em;
            }
            .et_pb_text_2 p {
                line-height: 2em;
            }
            .et_pb_text_7 p {
                line-height: 2em;
            }
            .et_pb_text_8 p {
                line-height: 2em;
            }
            .et_pb_text_3 p {
                line-height: 2em;
            }
            .et_pb_text_1 p {
                line-height: 2em;
            }
            .et_pb_text_1 {
                line-height: 2em;
                background-color: RGBA(0, 0, 0, 0);
                position: relative;
            }
            .et_pb_text_7 {
                line-height: 2em;
                background-color: RGBA(0, 0, 0, 0);
                position: relative;
            }
            .et_pb_text_1.et_pb_text a {
                color: #000000 !important;
            }
            .et_pb_text_7.et_pb_text a {
                color: #000000 !important;
            }
            .et_pb_text_7 h4 {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 1.2em;
            }
            .et_pb_text_2 h4 {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 1.2em;
            }
            .et_pb_text_3 h4 {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 1.2em;
            }
            .et_pb_text_4 h4 {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 1.2em;
            }
            .et_pb_text_9 h4 {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 1.2em;
            }
            .et_pb_text_10 h4 {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 1.2em;
            }
            .et_pb_text_5 h4 {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 1.2em;
            }
            .et_pb_text_11 h4 {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 1.2em;
            }
            .et_pb_text_8 h4 {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 1.2em;
            }
            .et_pb_text_1 h4 {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 1.2em;
            }
            .et_pb_text_8.et_pb_text {
                color: #2f4773 !important;
            }
            .et_pb_text_2.et_pb_text {
                color: #2f4773 !important;
            }
            .et_pb_text_8 {
                font-weight: 700;
                font-size: 30px;
                line-height: 2em;
                background-color: RGBA(0, 0, 0, 0);
                position: relative;
                margin-bottom: 0px !important;
            }
            .et_pb_text_2 {
                font-weight: 700;
                font-size: 30px;
                line-height: 2em;
                background-color: RGBA(0, 0, 0, 0);
                position: relative;
                margin-bottom: 0px !important;
            }
            .et_pb_text_3 {
                font-size: 16px;
                line-height: 2em;
                background-color: RGBA(0, 0, 0, 0);
                position: relative;
            }
            .et_pb_text_9 {
                font-size: 16px;
                line-height: 2em;
                background-color: RGBA(0, 0, 0, 0);
                position: relative;
            }
            .et_pb_text_9.et_pb_text a {
                color: #0c71c3 !important;
            }
            .et_pb_text_3.et_pb_text a {
                color: #0c71c3 !important;
            }
            .et_pb_text_9 a {
                font-size: 16px;
                line-height: 1.2em;
            }
            .et_pb_text_3 a {
                font-size: 16px;
                line-height: 1.2em;
            }
            .et_pb_text_10 {
                background-color: RGBA(0, 0, 0, 0);
                position: relative;
            }
            .et_pb_text_4 {
                background-color: RGBA(0, 0, 0, 0);
                position: relative;
            }
            .et_pb_text_11 {
                background-color: RGBA(0, 0, 0, 0);
                position: relative;
                margin-bottom: 0px !important;
            }
            .et_pb_text_5 {
                background-color: RGBA(0, 0, 0, 0);
                position: relative;
                margin-bottom: 0px !important;
            }
            .et_pb_social_media_follow_1 li.et_pb_social_icon a.icon:before {
                color: #000000;
            }
            .et_pb_social_media_follow_0 li.et_pb_social_icon a.icon:before {
                color: #000000;
            }
            .et_pb_signup_1.et_pb_subscribe .et_pb_newsletter_form .input {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 2em;
            }
            .et_pb_signup_1.et_pb_subscribe .et_pb_newsletter_form .input:-ms-input-placeholder {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 2em;
            }
            .et_pb_signup_1.et_pb_contact_field .et_pb_contact_field_options_title {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 2em;
            }
            .et_pb_signup_1.et_pb_subscribe .et_pb_newsletter_form .input::-webkit-input-placeholder {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 2em;
            }
            .et_pb_signup_1.et_pb_subscribe .et_pb_newsletter_form .input[type="checkbox"] + label {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 2em;
            }
            .et_pb_signup_1.et_pb_subscribe .et_pb_newsletter_form .input[type="radio"] + label {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 2em;
            }
            .et_pb_signup_0.et_pb_contact_field .et_pb_contact_field_options_title {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 2em;
            }
            .et_pb_signup_0.et_pb_subscribe .et_pb_newsletter_form .input {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 2em;
            }
            .et_pb_signup_0.et_pb_subscribe .et_pb_newsletter_form .input::-webkit-input-placeholder {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 2em;
            }
            .et_pb_signup_0.et_pb_subscribe .et_pb_newsletter_form .input::-moz-placeholder {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 2em;
            }
            .et_pb_signup_0.et_pb_subscribe .et_pb_newsletter_form .input:-ms-input-placeholder {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 2em;
            }
            .et_pb_signup_1.et_pb_subscribe .et_pb_newsletter_form .input::-moz-placeholder {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 2em;
            }
            .et_pb_signup_0.et_pb_subscribe .et_pb_newsletter_form .input[type="checkbox"] + label {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 2em;
            }
            .et_pb_signup_0.et_pb_subscribe .et_pb_newsletter_form .input[type="radio"] + label {
                font-weight: 700;
                text-transform: uppercase;
                font-size: 14px;
                letter-spacing: 3px;
                line-height: 2em;
            }
            .et_pb_signup_0 .et_pb_newsletter_form p input[type="text"],
            .et_pb_signup_0 .et_pb_newsletter_form p textarea,
            .et_pb_signup_0 .et_pb_newsletter_form p select,
            .et_pb_signup_0 .et_pb_newsletter_form p .input[type="radio"] + label i,
            .et_pb_signup_0 .et_pb_newsletter_form p .input[type="checkbox"] + label i {
                border-radius: 0px 0px 0px 0px;
                overflow: hidden;
                border-bottom-width: 2px;
            }
            .et_pb_signup_1 .et_pb_newsletter_form p input[type="text"],
            .et_pb_signup_1 .et_pb_newsletter_form p textarea,
            .et_pb_signup_1 .et_pb_newsletter_form p select,
            .et_pb_signup_1 .et_pb_newsletter_form p .input[type="radio"] + label i,
            .et_pb_signup_1 .et_pb_newsletter_form p .input[type="checkbox"] + label i {
                border-radius: 0px 0px 0px 0px;
                overflow: hidden;
                border-bottom-width: 2px;
            }
            body #page-container .et_pb_section .et_pb_signup_0.et_pb_subscribe .et_pb_newsletter_button.et_pb_button {
                color: #ffffff !important;
                border-width: 0px !important;
                border-radius: 0px;
                letter-spacing: 3px;
                font-size: 14px;
                font-weight: 700 !important;
                text-transform: uppercase !important;
                background-color: #000000;
                padding-top: 10px !important;
                padding-right: 20px !important;
                padding-bottom: 10px !important;
                padding-left: 20px !important;
            }
            body #page-container .et_pb_section .et_pb_signup_1.et_pb_subscribe .et_pb_newsletter_button.et_pb_button {
                color: #ffffff !important;
                border-width: 0px !important;
                border-radius: 0px;
                letter-spacing: 3px;
                font-size: 14px;
                font-weight: 700 !important;
                text-transform: uppercase !important;
                background-color: #000000;
                padding-top: 10px !important;
                padding-right: 20px !important;
                padding-bottom: 10px !important;
                padding-left: 20px !important;
            }
            body
                #page-container
                .et_pb_section
                .et_pb_signup_0.et_pb_subscribe
                .et_pb_newsletter_button.et_pb_button:after {
                font-size: 1.6em;
            }
            body
                #page-container
                .et_pb_section
                .et_pb_signup_1.et_pb_subscribe
                .et_pb_newsletter_button.et_pb_button:after {
                font-size: 1.6em;
            }
            body.et_button_custom_icon
                #page-container
                .et_pb_signup_1.et_pb_subscribe
                .et_pb_newsletter_button.et_pb_button:after {
                font-size: 14px;
            }
            body.et_button_custom_icon
                #page-container
                .et_pb_signup_0.et_pb_subscribe
                .et_pb_newsletter_button.et_pb_button:after {
                font-size: 14px;
            }
            .et_pb_signup_0 .et_pb_newsletter_form p input[type="text"],
            .et_pb_signup_0 .et_pb_newsletter_form p textarea,
            .et_pb_signup_0 .et_pb_newsletter_form p select,
            .et_pb_signup_0 .et_pb_newsletter_form p .input[type="checkbox"] + label i,
            .et_pb_signup_0 .et_pb_newsletter_form p .input[type="radio"] + label i {
                background-color: rgba(0, 0, 0, 0);
            }
            .et_pb_signup_1 .et_pb_newsletter_form p input[type="text"],
            .et_pb_signup_1 .et_pb_newsletter_form p textarea,
            .et_pb_signup_1 .et_pb_newsletter_form p select,
            .et_pb_signup_1 .et_pb_newsletter_form p .input[type="checkbox"] + label i,
            .et_pb_signup_1 .et_pb_newsletter_form p .input[type="radio"] + label i {
                background-color: rgba(0, 0, 0, 0);
            }
            .et_pb_signup_0 .et_pb_newsletter_form p .input::-ms-input-placeholder {
                color: #000000 !important;
            }
            .et_pb_signup_1 .et_pb_newsletter_form p .input::-moz-placeholder {
                color: #000000 !important;
            }
            .et_pb_signup_1 .et_pb_newsletter_form p .input::-ms-input-placeholder {
                color: #000000 !important;
            }
            .et_pb_signup_1 .et_pb_newsletter_form p .input::-webkit-input-placeholder {
                color: #000000 !important;
            }
            .et_pb_signup_0 .et_pb_newsletter_form p .input::-moz-placeholder {
                color: #000000 !important;
            }
            .et_pb_signup_0 .et_pb_newsletter_form p .input::-webkit-input-placeholder {
                color: #000000 !important;
            }
            .et_pb_signup_0 .et_pb_newsletter_form p input[type="text"],
            .et_pb_signup_0 .et_pb_newsletter_form p textarea,
            .et_pb_signup_0 .et_pb_newsletter_form p select,
            .et_pb_signup_0 .et_pb_newsletter_form p .input[type="checkbox"] + label i:before,
            .et_pb_signup_0 .et_pb_newsletter_form p .input::placeholder {
                color: #000000 !important;
            }
            .et_pb_signup_1 .et_pb_newsletter_form p input[type="text"],
            .et_pb_signup_1 .et_pb_newsletter_form p textarea,
            .et_pb_signup_1 .et_pb_newsletter_form p select,
            .et_pb_signup_1 .et_pb_newsletter_form p .input[type="checkbox"] + label i:before,
            .et_pb_signup_1 .et_pb_newsletter_form p .input::placeholder {
                color: #000000 !important;
            }
            .et_pb_signup_0.et_pb_subscribe .et_pb_newsletter_form {
                padding: 0 !important;
            }
            .et_pb_signup_1.et_pb_subscribe .et_pb_newsletter_form {
                padding: 0 !important;
            }
            .et_pb_signup_1 .et_pb_newsletter_form p .input[type="radio"] + label i:before {
                background-color: #000000;
            }
            .et_pb_signup_0 .et_pb_newsletter_form p .input[type="radio"] + label i:before {
                background-color: #000000;
            }
            div.et_pb_section.et_pb_section_4 {
                background-image: linear-gradient(180deg, rgba(43, 135, 218, 0.47) 0%, rgba(140, 41, 196, 0.6) 100%),
                    url(/wp-content/uploads/2021/03/1449-scaled.jpg) !important;
            }
            .et_pb_social_media_follow_network_3 a.icon {
                background-color: rgba(0, 0, 0, 0) !important;
            }
            .et_pb_social_media_follow_network_1 a.icon {
                background-color: rgba(0, 0, 0, 0) !important;
            }
            @media only screen and (max-width: 980px) {
                .et_pb_text_2 {
                    font-size: 24px;
                }
                .et_pb_text_8 {
                    font-size: 24px;
                }
                .et_pb_text_3 {
                    font-size: 14px;
                }
                .et_pb_text_3 a {
                    font-size: 14px;
                }
                .et_pb_text_5 {
                    font-size: 14px;
                }
                .et_pb_text_9 {
                    font-size: 14px;
                }
                .et_pb_text_9 a {
                    font-size: 14px;
                }
                .et_pb_text_11 {
                    font-size: 14px;
                }
                .et_pb_signup_0 .et_pb_newsletter_form p input[type="text"],
                .et_pb_signup_0 .et_pb_newsletter_form p textarea,
                .et_pb_signup_0 .et_pb_newsletter_form p select,
                .et_pb_signup_0 .et_pb_newsletter_form p .input[type="radio"] + label i,
                .et_pb_signup_0 .et_pb_newsletter_form p .input[type="checkbox"] + label i {
                    border-bottom-width: 2px;
                }
                .et_pb_signup_1 .et_pb_newsletter_form p input[type="text"],
                .et_pb_signup_1 .et_pb_newsletter_form p textarea,
                .et_pb_signup_1 .et_pb_newsletter_form p select,
                .et_pb_signup_1 .et_pb_newsletter_form p .input[type="radio"] + label i,
                .et_pb_signup_1 .et_pb_newsletter_form p .input[type="checkbox"] + label i {
                    border-bottom-width: 2px;
                }
                body
                    #page-container
                    .et_pb_section
                    .et_pb_signup_0.et_pb_subscribe
                    .et_pb_newsletter_button.et_pb_button:after {
                    display: inline-block;
                    opacity: 0;
                }
                body
                    #page-container
                    .et_pb_section
                    .et_pb_signup_1.et_pb_subscribe
                    .et_pb_newsletter_button.et_pb_button:after {
                    display: inline-block;
                    opacity: 0;
                }
                body
                    #page-container
                    .et_pb_section
                    .et_pb_signup_0.et_pb_subscribe
                    .et_pb_newsletter_button.et_pb_button:hover:after {
                    opacity: 1;
                }
                body
                    #page-container
                    .et_pb_section
                    .et_pb_signup_1.et_pb_subscribe
                    .et_pb_newsletter_button.et_pb_button:hover:after {
                    opacity: 1;
                }
            }
            @media only screen and (max-width: 767px) {
                .et_pb_text_2 {
                    font-size: 20px;
                }
                .et_pb_text_8 {
                    font-size: 20px;
                }
                .et_pb_signup_0 .et_pb_newsletter_form p input[type="text"],
                .et_pb_signup_0 .et_pb_newsletter_form p textarea,
                .et_pb_signup_0 .et_pb_newsletter_form p select,
                .et_pb_signup_0 .et_pb_newsletter_form p .input[type="radio"] + label i,
                .et_pb_signup_0 .et_pb_newsletter_form p .input[type="checkbox"] + label i {
                    border-bottom-width: 2px;
                }
                .et_pb_signup_1 .et_pb_newsletter_form p input[type="text"],
                .et_pb_signup_1 .et_pb_newsletter_form p textarea,
                .et_pb_signup_1 .et_pb_newsletter_form p select,
                .et_pb_signup_1 .et_pb_newsletter_form p .input[type="radio"] + label i,
                .et_pb_signup_1 .et_pb_newsletter_form p .input[type="checkbox"] + label i {
                    border-bottom-width: 2px;
                }
                body
                    #page-container
                    .et_pb_section
                    .et_pb_signup_0.et_pb_subscribe
                    .et_pb_newsletter_button.et_pb_button:after {
                    display: inline-block;
                    opacity: 0;
                }
                body
                    #page-container
                    .et_pb_section
                    .et_pb_signup_1.et_pb_subscribe
                    .et_pb_newsletter_button.et_pb_button:after {
                    display: inline-block;
                    opacity: 0;
                }
                body
                    #page-container
                    .et_pb_section
                    .et_pb_signup_0.et_pb_subscribe
                    .et_pb_newsletter_button.et_pb_button:hover:after {
                    opacity: 1;
                }
                body
                    #page-container
                    .et_pb_section
                    .et_pb_signup_1.et_pb_subscribe
                    .et_pb_newsletter_button.et_pb_button:hover:after {
                    opacity: 1;
                }
            }
        </style>