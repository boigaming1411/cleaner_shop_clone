<!DOCTYPE html>
<html dir="ltr" lang="vi" prefix="og: https://ogp.me/ns#">
    <head>
       
        <title>Liên hệ chúng tôi - DND Cleaners</title>

        <!-- All in One SEO 4.2.0 -->
        <meta
            name="description"
            content="Liên hệ (905) 738-9991 Nhắn tin cho chúng tôi Tên Email Nội dung Gửi Time Mon: 10:00am – 6:00pm Tue: 10:00am – 6:00pmWed: 10:00am – 7:00pm Thu: 10:00am – 7:00pmFri: 10:00am – 7:00pmSaturday: 10:00am – 6:00pmSunday: 11:00am – 3:00pm Contact (905) 738-9991 dndcleaners.ca@gmail.com Address 3255 Rutherford Road, unit J19, Vaughan, ON, L4K 5Y5 Follow us Theo"
        />
        <meta name="robots" content="noindex, nofollow, max-image-preview:large" />
        <link rel="canonical" href="/contact-us/" />
        <meta property="og:locale" content="vi_VN" />
        <meta property="og:site_name" content="DND Cleaners - Professional Alternations &amp; Repairs" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="Liên hệ chúng tôi - DND Cleaners" />
        <meta
            property="og:description"
            content="Liên hệ (905) 738-9991 Nhắn tin cho chúng tôi Tên Email Nội dung Gửi Time Mon: 10:00am – 6:00pm Tue: 10:00am – 6:00pmWed: 10:00am – 7:00pm Thu: 10:00am – 7:00pmFri: 10:00am – 7:00pmSaturday: 10:00am – 6:00pmSunday: 11:00am – 3:00pm Contact (905) 738-9991 dndcleaners.ca@gmail.com Address 3255 Rutherford Road, unit J19, Vaughan, ON, L4K 5Y5 Follow us Theo"
        />
        <meta property="og:url" content="/contact-us/" />
        <meta property="article:published_time" content="2021-03-02T22:06:27+00:00" />
        <meta property="article:modified_time" content="2021-12-20T04:27:52+00:00" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="Liên hệ chúng tôi - DND Cleaners" />
        <meta
            name="twitter:description"
            content="Liên hệ (905) 738-9991 Nhắn tin cho chúng tôi Tên Email Nội dung Gửi Time Mon: 10:00am – 6:00pm Tue: 10:00am – 6:00pmWed: 10:00am – 7:00pm Thu: 10:00am – 7:00pmFri: 10:00am – 7:00pmSaturday: 10:00am – 6:00pmSunday: 11:00am – 3:00pm Contact (905) 738-9991 dndcleaners.ca@gmail.com Address 3255 Rutherford Road, unit J19, Vaughan, ON, L4K 5Y5 Follow us Theo"
        />
        <script type="application/ld+json" class="aioseo-schema">
            {
                "@context": "https:\/\/schema.org",
                "@graph": [
                    {
                        "@type": "WebSite",
                        "@id": "httpsstaging.dndcleaners.ca\/#website",
                        "url": "httpsstaging.dndcleaners.ca\/",
                        "name": "DND Cleaners",
                        "description": "Professional Alternations & Repairs",
                        "inLanguage": "vi",
                        "publisher": { "@id": "httpsstaging.dndcleaners.ca\/#organization" }
                    },
                    {
                        "@type": "Organization",
                        "@id": "httpsstaging.dndcleaners.ca\/#organization",
                        "name": "DND Cleaners",
                        "url": "httpsstaging.dndcleaners.ca\/"
                    },
                    {
                        "@type": "BreadcrumbList",
                        "@id": "httpsstaging.dndcleaners.ca\/contact-us\/#breadcrumblist",
                        "itemListElement": [
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/#listItem",
                                "position": 1,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/",
                                    "name": "Home",
                                    "description": "D&D Cleaners chuyên may, cho thuê áo cưới, sửa áo quần, giặt ủi, bán dầu gió, thực phẩm chức năng tại Vaughan, Ontario, Canada",
                                    "url": "httpsstaging.dndcleaners.ca\/"
                                },
                                "nextItem": "httpsstaging.dndcleaners.ca\/contact-us\/#listItem"
                            },
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/contact-us\/#listItem",
                                "position": 2,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/contact-us\/",
                                    "name": "Liên hệ chúng tôi",
                                    "description": "Liên hệ (905) 738-9991 Nhắn tin cho chúng tôi Tên Email Nội dung Gửi Time Mon: 10:00am – 6:00pm Tue: 10:00am – 6:00pmWed: 10:00am – 7:00pm Thu: 10:00am – 7:00pmFri: 10:00am – 7:00pmSaturday: 10:00am – 6:00pmSunday: 11:00am – 3:00pm Contact (905) 738-9991 dndcleaners.ca@gmail.com Address 3255 Rutherford Road, unit J19, Vaughan, ON, L4K 5Y5 Follow us Theo",
                                    "url": "httpsstaging.dndcleaners.ca\/contact-us\/"
                                },
                                "previousItem": "httpsstaging.dndcleaners.ca\/#listItem"
                            }
                        ]
                    },
                    {
                        "@type": "WebPage",
                        "@id": "httpsstaging.dndcleaners.ca\/contact-us\/#webpage",
                        "url": "httpsstaging.dndcleaners.ca\/contact-us\/",
                        "name": "Liên hệ chúng tôi - DND Cleaners",
                        "description": "Liên hệ (905) 738-9991 Nhắn tin cho chúng tôi Tên Email Nội dung Gửi Time Mon: 10:00am – 6:00pm Tue: 10:00am – 6:00pmWed: 10:00am – 7:00pm Thu: 10:00am – 7:00pmFri: 10:00am – 7:00pmSaturday: 10:00am – 6:00pmSunday: 11:00am – 3:00pm Contact (905) 738-9991 dndcleaners.ca@gmail.com Address 3255 Rutherford Road, unit J19, Vaughan, ON, L4K 5Y5 Follow us Theo",
                        "inLanguage": "vi",
                        "isPartOf": { "@id": "httpsstaging.dndcleaners.ca\/#website" },
                        "breadcrumb": { "@id": "httpsstaging.dndcleaners.ca\/contact-us\/#breadcrumblist" },
                        "datePublished": "2021-03-02T22:06:27+00:00",
                        "dateModified": "2021-12-20T04:27:52+00:00"
                    }
                ]
            }
        </script>
        <!-- All in One SEO -->

        <link rel="alternate" href="/contact-us/" hreflang="vi" />
        <link rel="alternate" href="/en/contact-us/" hreflang="en" />
        @include('public.header')
    </head>
    {{$current='contact-us'}}
    <body
        class="page-template-default page page-id-294 theme-Divi woocommerce-no-js et_pb_button_helper_class et_fixed_nav et_show_nav et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_header_style_left et_pb_footer_columns4 et_cover_background et_pb_gutter et_pb_gutters3 et_pb_pagebuilder_layout et_smooth_scroll et_no_sidebar et_divi_theme et-db"
    >
        <div id="page-container">
            <header id="main-header" data-height-onload="66">
                <div class="container clearfix et_menu_container">
                    <div class="logo_container">
                        <span class="logo_helper"></span>
                        <a href="/">
                            <img
                                src="/wp-content/uploads/2021/03/DD-cleaners.jpg"
                                alt="DND Cleaners"
                                id="logo"
                                data-height-percentage="87"
                            />
                        </a>
                    </div>
                    <div id="et-top-navigation" data-height="66" data-fixed-height="40">
                        @include('public.navbar')
                        <a href="/cart/" class="et-cart-info">
                            <span></span>
                        </a>

                        <div id="et_mobile_nav_menu">
                            <div class="mobile_nav closed">
                                <span class="select_page">Chọn trang</span>
                                <span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
                            </div>
                        </div>
                    </div>
                    <!-- #et-top-navigation -->
                </div>
                <!-- .container -->
                <div class="et_search_outer">
                    <div class="container et_search_form_container">
                        <form
                            role="search"
                            method="get"
                            class="et-search-form"
                            action="https://staging.dndcleaners.ca/"
                        >
                            <input
                                type="search"
                                class="et-search-field"
                                placeholder="Tìm kiếm &hellip;"
                                value=""
                                name="s"
                                title="Tìm:"
                            />
                        </form>
                        <span class="et_close_search_field"></span>
                    </div>
                </div>
            </header>
            <!-- #main-header -->
            <div id="et-main-area">
                <div id="main-content">
                    <article id="post-294" class="post-294 page type-page status-publish hentry">
                        <div class="entry-content">
                            <div id="et-boc" class="et-boc">
                                <div class="et-l et-l--post">
                                    <div class="et_builder_inner_content et_pb_gutters3">
                                        <div
                                            class="et_pb_section et_pb_section_4 et_pb_with_background et_section_regular"
                                        >
                                            <div class="et_pb_row et_pb_row_3">
                                                <div
                                                    class="et_pb_column et_pb_column_4_4 et_pb_column_8 et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_7 et_pb_text_align_left et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner"><h1>Liên hệ</h1></div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                            <div class="et_pb_row et_pb_row_4 et_pb_equal_columns et_pb_gutters1">
                                                <div
                                                    class="et_pb_column et_pb_column_1_2 et_pb_column_9 et_pb_css_mix_blend_mode_passthrough et_pb_column_empty"
                                                ></div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_2 et_pb_column_10 et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_8 et_pb_text_align_center et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner"><h2>(905) 738-9991</h2></div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                        </div>
                                        <!-- .et_pb_section -->
                                        <div class="et_pb_section et_pb_section_5 et_section_regular">
                                            <div class="et_pb_row et_pb_row_5 et_pb_equal_columns et_pb_gutters1">
                                                <div
                                                    class="et_pb_column et_pb_column_1_2 et_pb_column_11 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div
                                                        id="et_pb_contact_form_1"
                                                        class="et_pb_with_border et_pb_module et_pb_contact_form_1 et_pb_contact_form_container clearfix"
                                                        data-form_unique_num="1"
                                                    >
                                                        <h4 class="et_pb_contact_main_title">Nhắn tin cho chúng tôi</h4>
                                                        <div class="et-pb-contact-message"></div>

                                                        <div class="et_pb_contact">
                                                            <form
                                                                class="et_pb_contact_form clearfix"
                                                                method="post"
                                                                action="https://staging.dndcleaners.ca/contact-us/"
                                                            >
                                                                <p
                                                                    class="et_pb_contact_field et_pb_contact_field_3 et_pb_contact_field_last"
                                                                    data-id="name"
                                                                    data-type="input"
                                                                >
                                                                    <label
                                                                        for="et_pb_contact_name_1"
                                                                        class="et_pb_contact_form_label"
                                                                        >Tên</label
                                                                    >
                                                                    <input
                                                                        type="text"
                                                                        id="et_pb_contact_name_1"
                                                                        class="input"
                                                                        value=""
                                                                        name="et_pb_contact_name_1"
                                                                        data-required_mark="required"
                                                                        data-field_type="input"
                                                                        data-original_id="name"
                                                                        placeholder="Tên"
                                                                    />
                                                                </p>
                                                                <p
                                                                    class="et_pb_contact_field et_pb_contact_field_4 et_pb_contact_field_last"
                                                                    data-id="email"
                                                                    data-type="email"
                                                                >
                                                                    <label
                                                                        for="et_pb_contact_email_1"
                                                                        class="et_pb_contact_form_label"
                                                                        >Email</label
                                                                    >
                                                                    <input
                                                                        type="text"
                                                                        id="et_pb_contact_email_1"
                                                                        class="input"
                                                                        value=""
                                                                        name="et_pb_contact_email_1"
                                                                        data-required_mark="required"
                                                                        data-field_type="email"
                                                                        data-original_id="email"
                                                                        placeholder="Email"
                                                                    />
                                                                </p>
                                                                <p
                                                                    class="et_pb_contact_field et_pb_contact_field_5 et_pb_contact_field_last"
                                                                    data-id="message"
                                                                    data-type="text"
                                                                >
                                                                    <label
                                                                        for="et_pb_contact_message_1"
                                                                        class="et_pb_contact_form_label"
                                                                        >Nội dung</label
                                                                    >
                                                                    <textarea
                                                                        name="et_pb_contact_message_1"
                                                                        id="et_pb_contact_message_1"
                                                                        class="et_pb_contact_message input"
                                                                        data-required_mark="required"
                                                                        data-field_type="text"
                                                                        data-original_id="message"
                                                                        placeholder="Nội dung"
                                                                    ></textarea>
                                                                </p>
                                                                <input
                                                                    type="hidden"
                                                                    value="et_contact_proccess"
                                                                    name="et_pb_contactform_submit_1"
                                                                />
                                                                <div class="et_contact_bottom_container">
                                                                    <button
                                                                        type="submit"
                                                                        name="et_builder_submit_button"
                                                                        class="et_pb_contact_submit et_pb_button"
                                                                    >
                                                                        Gửi
                                                                    </button>
                                                                </div>
                                                                <input
                                                                    type="hidden"
                                                                    id="_wpnonce-et-pb-contact-form-submitted-1"
                                                                    name="_wpnonce-et-pb-contact-form-submitted-1"
                                                                    value="c9c68e3b88"
                                                                /><input
                                                                    type="hidden"
                                                                    name="_wp_http_referer"
                                                                    value="/contact-us/"
                                                                />
                                                            </form>
                                                        </div>
                                                        <!-- .et_pb_contact -->
                                                    </div>
                                                    <!-- .et_pb_contact_form_container -->
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_2 et_pb_column_12 et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div class="et_pb_module et_pb_code et_pb_code_1">
                                                        <div class="et_pb_code_inner">
                                                            <iframe
                                                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2878.3143331419296!2d-79.54313638449653!3d43.82857997911563!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x882b2f4118aa0d8d%3A0x6275c7c0128a43d1!2s3255%20Rutherford%20Rd%20J19%2C%20Concord%2C%20ON%20L4K%205Y5!5e0!3m2!1sen!2sca!4v1614722739792!5m2!1sen!2sca"
                                                                width="600"
                                                                height="450"
                                                                style="border: 0"
                                                                allowfullscreen=""
                                                                loading="lazy"
                                                            ></iframe>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_code -->
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                        </div>
                                        <!-- .et_pb_section -->
                                        <div class="et_pb_section et_pb_section_7 et_section_specialty">
                                            <div class="et_pb_row">
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_13 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div class="et_pb_module et_pb_image et_pb_image_1">
                                                        <span class="et_pb_image_wrap"
                                                            ><img
                                                                src="/wp-content/uploads/2021/03/DD-cleaners.jpg"
                                                                alt=""
                                                                title="D&amp;D-cleaners"
                                                                height="auto"
                                                                width="auto"
                                                                srcset="
                                                                    /wp-content/uploads/2021/03/DD-cleaners.jpg         800w,
                                                                    /wp-content/uploads/2021/03/DD-cleaners-480x235.jpg 480w
                                                                "
                                                                sizes="(min-width: 0px) and (max-width: 480px) 480px, (min-width: 481px) 800px, 100vw"
                                                                class="wp-image-425"
                                                        /></span>
                                                    </div>
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_14 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_9 et_clickable et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <h4><span style="color: #000000">Time</span></h4>
                                                            <p>
                                                                <span style="color: #000000"
                                                                    ><strong>Mon:</strong> 10:00am – 6:00pm</span
                                                                ><br /><span style="color: #000000">
                                                                    <strong>Tue:</strong> 10:00am – 6:00pm</span
                                                                ><br /><span style="color: #000000"
                                                                    ><strong>Wed:</strong> 10:00am – 7:00pm</span
                                                                ><br /><span style="color: #000000">
                                                                    <strong>Thu:</strong> 10:00am – 7:00pm</span
                                                                ><br /><span style="color: #000000"
                                                                    ><strong>Fri:</strong> 10:00am – 7:00pm</span
                                                                ><br /><span style="color: #000000"
                                                                    ><strong>Saturday: </strong>10:00am – 6:00pm<strong
                                                                        ><br /></strong></span
                                                                ><span style="color: #000000"
                                                                    ><strong>Sunday:</strong> 11:00am – 3:00pm</span
                                                                >
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_10 et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <h4>Contact</h4>
                                                            <p><a href="tel:9057389991">(905) 738-9991</a></p>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_11 et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <p>
                                                                <a href="mailto:dndcleaners.ca@gmail.com"
                                                                    >dndcleaners.ca@gmail.com</a
                                                                >
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_2 et_pb_column_15 et_pb_specialty_column et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div class="et_pb_row_inner et_pb_row_inner_2">
                                                        <div
                                                            class="et_pb_column et_pb_column_1_4 et_pb_column_inner et_pb_column_inner_3"
                                                        >
                                                            <div
                                                                class="et_pb_module et_pb_text et_pb_text_12 et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                            >
                                                                <div class="et_pb_text_inner">
                                                                    <h4><span style="color: #000000">Address</span></h4>
                                                                    <p>
                                                                        <span style="color: #000000"
                                                                            >3255 Rutherford Road, unit J19, Vaughan,
                                                                            ON, L4K 5Y5</span
                                                                        >
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <!-- .et_pb_text -->
                                                        </div>
                                                        <!-- .et_pb_column -->
                                                        <div
                                                            class="et_pb_column et_pb_column_1_4 et_pb_column_inner et_pb_column_inner_4 et-last-child"
                                                        >
                                                            <div
                                                                class="et_pb_module et_pb_text et_pb_text_13 et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                            >
                                                                <div class="et_pb_text_inner"><h4>Follow us</h4></div>
                                                            </div>
                                                            <!-- .et_pb_text -->
                                                            <ul
                                                                class="et_pb_module et_pb_social_media_follow et_pb_social_media_follow_1 clearfix et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                            >
                                                                <li
                                                                    class="et_pb_social_media_follow_network_2 et_pb_social_icon et_pb_social_network_link et-social-facebook et_pb_social_media_follow_network_2"
                                                                >
                                                                    <a
                                                                        href="https://www.facebook.com/ddcleanersvaughan/"
                                                                        class="icon et_pb_with_border"
                                                                        title="Theo dõi trên Facebook"
                                                                        target="_blank"
                                                                        ><span
                                                                            class="et_pb_social_media_follow_network_name"
                                                                            aria-hidden="true"
                                                                            >Theo dõi</span
                                                                        ></a
                                                                    >
                                                                </li>
                                                                <li
                                                                    class="et_pb_social_media_follow_network_3 et_pb_social_icon et_pb_social_network_link et-social-instagram et_pb_social_media_follow_network_3"
                                                                >
                                                                    <a
                                                                        href="https://www.instagram.com/dndcleaners/?fbclid=IwAR0Yj-nlZCUJA1omDZp5MbnGZqSKttQj9Bp77Fm2raYdHePUMit2LBmaL94"
                                                                        class="icon et_pb_with_border"
                                                                        title="Theo dõi trên Instagram"
                                                                        target="_blank"
                                                                        ><span
                                                                            class="et_pb_social_media_follow_network_name"
                                                                            aria-hidden="true"
                                                                            >Theo dõi</span
                                                                        ></a
                                                                    >
                                                                </li>
                                                            </ul>
                                                            <!-- .et_pb_counters -->
                                                        </div>
                                                        <!-- .et_pb_column -->
                                                    </div>
                                                    <!-- .et_pb_row_inner -->
                                                    <div class="et_pb_row_inner et_pb_row_inner_3">
                                                        <div
                                                            class="et_pb_column et_pb_column_4_4 et_pb_column_inner et_pb_column_inner_5 et-last-child"
                                                        >
                                                            <div
                                                                class="et_pb_with_border et_pb_module et_pb_signup_1 et_pb_newsletter_layout_top_bottom et_pb_newsletter et_pb_subscribe clearfix et_pb_text_align_left et_pb_bg_layout_dark et_pb_no_bg et_pb_newsletter_description_no_title et_pb_newsletter_description_no_content"
                                                            >
                                                                <div
                                                                    class="et_pb_newsletter_description et_multi_view_hidden"
                                                                ></div>

                                                                <div class="et_pb_newsletter_form">
                                                                    <form method="post">
                                                                        <div
                                                                            class="et_pb_newsletter_result et_pb_newsletter_error"
                                                                        ></div>
                                                                        <div
                                                                            class="et_pb_newsletter_result et_pb_newsletter_success"
                                                                        >
                                                                            <h2>Thông báo Thành công</h2>
                                                                        </div>
                                                                        <div class="et_pb_newsletter_fields">
                                                                            <p
                                                                                class="et_pb_newsletter_field et_pb_contact_field_last et_pb_contact_field_last_tablet et_pb_contact_field_last_phone"
                                                                            >
                                                                                <label
                                                                                    class="et_pb_contact_form_label"
                                                                                    for="et_pb_signup_email"
                                                                                    style="display: none"
                                                                                    >Email</label
                                                                                >
                                                                                <input
                                                                                    id="et_pb_signup_email"
                                                                                    class="input"
                                                                                    type="text"
                                                                                    placeholder="Email"
                                                                                    name="et_pb_signup_email"
                                                                                />
                                                                            </p>

                                                                            <p class="et_pb_newsletter_button_wrap">
                                                                                <a
                                                                                    class="et_pb_newsletter_button et_pb_button"
                                                                                    href="#"
                                                                                    data-icon=""
                                                                                >
                                                                                    <span
                                                                                        class="et_subscribe_loader"
                                                                                    ></span>
                                                                                    <span
                                                                                        class="et_pb_newsletter_button_text"
                                                                                        >Đăng ký</span
                                                                                    >
                                                                                </a>
                                                                            </p>
                                                                        </div>

                                                                        <input
                                                                            type="hidden"
                                                                            value="mailchimp"
                                                                            name="et_pb_signup_provider"
                                                                        />
                                                                        <input
                                                                            type="hidden"
                                                                            value="1ea2bbd026"
                                                                            name="et_pb_signup_list_id"
                                                                        />
                                                                        <input
                                                                            type="hidden"
                                                                            value="elegantthemestest"
                                                                            name="et_pb_signup_account_name"
                                                                        />
                                                                        <input
                                                                            type="hidden"
                                                                            value="true"
                                                                            name="et_pb_signup_ip_address"
                                                                        /><input
                                                                            type="hidden"
                                                                            value="c31b809bb99343fbd7464e3628d08cb0"
                                                                            name="et_pb_signup_checksum"
                                                                        />
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- .et_pb_column -->
                                                    </div>
                                                    <!-- .et_pb_row_inner -->
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                        </div>
                                        <!-- .et_pb_section -->
                                    </div>
                                    <!-- .et_builder_inner_content -->
                                </div>
                                <!-- .et-l -->
                            </div>
                            <!-- #et-boc -->
                        </div>
                        <!-- .entry-content -->
                    </article>
                    <!-- .et_pb_post -->
                </div>
                <!-- #main-content -->

                <span class="et_pb_scroll_top et-pb-icon"></span>

                <footer id="main-footer">
                    <div id="footer-bottom">
                        <div class="container clearfix">
                            <div id="footer-info">D&amp;D CLEANERS | Copyright © 2021. All Rights Reserved.</div>
                        </div>
                        <!-- .container -->
                    </div>
                </footer>
                <!-- #main-footer -->
            </div>
            <!-- #et-main-area -->
        </div>
        <!-- #page-container -->

       
        <script type="text/javascript">
            var et_link_options_data = [
                { class: "et_pb_text_2", url: "http:\/\/staging.dndcleaners.ca\/d-d-services\/", target: "_self" },
                { class: "et_pb_text_9", url: "http:\/\/staging.dndcleaners.ca\/d-d-services\/", target: "_self" },
            ];
        </script>
        
        <script type="text/javascript" id="et-builder-modules-script-js-extra">
            /* <![CDATA[ */
            var et_frontend_scripts = { builderCssContainerPrefix: "#et-boc", builderCssLayoutPrefix: "#et-boc .et-l" };
            var et_pb_custom = {
                ajaxurl: "httpsstaging.dndcleaners.ca\/wp-admin\/admin-ajax.php",
                images_uri: "httpsstaging.dndcleaners.ca\/wp-content\/themes\/Divi\/images",
                builder_images_uri: "httpsstaging.dndcleaners.ca\/wp-content\/themes\/Divi\/includes\/builder\/images",
                et_frontend_nonce: "4e47d1f20d",
                subscription_failed: "Làm ơn, kiểm tra đồng dưới chắc chắn để các ông gõ đúng thông tin.",
                et_ab_log_nonce: "0f432c9a55",
                fill_message: "Xin vui lòng, điền vào các lĩnh vực sau đây:",
                contact_error_message: "Xin hãy sửa những lỗi sau:",
                invalid: "Không hợp lệ email",
                captcha: "Captcha",
                prev: "Trước",
                previous: "Trước",
                next: "Tiếp theo",
                wrong_captcha: "Bạn đã nhập sai số captcha.",
                wrong_checkbox: "Checkbox",
                ignore_waypoints: "no",
                is_divi_theme_used: "1",
                widget_search_selector: ".widget_search",
                ab_tests: [],
                is_ab_testing_active: "",
                page_id: "294",
                unique_test_id: "",
                ab_bounce_rate: "5",
                is_cache_plugin_active: "no",
                is_shortcode_tracking: "",
                tinymce_uri: "",
            };
            var et_pb_box_shadow_elements = [];
            var et_pb_motion_elements = { desktop: [], tablet: [], phone: [] };
            var et_pb_sticky_elements = [];
            /* ]]> */
        </script>
       @include('public.script')
        @include('contact-us.style')
    </body>

    <!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/

Page Caching using disk: enhanced 

Served from: staging.dndcleaners.ca @ 2022-12-27 15:12:51 by W3 Total Cache
--></html>
