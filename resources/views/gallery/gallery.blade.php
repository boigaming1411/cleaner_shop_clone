<!DOCTYPE html>
<html dir="ltr" lang="vi" prefix="og: https://ogp.me/ns#">
    <head>
        
        <title>Bộ sưu tập - DND Cleaners</title>

        <!-- All in One SEO 4.2.0 -->
        <meta
            name="description"
            content="BỘ SƯU TẬP D&amp;D Time Mon: 10:00am – 6:00pm Tue: 10:00am – 6:00pmWed: 10:00am – 7:00pm Thu: 10:00am – 7:00pmFri: 10:00am – 7:00pmSaturday: 10:00am – 6:00pmSunday: 11:00am – 3:00pm Contact (905) 738-9991 dndcleaners.ca@gmail.com Address 3255 Rutherford Road, unit J19, Vaughan, ON, L4K 5Y5 Follow us Theo dõiTheo dõi Thông báo Thành công Email Đăng ký"
        />
       
        <link rel="canonical" href="/gallery/" />
        <meta property="og:locale" content="vi_VN" />
        <meta property="og:site_name" content="DND Cleaners - Professional Alternations &amp; Repairs" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="Bộ sưu tập - DND Cleaners" />
        <meta
            property="og:description"
            content="BỘ SƯU TẬP D&amp;D Time Mon: 10:00am – 6:00pm Tue: 10:00am – 6:00pmWed: 10:00am – 7:00pm Thu: 10:00am – 7:00pmFri: 10:00am – 7:00pmSaturday: 10:00am – 6:00pmSunday: 11:00am – 3:00pm Contact (905) 738-9991 dndcleaners.ca@gmail.com Address 3255 Rutherford Road, unit J19, Vaughan, ON, L4K 5Y5 Follow us Theo dõiTheo dõi Thông báo Thành công Email Đăng ký"
        />
        <meta property="og:url" content="/gallery/" />
        <meta property="article:published_time" content="2021-03-02T22:50:14+00:00" />
        <meta property="article:modified_time" content="2022-04-07T11:52:56+00:00" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="Bộ sưu tập - DND Cleaners" />
        <meta
            name="twitter:description"
            content="BỘ SƯU TẬP D&amp;D Time Mon: 10:00am – 6:00pm Tue: 10:00am – 6:00pmWed: 10:00am – 7:00pm Thu: 10:00am – 7:00pmFri: 10:00am – 7:00pmSaturday: 10:00am – 6:00pmSunday: 11:00am – 3:00pm Contact (905) 738-9991 dndcleaners.ca@gmail.com Address 3255 Rutherford Road, unit J19, Vaughan, ON, L4K 5Y5 Follow us Theo dõiTheo dõi Thông báo Thành công Email Đăng ký"
        />
        <script type="application/ld+json" class="aioseo-schema">
            {
                "@context": "https:\/\/schema.org",
                "@graph": [
                    {
                        "@type": "WebSite",
                        "@id": "httpsstaging.dndcleaners.ca\/#website",
                        "url": "httpsstaging.dndcleaners.ca\/",
                        "name": "DND Cleaners",
                        "description": "Professional Alternations & Repairs",
                        "inLanguage": "vi",
                        "publisher": { "@id": "httpsstaging.dndcleaners.ca\/#organization" }
                    },
                    {
                        "@type": "Organization",
                        "@id": "httpsstaging.dndcleaners.ca\/#organization",
                        "name": "DND Cleaners",
                        "url": "httpsstaging.dndcleaners.ca\/"
                    },
                    {
                        "@type": "BreadcrumbList",
                        "@id": "httpsstaging.dndcleaners.ca\/gallery\/#breadcrumblist",
                        "itemListElement": [
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/#listItem",
                                "position": 1,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/",
                                    "name": "Home",
                                    "description": "D&D Cleaners chuyên may, cho thuê áo cưới, sửa áo quần, giặt ủi, bán dầu gió, thực phẩm chức năng tại Vaughan, Ontario, Canada",
                                    "url": "httpsstaging.dndcleaners.ca\/"
                                },
                                "nextItem": "httpsstaging.dndcleaners.ca\/gallery\/#listItem"
                            },
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/gallery\/#listItem",
                                "position": 2,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/gallery\/",
                                    "name": "Bộ sưu tập",
                                    "description": "BỘ SƯU TẬP D&D Time Mon: 10:00am – 6:00pm Tue: 10:00am – 6:00pmWed: 10:00am – 7:00pm Thu: 10:00am – 7:00pmFri: 10:00am – 7:00pmSaturday: 10:00am – 6:00pmSunday: 11:00am – 3:00pm Contact (905) 738-9991 dndcleaners.ca@gmail.com Address 3255 Rutherford Road, unit J19, Vaughan, ON, L4K 5Y5 Follow us Theo dõiTheo dõi Thông báo Thành công Email Đăng ký",
                                    "url": "httpsstaging.dndcleaners.ca\/gallery\/"
                                },
                                "previousItem": "httpsstaging.dndcleaners.ca\/#listItem"
                            }
                        ]
                    },
                    {
                        "@type": "WebPage",
                        "@id": "httpsstaging.dndcleaners.ca\/gallery\/#webpage",
                        "url": "httpsstaging.dndcleaners.ca\/gallery\/",
                        "name": "Bộ sưu tập - DND Cleaners",
                        "description": "BỘ SƯU TẬP D&D Time Mon: 10:00am – 6:00pm Tue: 10:00am – 6:00pmWed: 10:00am – 7:00pm Thu: 10:00am – 7:00pmFri: 10:00am – 7:00pmSaturday: 10:00am – 6:00pmSunday: 11:00am – 3:00pm Contact (905) 738-9991 dndcleaners.ca@gmail.com Address 3255 Rutherford Road, unit J19, Vaughan, ON, L4K 5Y5 Follow us Theo dõiTheo dõi Thông báo Thành công Email Đăng ký",
                        "inLanguage": "vi",
                        "isPartOf": { "@id": "httpsstaging.dndcleaners.ca\/#website" },
                        "breadcrumb": { "@id": "httpsstaging.dndcleaners.ca\/gallery\/#breadcrumblist" },
                        "datePublished": "2021-03-02T22:50:14+00:00",
                        "dateModified": "2022-04-07T11:52:56+00:00"
                    }
                ]
            }
        </script>
        <!-- All in One SEO -->

        <link rel="alternate" href="/gallery/" hreflang="vi" />
        <link rel="alternate" href="/en/gallery/" hreflang="en" />
        @include('public.header')    
    </head>
    {{$current='gallery'}}
    <body
        class="page-template-default page page-id-335 theme-Divi woocommerce-no-js et_pb_button_helper_class et_fixed_nav et_show_nav et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_header_style_left et_pb_footer_columns4 et_cover_background et_pb_gutter windows et_pb_gutters3 et_pb_pagebuilder_layout et_smooth_scroll et_no_sidebar et_divi_theme et-db"
    >
        <div id="page-container">
            <header id="main-header" data-height-onload="66">
                <div class="container clearfix et_menu_container">
                    <div class="logo_container">
                        <span class="logo_helper"></span>
                        <a href="/">
                            <img
                                src="/wp-content/uploads/2021/03/DD-cleaners.jpg"
                                alt="DND Cleaners"
                                id="logo"
                                data-height-percentage="87"
                            />
                        </a>
                    </div>
                    <div id="et-top-navigation" data-height="66" data-fixed-height="40">
                        @include('public.navbar')

                        <a href="/cart/" class="et-cart-info">
                            <span></span>
                        </a>

                        <div id="et_mobile_nav_menu">
                            <div class="mobile_nav closed">
                                <span class="select_page">Chọn trang</span>
                                <span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
                            </div>
                        </div>
                    </div>
                    <!-- #et-top-navigation -->
                </div>
                <!-- .container -->
                <div class="et_search_outer">
                    <div class="container et_search_form_container">
                        <form
                            role="search"
                            method="get"
                            class="et-search-form"
                            action="https://staging.dndcleaners.ca/"
                        >
                            <input
                                type="search"
                                class="et-search-field"
                                placeholder="Tìm kiếm &hellip;"
                                value=""
                                name="s"
                                title="Tìm:"
                            />
                        </form>
                        <span class="et_close_search_field"></span>
                    </div>
                </div>
            </header>
            <!-- #main-header -->
            <div id="et-main-area">
                <div id="main-content">
                    <article id="post-335" class="post-335 page type-page status-publish hentry">
                        <div class="entry-content">
                            <div id="et-boc" class="et-boc">
                                <div class="et-l et-l--post">
                                    <div class="et_builder_inner_content et_pb_gutters3">
                                        <div
                                            class="et_pb_section et_pb_section_4 et_pb_with_background et_section_regular"
                                        >
                                            <div class="et_pb_row et_pb_row_2">
                                                <div
                                                    class="et_pb_column et_pb_column_4_4 et_pb_column_5 et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_6 et_animated et_pb_text_align_left et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner"><h1>BỘ SƯU TẬP D&amp;D</h1></div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                        </div>
                                        <!-- .et_pb_section -->
                                        <div class="et_pb_section et_pb_section_5 gallery_gallery et_section_regular">
                                            <div class="et_pb_row et_pb_row_3">
                                                <div
                                                    class="et_pb_column et_pb_column_4_4 et_pb_column_6 et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div
                                                        class="et_pb_module et_pb_gallery et_pb_gallery_1 et_pb_bg_layout_light et_pb_gallery_grid"
                                                    >
                                                        <div
                                                            class="et_pb_gallery_items et_post_gallery clearfix"
                                                            data-per_page="50"
                                                        >
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_0"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaner-04.jpg"
                                                                        title="dnd-cleaner-04"
                                                                    >
                                                                        <img
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaner-04-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaner-04.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaner-04-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-355"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_1"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaner-08.jpg"
                                                                        title="dnd-cleaner-08"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaner-08-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaner-08.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaner-08-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-359"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_2"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaner-03.jpg"
                                                                        title="dnd-cleaner-03"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="294"
                                                                            height="520"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaner-03.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaner-03.jpg 479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaner-03.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-354"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_3"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaner-02.jpg"
                                                                        title="dnd-cleaner-02"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="365"
                                                                            height="495"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaner-02.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaner-02.jpg 479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaner-02.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-353"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_4"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaner-01.jpg"
                                                                        title="dnd-cleaner-01"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="363"
                                                                            height="373"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaner-01.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaner-01.jpg 479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaner-01.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-352"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_5"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-1.jpg"
                                                                        title="dnd-cleaners-gallery-shop (1)"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-1-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-1.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-1-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-674"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_6"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-2.jpg"
                                                                        title="dnd-cleaners-gallery-shop (2)"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-2-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-2.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-2-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-675"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_7"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-4.jpg"
                                                                        title="dnd-cleaners-gallery-shop (4)"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-4-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-4.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-4-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-677"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_8"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-5.jpg"
                                                                        title="dnd-cleaners-gallery-shop (5)"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-5-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-5.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-5-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-678"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_9"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-6.jpg"
                                                                        title="dnd-cleaners-gallery-shop (6)"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-6-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-6.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-6-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-679"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_10"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-7.jpg"
                                                                        title="dnd-cleaners-gallery-shop (7)"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-7-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-7.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-7-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-680"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_11"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-8.jpg"
                                                                        title="dnd-cleaners-gallery-shop (8)"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-8-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-8.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-8-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-681"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_12"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-12.jpg"
                                                                        title="dnd-cleaners-gallery-shop (12)"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-12-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-12.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-12-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-685"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_13"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-13.jpg"
                                                                        title="dnd-cleaners-gallery-shop (13)"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-13-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-13.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-13-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-686"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_14"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-14.jpg"
                                                                        title="dnd-cleaners-gallery-shop (14)"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-14-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-14.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-14-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-687"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_15"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-15.jpg"
                                                                        title="dnd-cleaners-gallery-shop (15)"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-15-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-15.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-15-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-688"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_16"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-16.jpg"
                                                                        title="dnd-cleaners-gallery-shop (16)"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-16-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-16.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-16-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-689"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_17"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-18.jpg"
                                                                        title="dnd-cleaners-gallery-shop (18)"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-18-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-18.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-18-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-691"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_18"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaner-07.jpg"
                                                                        title="dnd-cleaner-07"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="240"
                                                                            height="320"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaner-07.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaner-07.jpg 479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaner-07.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-358"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_19"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-19.jpg"
                                                                        title="dnd-cleaners-gallery-shop (19)"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-19-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-19.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-19-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-692"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_20"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-20.jpg"
                                                                        title="dnd-cleaners-gallery-shop (20)"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-20-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-20.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-20-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-693"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_21"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-21.jpg"
                                                                        title="dnd-cleaners-gallery-shop (21)"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-21-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-21.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-21-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-694"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_22"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-23.jpg"
                                                                        title="dnd-cleaners-gallery-shop (23)"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-23-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-23.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-23-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-696"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_23"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-22.jpg"
                                                                        title="dnd-cleaners-gallery-shop (22)"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="400"
                                                                            height="516"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-22-400x516.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-22.jpg         479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-22-400x516.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-695"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_24"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaner-11.jpg"
                                                                        title="dnd-cleaner-11"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="320"
                                                                            height="240"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaner-11.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaner-11.jpg 479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaner-11.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-362"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_25"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaner-06.jpg"
                                                                        title="dnd-cleaner-06"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="365"
                                                                            height="490"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaner-06.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaner-06.jpg 479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaner-06.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-357"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="et_pb_gallery_item et_pb_grid_item et_pb_bg_layout_light et_pb_gallery_item_1_26"
                                                            >
                                                                <div class="et_pb_gallery_image portrait">
                                                                    <a
                                                                        href="/wp-content/uploads/2021/03/dnd-cleaner-10.jpg"
                                                                        title="dnd-cleaner-10"
                                                                    >
                                                                        <img
                                                                            loading="lazy"
                                                                            width="240"
                                                                            height="320"
                                                                            src="/wp-content/uploads/2021/03/dnd-cleaner-10.jpg"
                                                                            srcset="
                                                                                /wp-content/uploads/2021/03/dnd-cleaner-10.jpg 479w,
                                                                                /wp-content/uploads/2021/03/dnd-cleaner-10.jpg 480w
                                                                            "
                                                                            sizes="(max-width:479px) 479px, 100vw"
                                                                            class="wp-image-361"
                                                                        />
                                                                        <span class="et_overlay"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- .et_pb_gallery_items -->
                                                    </div>
                                                    <!-- .et_pb_gallery -->
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                        </div>
                                        <!-- .et_pb_section -->
                                        <div class="et_pb_section et_pb_section_7 et_section_specialty">
                                            <div class="et_pb_row">
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_7 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div class="et_pb_module et_pb_image et_pb_image_1">
                                                        <span class="et_pb_image_wrap"
                                                            ><img
                                                                loading="lazy"
                                                                src="/wp-content/uploads/2021/03/DD-cleaners.jpg"
                                                                alt=""
                                                                title="D&amp;D-cleaners"
                                                                height="auto"
                                                                width="auto"
                                                                srcset="
                                                                    /wp-content/uploads/2021/03/DD-cleaners.jpg         800w,
                                                                    /wp-content/uploads/2021/03/DD-cleaners-480x235.jpg 480w
                                                                "
                                                                sizes="(min-width: 0px) and (max-width: 480px) 480px, (min-width: 481px) 800px, 100vw"
                                                                class="wp-image-425"
                                                        /></span>
                                                    </div>
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_8 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_7 et_clickable et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <h4><span style="color: #000000">Time</span></h4>
                                                            <p>
                                                                <span style="color: #000000"
                                                                    ><strong>Mon:</strong> 10:00am – 6:00pm</span
                                                                ><br /><span style="color: #000000">
                                                                    <strong>Tue:</strong> 10:00am – 6:00pm</span
                                                                ><br /><span style="color: #000000"
                                                                    ><strong>Wed:</strong> 10:00am – 7:00pm</span
                                                                ><br /><span style="color: #000000">
                                                                    <strong>Thu:</strong> 10:00am – 7:00pm</span
                                                                ><br /><span style="color: #000000"
                                                                    ><strong>Fri:</strong> 10:00am – 7:00pm</span
                                                                ><br /><span style="color: #000000"
                                                                    ><strong>Saturday: </strong>10:00am – 6:00pm<strong
                                                                        ><br /></strong></span
                                                                ><span style="color: #000000"
                                                                    ><strong>Sunday:</strong> 11:00am – 3:00pm</span
                                                                >
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_8 et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <h4>Contact</h4>
                                                            <p><a href="tel:9057389991">(905) 738-9991</a></p>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_9 et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <p>
                                                                <a href="mailto:dndcleaners.ca@gmail.com"
                                                                    >dndcleaners.ca@gmail.com</a
                                                                >
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_2 et_pb_column_9 et_pb_specialty_column et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div class="et_pb_row_inner et_pb_row_inner_2">
                                                        <div
                                                            class="et_pb_column et_pb_column_1_4 et_pb_column_inner et_pb_column_inner_3"
                                                        >
                                                            <div
                                                                class="et_pb_module et_pb_text et_pb_text_10 et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                            >
                                                                <div class="et_pb_text_inner">
                                                                    <h4><span style="color: #000000">Address</span></h4>
                                                                    <p>
                                                                        <span style="color: #000000"
                                                                            >3255 Rutherford Road, unit J19, Vaughan,
                                                                            ON, L4K 5Y5</span
                                                                        >
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <!-- .et_pb_text -->
                                                        </div>
                                                        <!-- .et_pb_column -->
                                                        <div
                                                            class="et_pb_column et_pb_column_1_4 et_pb_column_inner et_pb_column_inner_4 et-last-child"
                                                        >
                                                            <div
                                                                class="et_pb_module et_pb_text et_pb_text_11 et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                            >
                                                                <div class="et_pb_text_inner"><h4>Follow us</h4></div>
                                                            </div>
                                                            <!-- .et_pb_text -->
                                                            <ul
                                                                class="et_pb_module et_pb_social_media_follow et_pb_social_media_follow_1 clearfix et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                            >
                                                                <li
                                                                    class="et_pb_social_media_follow_network_2 et_pb_social_icon et_pb_social_network_link et-social-facebook et_pb_social_media_follow_network_2"
                                                                >
                                                                    <a
                                                                        href="https://www.facebook.com/ddcleanersvaughan/"
                                                                        class="icon et_pb_with_border"
                                                                        title="Theo dõi trên Facebook"
                                                                        target="_blank"
                                                                        ><span
                                                                            class="et_pb_social_media_follow_network_name"
                                                                            aria-hidden="true"
                                                                            >Theo dõi</span
                                                                        ></a
                                                                    >
                                                                </li>
                                                                <li
                                                                    class="et_pb_social_media_follow_network_3 et_pb_social_icon et_pb_social_network_link et-social-instagram et_pb_social_media_follow_network_3"
                                                                >
                                                                    <a
                                                                        href="https://www.instagram.com/dndcleaners/?fbclid=IwAR0Yj-nlZCUJA1omDZp5MbnGZqSKttQj9Bp77Fm2raYdHePUMit2LBmaL94"
                                                                        class="icon et_pb_with_border"
                                                                        title="Theo dõi trên Instagram"
                                                                        target="_blank"
                                                                        ><span
                                                                            class="et_pb_social_media_follow_network_name"
                                                                            aria-hidden="true"
                                                                            >Theo dõi</span
                                                                        ></a
                                                                    >
                                                                </li>
                                                            </ul>
                                                            <!-- .et_pb_counters -->
                                                        </div>
                                                        <!-- .et_pb_column -->
                                                    </div>
                                                    <!-- .et_pb_row_inner -->
                                                    <div class="et_pb_row_inner et_pb_row_inner_3">
                                                        <div
                                                            class="et_pb_column et_pb_column_4_4 et_pb_column_inner et_pb_column_inner_5 et-last-child"
                                                        >
                                                            <div
                                                                class="et_pb_with_border et_pb_module et_pb_signup_1 et_pb_newsletter_layout_top_bottom et_pb_newsletter et_pb_subscribe clearfix et_pb_text_align_left et_pb_bg_layout_dark et_pb_no_bg et_pb_newsletter_description_no_title et_pb_newsletter_description_no_content"
                                                            >
                                                                <div
                                                                    class="et_pb_newsletter_description et_multi_view_hidden"
                                                                ></div>

                                                                <div class="et_pb_newsletter_form">
                                                                    <form method="post">
                                                                        <div
                                                                            class="et_pb_newsletter_result et_pb_newsletter_error"
                                                                        ></div>
                                                                        <div
                                                                            class="et_pb_newsletter_result et_pb_newsletter_success"
                                                                        >
                                                                            <h2>Thông báo Thành công</h2>
                                                                        </div>
                                                                        <div class="et_pb_newsletter_fields">
                                                                            <p
                                                                                class="et_pb_newsletter_field et_pb_contact_field_last et_pb_contact_field_last_tablet et_pb_contact_field_last_phone"
                                                                            >
                                                                                <label
                                                                                    class="et_pb_contact_form_label"
                                                                                    for="et_pb_signup_email"
                                                                                    style="display: none"
                                                                                    >Email</label
                                                                                >
                                                                                <input
                                                                                    id="et_pb_signup_email"
                                                                                    class="input"
                                                                                    type="text"
                                                                                    placeholder="Email"
                                                                                    name="et_pb_signup_email"
                                                                                />
                                                                            </p>

                                                                            <p class="et_pb_newsletter_button_wrap">
                                                                                <a
                                                                                    class="et_pb_newsletter_button et_pb_button"
                                                                                    href="#"
                                                                                    data-icon=""
                                                                                >
                                                                                    <span
                                                                                        class="et_subscribe_loader"
                                                                                    ></span>
                                                                                    <span
                                                                                        class="et_pb_newsletter_button_text"
                                                                                        >Đăng ký</span
                                                                                    >
                                                                                </a>
                                                                            </p>
                                                                        </div>

                                                                        <input
                                                                            type="hidden"
                                                                            value="mailchimp"
                                                                            name="et_pb_signup_provider"
                                                                        />
                                                                        <input
                                                                            type="hidden"
                                                                            value="1ea2bbd026"
                                                                            name="et_pb_signup_list_id"
                                                                        />
                                                                        <input
                                                                            type="hidden"
                                                                            value="elegantthemestest"
                                                                            name="et_pb_signup_account_name"
                                                                        />
                                                                        <input
                                                                            type="hidden"
                                                                            value="true"
                                                                            name="et_pb_signup_ip_address"
                                                                        /><input
                                                                            type="hidden"
                                                                            value="c31b809bb99343fbd7464e3628d08cb0"
                                                                            name="et_pb_signup_checksum"
                                                                        />
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- .et_pb_column -->
                                                    </div>
                                                    <!-- .et_pb_row_inner -->
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                        </div>
                                        <!-- .et_pb_section -->
                                    </div>
                                    <!-- .et_builder_inner_content -->
                                </div>
                                <!-- .et-l -->
                            </div>
                            <!-- #et-boc -->
                        </div>
                        <!-- .entry-content -->
                    </article>
                    <!-- .et_pb_post -->
                </div>
                <!-- #main-content -->

                <span class="et_pb_scroll_top et-pb-icon"></span>

                <footer id="main-footer">
                    <div id="footer-bottom">
                        <div class="container clearfix">
                            <div id="footer-info">D&amp;D CLEANERS | Copyright © 2021. All Rights Reserved.</div>
                        </div>
                        <!-- .container -->
                    </div>
                </footer>
                <!-- #main-footer -->
            </div>
            <!-- #et-main-area -->
        </div>
        <!-- #page-container -->

       
        <script type="text/javascript">
            var et_animation_data = [
                {
                    class: "et_pb_text_0",
                    style: "fade",
                    repeat: "once",
                    duration: "1000ms",
                    delay: "0ms",
                    intensity: "50%",
                    starting_opacity: "0%",
                    speed_curve: "ease-in-out",
                },
                {
                    class: "et_pb_text_6",
                    style: "fade",
                    repeat: "once",
                    duration: "1000ms",
                    delay: "0ms",
                    intensity: "50%",
                    starting_opacity: "0%",
                    speed_curve: "ease-in-out",
                },
            ];
            var et_link_options_data = [
                { class: "et_pb_text_1", url: "http:\/\/staging.dndcleaners.ca\/d-d-services\/", target: "_self" },
                { class: "et_pb_text_7", url: "http:\/\/staging.dndcleaners.ca\/d-d-services\/", target: "_self" },
            ];
        </script>
       
       
       
        
      
       
        <script type="text/javascript" id="et-builder-modules-script-js-extra">
            /* <![CDATA[ */
            var et_frontend_scripts = { builderCssContainerPrefix: "#et-boc", builderCssLayoutPrefix: "#et-boc .et-l" };
            var et_pb_custom = {
                ajaxurl: "httpsstaging.dndcleaners.ca\/wp-admin\/admin-ajax.php",
                images_uri: "httpsstaging.dndcleaners.ca\/wp-content\/themes\/Divi\/images",
                builder_images_uri: "httpsstaging.dndcleaners.ca\/wp-content\/themes\/Divi\/includes\/builder\/images",
                et_frontend_nonce: "e724749a30",
                subscription_failed: "Làm ơn, kiểm tra đồng dưới chắc chắn để các ông gõ đúng thông tin.",
                et_ab_log_nonce: "25383f29c3",
                fill_message: "Xin vui lòng, điền vào các lĩnh vực sau đây:",
                contact_error_message: "Xin hãy sửa những lỗi sau:",
                invalid: "Không hợp lệ email",
                captcha: "Captcha",
                prev: "Trước",
                previous: "Trước",
                next: "Tiếp theo",
                wrong_captcha: "Bạn đã nhập sai số captcha.",
                wrong_checkbox: "Checkbox",
                ignore_waypoints: "no",
                is_divi_theme_used: "1",
                widget_search_selector: ".widget_search",
                ab_tests: [],
                is_ab_testing_active: "",
                page_id: "335",
                unique_test_id: "",
                ab_bounce_rate: "5",
                is_cache_plugin_active: "no",
                is_shortcode_tracking: "",
                tinymce_uri: "",
            };
            var et_pb_box_shadow_elements = [];
            var et_pb_motion_elements = { desktop: [], tablet: [], phone: [] };
            var et_pb_sticky_elements = [];
            /* ]]> */
        </script>
       
       
        <script
            type="text/javascript"
            src="/wp-content/themes/Divi/includes/builder/scripts/ext/jquery.hashchange.js?ver=4.9.0"
            id="hashchange-js"
        ></script>
        @include('public.script')
        @include('gallery.style')
    </body>

    <!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/

Page Caching using disk: enhanced 

Served from: staging.dndcleaners.ca @ 2022-12-27 02:06:59 by W3 Total Cache
--></html>
