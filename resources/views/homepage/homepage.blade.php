<!DOCTYPE html>
<html dir="ltr" lang="vi" prefix="og: https://ogp.me/ns#">
    <head>
        
        <title>D&amp;D Cleaners may, cho thuê áo cưới, sửa áo quần, giặt ủi</title>

        <!-- All in One SEO 4.2.0 -->
        <meta
            name="description"
            content="D&amp;D Cleaners chuyên may, cho thuê áo cưới, sửa áo quần, giặt ủi, bán dầu gió, thực phẩm chức năng tại Vaughan, Ontario, Canada"
        />
       
        <link rel="canonical" href="/" />
        <meta property="og:locale" content="vi_VN" />
        <meta property="og:site_name" content="DND Cleaners - Professional Alternations &amp; Repairs" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="D&amp;D Cleaners may, cho thuê áo cưới, sửa áo quần, giặt ủi" />
        <meta
            property="og:description"
            content="D&amp;D Cleaners chuyên may, cho thuê áo cưới, sửa áo quần, giặt ủi, bán dầu gió, thực phẩm chức năng tại Vaughan, Ontario, Canada"
        />
        <meta property="og:url" content="/" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="D&amp;D Cleaners may, cho thuê áo cưới, sửa áo quần, giặt ủi" />
        <meta
            name="twitter:description"
            content="D&amp;D Cleaners chuyên may, cho thuê áo cưới, sửa áo quần, giặt ủi, bán dầu gió, thực phẩm chức năng tại Vaughan, Ontario, Canada"
        />
        <link rel="alternate" href="/" hreflang="vi" />
        <link rel="alternate" href="/en/homepage/" hreflang="en" />
        <script type="application/ld+json" class="aioseo-schema">
            {
                "@context": "https:\/\/schema.org",
                "@graph": [
                    {
                        "@type": "WebSite",
                        "@id": "httpsstaging.dndcleaners.ca\/#website",
                        "url": "httpsstaging.dndcleaners.ca\/",
                        "name": "DND Cleaners",
                        "description": "Professional Alternations & Repairs",
                        "inLanguage": "vi",
                        "publisher": { "@id": "httpsstaging.dndcleaners.ca\/#organization" },
                        "potentialAction": {
                            "@type": "SearchAction",
                            "target": {
                                "@type": "EntryPoint",
                                "urlTemplate": "httpsstaging.dndcleaners.ca\/?s={search_term_string}"
                            },
                            "query-input": "required name=search_term_string"
                        }
                    },
                    {
                        "@type": "Organization",
                        "@id": "httpsstaging.dndcleaners.ca\/#organization",
                        "name": "DND Cleaners",
                        "url": "httpsstaging.dndcleaners.ca\/"
                    },
                    {
                        "@type": "BreadcrumbList",
                        "@id": "http:\/\/staging.dndcleaners.ca\/#breadcrumblist",
                        "itemListElement": [
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/#listItem",
                                "position": 1,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/",
                                    "name": "Home",
                                    "description": "D&D Cleaners chuyên may, cho thuê áo cưới, sửa áo quần, giặt ủi, bán dầu gió, thực phẩm chức năng tại Vaughan, Ontario, Canada",
                                    "url": "httpsstaging.dndcleaners.ca\/"
                                },
                                "nextItem": "http:\/\/staging.dndcleaners.ca\/#listItem"
                            },
                            {
                                "@type": "ListItem",
                                "@id": "http:\/\/staging.dndcleaners.ca\/#listItem",
                                "position": 2,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "http:\/\/staging.dndcleaners.ca\/",
                                    "name": "Trang chủ",
                                    "description": "D&D Cleaners chuyên may, cho thuê áo cưới, sửa áo quần, giặt ủi, bán dầu gió, thực phẩm chức năng tại Vaughan, Ontario, Canada",
                                    "url": "http:\/\/staging.dndcleaners.ca\/"
                                },
                                "previousItem": "httpsstaging.dndcleaners.ca\/#listItem"
                            }
                        ]
                    },
                    {
                        "@type": "WebPage",
                        "@id": "http:\/\/staging.dndcleaners.ca\/#webpage",
                        "url": "http:\/\/staging.dndcleaners.ca\/",
                        "name": "D&D Cleaners may, cho thuê áo cưới, sửa áo quần, giặt ủi",
                        "description": "D&D Cleaners chuyên may, cho thuê áo cưới, sửa áo quần, giặt ủi, bán dầu gió, thực phẩm chức năng tại Vaughan, Ontario, Canada",
                        "inLanguage": "vi",
                        "isPartOf": { "@id": "httpsstaging.dndcleaners.ca\/#website" },
                        "breadcrumb": { "@id": "http:\/\/staging.dndcleaners.ca\/#breadcrumblist" },
                        "image": {
                            "@type": "ImageObject",
                            "@id": "httpsstaging.dndcleaners.ca\/#mainImage",
                            "url": "httpsstaging.dndcleaners.ca\/wp-content\/uploads\/2021\/11\/dnd-cleaner-chuyen-tien-tu-canada-ve-vietnam.png",
                            "width": 600,
                            "height": 400
                        },
                        "primaryImageOfPage": { "@id": "http:\/\/staging.dndcleaners.ca\/#mainImage" },
                        "datePublished": "2021-02-25T22:48:48+00:00",
                        "dateModified": "2021-12-20T04:20:28+00:00"
                    }
                ]
            }
        </script>
        <!-- All in One SEO -->
       @include('public.header')
    </head>
    {{$current='homepage'}}
    <body
        class="home page-template-default page page-id-142 theme-Divi woocommerce-no-js et_pb_button_helper_class et_fixed_nav et_show_nav et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_header_style_left et_pb_footer_columns4 et_cover_background et_pb_gutter et_pb_gutters3 et_pb_pagebuilder_layout et_smooth_scroll et_no_sidebar et_divi_theme et-db"
    >
        <div id="page-container">
            <header id="main-header" data-height-onload="66">
                <div class="container clearfix et_menu_container">
                    <div class="logo_container">
                        <span class="logo_helper"></span>
                        <a href="/">
                            <img
                                src="/wp-content/uploads/2021/03/DD-cleaners.jpg"
                                alt="DND Cleaners"
                                id="logo"
                                data-height-percentage="87"
                            />
                        </a>
                    </div>
                    <div id="et-top-navigation" data-height="66" data-fixed-height="40">
                        @include('public.navbar')

                        <a href="/cart/" class="et-cart-info">
                            <span></span>
                        </a>

                        <div id="et_mobile_nav_menu">
                            <div class="mobile_nav closed">
                                <span class="select_page">Chọn trang</span>
                                <span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
                            </div>
                        </div>
                    </div>
                    <!-- #et-top-navigation -->
                </div>
                <!-- .container -->
                <div class="et_search_outer">
                    <div class="container et_search_form_container">
                        <form role="search" method="get" class="et-search-form" action="http://staging.dndcleaners.ca/">
                            <input
                                type="search"
                                class="et-search-field"
                                placeholder="Tìm kiếm &hellip;"
                                value=""
                                name="s"
                                title="Tìm:"
                            />
                        </form>
                        <span class="et_close_search_field"></span>
                    </div>
                </div>
            </header>
            <!-- #main-header -->
            <div id="et-main-area">
                <div id="main-content">
                    <article id="post-142" class="post-142 page type-page status-publish has-post-thumbnail hentry">
                        <div class="entry-content">
                            <div id="et-boc" class="et-boc">
                                <div class="et-l et-l--post">
                                    <div class="et_builder_inner_content et_pb_gutters3">
                                        <div
                                            class="et_pb_section et_pb_section_0 et_pb_section_parallax et_pb_with_background et_section_regular"
                                        >
                                            <div class="et_parallax_bg_wrap">
                                                <div
                                                    class="et_parallax_bg"
                                                    style="
                                                        background-image: url(/wp-content/uploads/2021/03/sewing-background-2.png);
                                                    "
                                                ></div>
                                            </div>

                                            <div class="et_pb_row et_pb_row_0">
                                                <div
                                                    class="et_pb_column et_pb_column_4_4 et_pb_column_0 et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div class="et_pb_module et_pb_image et_pb_image_0">
                                                        <span class="et_pb_image_wrap"
                                                            ><img
                                                                src="/wp-content/uploads/2021/03/hangers-small.png"
                                                                alt=""
                                                                title="hangers-small"
                                                                height="auto"
                                                                width="auto"
                                                                class="wp-image-435"
                                                        /></span>
                                                    </div>
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_0 et_pb_text_align_center et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner"><h1>D &amp; D Cleaners</h1></div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                            <div class="et_pb_row et_pb_row_1 et_pb_equal_columns et_pb_gutters1">
                                                <div
                                                    class="et_pb_column et_pb_column_4_4 et_pb_column_1 et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_1 et_pb_text_align_left et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <h2 style="text-align: center">
                                                                <span style="color: #ffffff"
                                                                    ><i class="fa fa-phone"></i> (905) 738-9991</span
                                                                >
                                                            </h2>
                                                            <h2 style="text-align: center">
                                                                <span style="color: #ffffff"
                                                                    ><i class="fa fa-map-marker"></i> 3255 Rutherford
                                                                    Road, unit J19, Vaughan, ON</span
                                                                >
                                                            </h2>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                            <div class="et_pb_row et_pb_row_2 et_pb_equal_columns et_pb_gutters1">
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_2 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_2 et_pb_text_align_center et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <h2>Dịch vụ chuyển tiền từ Canada về Việt Nam</h2>
                                                            <p>
                                                                <span style="color: #000000"
                                                                    >Chuyển tiền nhanh, uy tín từ Canada về Việt
                                                                    Nam</span
                                                                >
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_3 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div class="et_pb_module et_pb_image et_pb_image_1">
                                                        <span class="et_pb_image_wrap"
                                                            ><img
                                                                loading="lazy"
                                                                src="/wp-content/uploads/2021/11/dnd-cleaner-chuyen-tien-tu-canada-ve-vietnam.png"
                                                                alt=""
                                                                title="dnd cleaner chuyen tien tu canada ve vietnam"
                                                                height="auto"
                                                                width="auto"
                                                                srcset="
                                                                    /wp-content/uploads/2021/11/dnd-cleaner-chuyen-tien-tu-canada-ve-vietnam.png         600w,
                                                                    /wp-content/uploads/2021/11/dnd-cleaner-chuyen-tien-tu-canada-ve-vietnam-480x320.png 480w
                                                                "
                                                                sizes="(min-width: 0px) and (max-width: 480px) 480px, (min-width: 481px) 600px, 100vw"
                                                                class="wp-image-892"
                                                        /></span>
                                                    </div>
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_4 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_3 et_pb_text_align_center et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <h2>Địa điểm cho thuê áo dài cưới</h2>
                                                            <p>
                                                                <span style="color: #000000"
                                                                    >Muốn thuê áo dài cưới, trang phục cưới truyền
                                                                    thống, hãy đến với D&amp;D Cleaners.</span
                                                                >
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_5 et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div class="et_pb_module et_pb_image et_pb_image_2">
                                                        <span class="et_pb_image_wrap"
                                                            ><img
                                                                loading="lazy"
                                                                src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-2.jpg"
                                                                alt=""
                                                                title="dnd-cleaners-gallery-shop (2)"
                                                                height="auto"
                                                                width="auto"
                                                                srcset="
                                                                    /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-2.jpg          1536w,
                                                                    /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-2-1280x853.jpg 1280w,
                                                                    /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-2-980x653.jpg   980w,
                                                                    /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-2-480x320.jpg   480w
                                                                "
                                                                sizes="(min-width: 0px) and (max-width: 480px) 480px, (min-width: 481px) and (max-width: 980px) 980px, (min-width: 981px) and (max-width: 1280px) 1280px, (min-width: 1281px) 1536px, 100vw"
                                                                class="wp-image-675"
                                                        /></span>
                                                    </div>
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                        </div>
                                        <!-- .et_pb_section -->
                                        <div class="et_pb_section et_pb_section_1 et_section_regular">
                                            <div class="et_pb_row et_pb_row_3 et_pb_equal_columns et_pb_gutters1">
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_6 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div class="et_pb_module et_pb_image et_pb_image_3">
                                                        <span class="et_pb_image_wrap"
                                                            ><img
                                                                loading="lazy"
                                                                src="/wp-content/uploads/2021/03/alternations-cloth-tailoring.jpg"
                                                                alt=""
                                                                title="alternations-cloth-tailoring"
                                                                height="auto"
                                                                width="auto"
                                                                srcset="
                                                                    /wp-content/uploads/2021/03/alternations-cloth-tailoring.jpg         385w,
                                                                    /wp-content/uploads/2021/03/alternations-cloth-tailoring-300x263.jpg 300w
                                                                "
                                                                sizes="(max-width: 385px) 100vw, 385px"
                                                                class="wp-image-227"
                                                        /></span>
                                                    </div>
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_7 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_4 et_pb_text_align_center et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <h2>Dịch vụ sửa chữa quần áo</h2>
                                                            <p>
                                                                <span style="color: #000000"
                                                                    >Giúp bộ trang phục trở lại trạng thái tốt nhất như
                                                                    mới</span
                                                                >
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_8 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div class="et_pb_module et_pb_image et_pb_image_4">
                                                        <span class="et_pb_image_wrap"
                                                            ><img
                                                                loading="lazy"
                                                                src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-22.jpg"
                                                                alt=""
                                                                title="dnd-cleaners-gallery-shop (22)"
                                                                height="auto"
                                                                width="auto"
                                                                srcset="
                                                                    /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-22.jpg          1536w,
                                                                    /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-22-1280x853.jpg 1280w,
                                                                    /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-22-980x653.jpg   980w,
                                                                    /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-22-480x320.jpg   480w
                                                                "
                                                                sizes="(min-width: 0px) and (max-width: 480px) 480px, (min-width: 481px) and (max-width: 980px) 980px, (min-width: 981px) and (max-width: 1280px) 1280px, (min-width: 1281px) 1536px, 100vw"
                                                                class="wp-image-695"
                                                        /></span>
                                                    </div>
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_9 et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_5 et_pb_text_align_center et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <h2>Dịch vụ cho thuê mâm ngũ quả cưới</h2>
                                                            <p>
                                                                <span style="color: #000000"
                                                                    >Lưu giữ truyền thống văn hóa Việt Nam tại
                                                                    Canada</span
                                                                >
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                            <div class="et_pb_row et_pb_row_4 et_pb_equal_columns et_pb_gutters1">
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_10 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_6 et_pb_text_align_center et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <h2>Chuyên may đo/ bán trang phục</h2>
                                                            <p>
                                                                <span style="color: #000000"
                                                                    >Thợ may lành nghề, nhiều mẫu mã kiểu dáng đẹp</span
                                                                >
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_11 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div class="et_pb_module et_pb_image et_pb_image_5">
                                                        <span class="et_pb_image_wrap"
                                                            ><img
                                                                loading="lazy"
                                                                src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-10.jpg"
                                                                alt=""
                                                                title="dnd-cleaners-gallery-shop (10)"
                                                                height="auto"
                                                                width="auto"
                                                                srcset="
                                                                    /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-10.jpg          1536w,
                                                                    /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-10-1280x853.jpg 1280w,
                                                                    /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-10-980x653.jpg   980w,
                                                                    /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-10-480x320.jpg   480w
                                                                "
                                                                sizes="(min-width: 0px) and (max-width: 480px) 480px, (min-width: 481px) and (max-width: 980px) 980px, (min-width: 981px) and (max-width: 1280px) 1280px, (min-width: 1281px) 1536px, 100vw"
                                                                class="wp-image-683"
                                                        /></span>
                                                    </div>
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_12 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_7 et_pb_text_align_center et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <h2>Dịch vụ khác</h2>
                                                            <p>
                                                                <span style="color: #000000"
                                                                    >Đại lý mỹ phẩm, sữa, thực phẩm chức năng, dầu
                                                                    xanh</span
                                                                >
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_13 et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div class="et_pb_module et_pb_image et_pb_image_6">
                                                        <span class="et_pb_image_wrap"
                                                            ><img
                                                                loading="lazy"
                                                                src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-19.jpg"
                                                                alt=""
                                                                title="dnd-cleaners-gallery-shop (19)"
                                                                height="auto"
                                                                width="auto"
                                                                srcset="
                                                                    /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-19.jpg          1536w,
                                                                    /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-19-1280x853.jpg 1280w,
                                                                    /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-19-980x653.jpg   980w,
                                                                    /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-19-480x320.jpg   480w
                                                                "
                                                                sizes="(min-width: 0px) and (max-width: 480px) 480px, (min-width: 481px) and (max-width: 980px) 980px, (min-width: 981px) and (max-width: 1280px) 1280px, (min-width: 1281px) 1536px, 100vw"
                                                                class="wp-image-692"
                                                        /></span>
                                                    </div>
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                        </div>
                                        <!-- .et_pb_section -->
                                        <div class="et_pb_section et_pb_section_2 et_section_regular">
                                            <div class="et_pb_row et_pb_row_5">
                                                <div
                                                    class="et_pb_column et_pb_column_3_5 et_pb_column_14 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div class="et_pb_module et_pb_image et_pb_image_7">
                                                        <span class="et_pb_image_wrap"
                                                            ><img
                                                                loading="lazy"
                                                                src="/wp-content/uploads/2021/03/washing-machine-300x300.png"
                                                                alt=""
                                                                title="washing-machine"
                                                                height="auto"
                                                                width="auto"
                                                                class="wp-image-205"
                                                        /></span>
                                                    </div>
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_2_5 et_pb_column_15 et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_8 et_pb_text_align_left et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <h3>
                                                                <span style="font-weight: normal; font-family: inherit"
                                                                    ><strong
                                                                        >D&amp;D CLEANERS <br />XIN KÍNH CHÀO QUÝ
                                                                        KHÁCH</strong
                                                                    ></span
                                                                >
                                                            </h3>
                                                            <p>
                                                                <span
                                                                    style="
                                                                        font-weight: normal;
                                                                        font-family: inherit;
                                                                        color: #000000;
                                                                    "
                                                                    >D&amp;D Cleaners là một dịch tổ hợp chuyên cung cấp
                                                                    áo dài cưới, cho thuê áo dài cưới Việt Nam tại
                                                                    Vaughan, Ontario, Canada. Hiểu được văn hóa truyền
                                                                    thống của Việt Nam và thấu hiểu được khó khăn khi bà
                                                                    con Việt Nam sinh sống tại Canada về việc đáp ứng
                                                                    các trang phục, sính lễ khi tổ chức các lễ đám cưới,
                                                                    D&amp;D Cleaners còn hỗ trợ thêm các dịch vụ về mâm
                                                                    lễ cưới hỏi. Bà con người Việt sinh sống tại Canada,
                                                                    đặc biệt tại vùng Vaughan, Ontario có thể yên tâm về
                                                                    dịch vụ này khi tìm đến D&amp;DCleaners.</span
                                                                >
                                                            </p>
                                                            <p>
                                                                <span
                                                                    style="
                                                                        font-weight: normal;
                                                                        font-family: inherit;
                                                                        color: #000000;
                                                                    "
                                                                    >Ngoài ra, D&amp;DCleaners còn cung cấp các dịch vụ
                                                                    về sửa chữa trang phục, giặt khô, giặt ủi áo quần và
                                                                    các loại sản phẩm liên quan đến vải. Sự tận tâm
                                                                    trong dịch vụ phục vụ quý khách luôn là yếu tố mà
                                                                    D&amp;D Cleaners đặt lên hàng đầu.</span
                                                                >
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                        </div>
                                        <!-- .et_pb_section -->
                                        <div class="et_pb_section et_pb_section_3 et_section_regular">
                                            <div class="et_pb_row et_pb_row_6 et_pb_equal_columns et_pb_gutters1">
                                                <div
                                                    class="et_pb_column et_pb_column_3_5 et_pb_column_16 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_9 et_pb_text_align_left et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <h2>
                                                                <span style="font-weight: normal; color: #ffffff"
                                                                    >DỊCH VỤ TỪ TÂM</span
                                                                ><br />
                                                                <span style="font-weight: normal; color: #ffffff">
                                                                    LƯU GIỮ VĂN HÓA</span
                                                                ><br />
                                                                <span style="font-weight: normal; color: #ffffff">
                                                                    HIỂU KHÁCH HÀNG</span
                                                                >
                                                            </h2>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_2_5 et_pb_column_17 et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_10 et_pb_text_align_left et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <ul>
                                                                <li>Am hiểu văn hóa truyền thống</li>
                                                                <li>Cung cấp các dịch vụ sẵn sàng tại Canada</li>
                                                                <li>
                                                                    Hiểu tâm lý khách hàng, đặc biệt là bà con sinh sống
                                                                    xa quê hương
                                                                </li>
                                                                <li>Phục vụ tận tâm, nhanh chóng, chất lượng</li>
                                                                <li>
                                                                    Đầy đủ các sản phẩm phục cho lễ cưới truyền thống
                                                                    của Việt Nam tại Canada
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                            <div class="et_pb_row et_pb_row_7 et_pb_gutters1">
                                                <div
                                                    class="et_pb_column et_pb_column_2_5 et_pb_column_18 et_pb_css_mix_blend_mode_passthrough et_pb_column_empty"
                                                ></div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_3_5 et_pb_column_19 et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div class="et_pb_module et_pb_image et_pb_image_8">
                                                        <span class="et_pb_image_wrap"
                                                            ><img
                                                                loading="lazy"
                                                                src="/wp-content/uploads/2021/03/1449-scaled.jpg"
                                                                alt="Dry and clean"
                                                                title="Public laundry or dry cleaning washing machines"
                                                                height="auto"
                                                                width="auto"
                                                                srcset="
                                                                    /wp-content/uploads/2021/03/1449-scaled.jpg   2560w,
                                                                    /wp-content/uploads/2021/03/1449-1280x427.jpg 1280w,
                                                                    /wp-content/uploads/2021/03/1449-980x327.jpg   980w,
                                                                    /wp-content/uploads/2021/03/1449-480x160.jpg   480w
                                                                "
                                                                sizes="(min-width: 0px) and (max-width: 480px) 480px, (min-width: 481px) and (max-width: 980px) 980px, (min-width: 981px) and (max-width: 1280px) 1280px, (min-width: 1281px) 2560px, 100vw"
                                                                class="wp-image-232"
                                                        /></span>
                                                    </div>
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                        </div>
                                        <!-- .et_pb_section -->
                                        <div class="et_pb_section et_pb_section_5 et_section_specialty">
                                            <div class="et_pb_row">
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_20 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div class="et_pb_module et_pb_image et_pb_image_9">
                                                        <span class="et_pb_image_wrap"
                                                            ><img
                                                                loading="lazy"
                                                                src="/wp-content/uploads/2021/03/DD-cleaners.jpg"
                                                                alt=""
                                                                title="D&amp;D-cleaners"
                                                                height="auto"
                                                                width="auto"
                                                                srcset="
                                                                    /wp-content/uploads/2021/03/DD-cleaners.jpg         800w,
                                                                    /wp-content/uploads/2021/03/DD-cleaners-480x235.jpg 480w
                                                                "
                                                                sizes="(min-width: 0px) and (max-width: 480px) 480px, (min-width: 481px) 800px, 100vw"
                                                                class="wp-image-425"
                                                        /></span>
                                                    </div>
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_4 et_pb_column_21 et_pb_css_mix_blend_mode_passthrough"
                                                >
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_11 et_clickable et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <h4><span style="color: #000000">Time</span></h4>
                                                            <p>
                                                                <span style="color: #000000"
                                                                    ><strong>Mon:</strong> 10:00am – 6:00pm</span
                                                                ><br /><span style="color: #000000">
                                                                    <strong>Tue:</strong> 10:00am – 6:00pm</span
                                                                ><br /><span style="color: #000000"
                                                                    ><strong>Wed:</strong> 10:00am – 7:00pm</span
                                                                ><br /><span style="color: #000000">
                                                                    <strong>Thu:</strong> 10:00am – 7:00pm</span
                                                                ><br /><span style="color: #000000"
                                                                    ><strong>Fri:</strong> 10:00am – 7:00pm</span
                                                                ><br /><span style="color: #000000"
                                                                    ><strong>Saturday: </strong>10:00am – 6:00pm<strong
                                                                        ><br /></strong></span
                                                                ><span style="color: #000000"
                                                                    ><strong>Sunday:</strong> 11:00am – 3:00pm</span
                                                                >
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_12 et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <h4>Contact</h4>
                                                            <p><a href="tel:9057389991">(905) 738-9991</a></p>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                    <div
                                                        class="et_pb_module et_pb_text et_pb_text_13 et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                    >
                                                        <div class="et_pb_text_inner">
                                                            <p>
                                                                <a href="mailto:dndcleaners.ca@gmail.com"
                                                                    >dndcleaners.ca@gmail.com</a
                                                                >
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <!-- .et_pb_text -->
                                                </div>
                                                <!-- .et_pb_column -->
                                                <div
                                                    class="et_pb_column et_pb_column_1_2 et_pb_column_22 et_pb_specialty_column et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div class="et_pb_row_inner et_pb_row_inner_0">
                                                        <div
                                                            class="et_pb_column et_pb_column_1_4 et_pb_column_inner et_pb_column_inner_0"
                                                        >
                                                            <div
                                                                class="et_pb_module et_pb_text et_pb_text_14 et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                            >
                                                                <div class="et_pb_text_inner">
                                                                    <h4><span style="color: #000000">Address</span></h4>
                                                                    <p>
                                                                        <span style="color: #000000"
                                                                            >3255 Rutherford Road, unit J19, Vaughan,
                                                                            ON, L4K 5Y5</span
                                                                        >
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <!-- .et_pb_text -->
                                                        </div>
                                                        <!-- .et_pb_column -->
                                                        <div
                                                            class="et_pb_column et_pb_column_1_4 et_pb_column_inner et_pb_column_inner_1 et-last-child"
                                                        >
                                                            <div
                                                                class="et_pb_module et_pb_text et_pb_text_15 et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                            >
                                                                <div class="et_pb_text_inner"><h4>Follow us</h4></div>
                                                            </div>
                                                            <!-- .et_pb_text -->
                                                            <ul
                                                                class="et_pb_module et_pb_social_media_follow et_pb_social_media_follow_0 clearfix et_pb_text_align_left et_pb_text_align_center-tablet et_pb_bg_layout_light"
                                                            >
                                                                <li
                                                                    class="et_pb_social_media_follow_network_0 et_pb_social_icon et_pb_social_network_link et-social-facebook et_pb_social_media_follow_network_0"
                                                                >
                                                                    <a
                                                                        href="https://www.facebook.com/ddcleanersvaughan/"
                                                                        class="icon et_pb_with_border"
                                                                        title="Theo dõi trên Facebook"
                                                                        target="_blank"
                                                                        ><span
                                                                            class="et_pb_social_media_follow_network_name"
                                                                            aria-hidden="true"
                                                                            >Theo dõi</span
                                                                        ></a
                                                                    >
                                                                </li>
                                                                <li
                                                                    class="et_pb_social_media_follow_network_1 et_pb_social_icon et_pb_social_network_link et-social-instagram et_pb_social_media_follow_network_1"
                                                                >
                                                                    <a
                                                                        href="https://www.instagram.com/dndcleaners/?fbclid=IwAR0Yj-nlZCUJA1omDZp5MbnGZqSKttQj9Bp77Fm2raYdHePUMit2LBmaL94"
                                                                        class="icon et_pb_with_border"
                                                                        title="Theo dõi trên Instagram"
                                                                        target="_blank"
                                                                        ><span
                                                                            class="et_pb_social_media_follow_network_name"
                                                                            aria-hidden="true"
                                                                            >Theo dõi</span
                                                                        ></a
                                                                    >
                                                                </li>
                                                            </ul>
                                                            <!-- .et_pb_counters -->
                                                        </div>
                                                        <!-- .et_pb_column -->
                                                    </div>
                                                    <!-- .et_pb_row_inner -->
                                                    <div class="et_pb_row_inner et_pb_row_inner_1">
                                                        <div
                                                            class="et_pb_column et_pb_column_4_4 et_pb_column_inner et_pb_column_inner_2 et-last-child"
                                                        >
                                                            <div
                                                                class="et_pb_with_border et_pb_module et_pb_signup_0 et_pb_newsletter_layout_top_bottom et_pb_newsletter et_pb_subscribe clearfix et_pb_text_align_left et_pb_bg_layout_dark et_pb_no_bg et_pb_newsletter_description_no_title et_pb_newsletter_description_no_content"
                                                            >
                                                                <div
                                                                    class="et_pb_newsletter_description et_multi_view_hidden"
                                                                ></div>

                                                                <div class="et_pb_newsletter_form">
                                                                    <form method="post">
                                                                        <div
                                                                            class="et_pb_newsletter_result et_pb_newsletter_error"
                                                                        ></div>
                                                                        <div
                                                                            class="et_pb_newsletter_result et_pb_newsletter_success"
                                                                        >
                                                                            <h2>Thông báo Thành công</h2>
                                                                        </div>
                                                                        <div class="et_pb_newsletter_fields">
                                                                            <p
                                                                                class="et_pb_newsletter_field et_pb_contact_field_last et_pb_contact_field_last_tablet et_pb_contact_field_last_phone"
                                                                            >
                                                                                <label
                                                                                    class="et_pb_contact_form_label"
                                                                                    for="et_pb_signup_email"
                                                                                    style="display: none"
                                                                                    >Email</label
                                                                                >
                                                                                <input
                                                                                    id="et_pb_signup_email"
                                                                                    class="input"
                                                                                    type="text"
                                                                                    placeholder="Email"
                                                                                    name="et_pb_signup_email"
                                                                                />
                                                                            </p>

                                                                            <p class="et_pb_newsletter_button_wrap">
                                                                                <a
                                                                                    class="et_pb_newsletter_button et_pb_button"
                                                                                    href="#"
                                                                                    data-icon=""
                                                                                >
                                                                                    <span
                                                                                        class="et_subscribe_loader"
                                                                                    ></span>
                                                                                    <span
                                                                                        class="et_pb_newsletter_button_text"
                                                                                        >Đăng ký</span
                                                                                    >
                                                                                </a>
                                                                            </p>
                                                                        </div>

                                                                        <input
                                                                            type="hidden"
                                                                            value="mailchimp"
                                                                            name="et_pb_signup_provider"
                                                                        />
                                                                        <input
                                                                            type="hidden"
                                                                            value="1ea2bbd026"
                                                                            name="et_pb_signup_list_id"
                                                                        />
                                                                        <input
                                                                            type="hidden"
                                                                            value="elegantthemestest"
                                                                            name="et_pb_signup_account_name"
                                                                        />
                                                                        <input
                                                                            type="hidden"
                                                                            value="true"
                                                                            name="et_pb_signup_ip_address"
                                                                        /><input
                                                                            type="hidden"
                                                                            value="c31b809bb99343fbd7464e3628d08cb0"
                                                                            name="et_pb_signup_checksum"
                                                                        />
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- .et_pb_column -->
                                                    </div>
                                                    <!-- .et_pb_row_inner -->
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                        </div>
                                        <!-- .et_pb_section -->
                                    </div>
                                    <!-- .et_builder_inner_content -->
                                </div>
                                <!-- .et-l -->
                            </div>
                            <!-- #et-boc -->
                        </div>
                        <!-- .entry-content -->
                    </article>
                    <!-- .et_pb_post -->
                </div>
                <!-- #main-content -->

                <span class="et_pb_scroll_top et-pb-icon"></span>

                <footer id="main-footer">
                    <div id="footer-bottom">
                        <div class="container clearfix">
                            <div id="footer-info">D&amp;D CLEANERS | Copyright © 2021. All Rights Reserved.</div>
                        </div>
                        <!-- .container -->
                    </div>
                </footer>
                <!-- #main-footer -->
            </div>
            <!-- #et-main-area -->
        </div>
        <!-- #page-container -->
        
        <script type="text/javascript">
            var et_link_options_data = [
                { class: "et_pb_text_11", url: "http:\/\/staging.dndcleaners.ca\/d-d-services\/", target: "_self" },
            ];
        </script>
       
        <script type="text/javascript" id="et-builder-modules-script-js-extra">
            /* <![CDATA[ */
            var et_frontend_scripts = { builderCssContainerPrefix: "#et-boc", builderCssLayoutPrefix: "#et-boc .et-l" };
            var et_pb_custom = {
                ajaxurl: "http:\/\/staging.dndcleaners.ca\/wp-admin\/admin-ajax.php",
                images_uri: "http:\/\/staging.dndcleaners.ca\/wp-content\/themes\/Divi\/images",
                builder_images_uri:
                    "http:\/\/staging.dndcleaners.ca\/wp-content\/themes\/Divi\/includes\/builder\/images",
                et_frontend_nonce: "4e47d1f20d",
                subscription_failed: "Làm ơn, kiểm tra đồng dưới chắc chắn để các ông gõ đúng thông tin.",
                et_ab_log_nonce: "0f432c9a55",
                fill_message: "Xin vui lòng, điền vào các lĩnh vực sau đây:",
                contact_error_message: "Xin hãy sửa những lỗi sau:",
                invalid: "Không hợp lệ email",
                captcha: "Captcha",
                prev: "Trước",
                previous: "Trước",
                next: "Tiếp theo",
                wrong_captcha: "Bạn đã nhập sai số captcha.",
                wrong_checkbox: "Checkbox",
                ignore_waypoints: "no",
                is_divi_theme_used: "1",
                widget_search_selector: ".widget_search",
                ab_tests: [],
                is_ab_testing_active: "",
                page_id: "142",
                unique_test_id: "",
                ab_bounce_rate: "5",
                is_cache_plugin_active: "no",
                is_shortcode_tracking: "",
                tinymce_uri: "",
            };
            var et_pb_box_shadow_elements = [];
            var et_pb_motion_elements = {
                desktop: [
                    {
                        id: ".et_pb_image_7",
                        start: 0,
                        midStart: 50,
                        midEnd: 50,
                        end: 100,
                        startValue: 2,
                        midValue: 0,
                        endValue: -2,
                        resolver: "translateY",
                        module_type: "et_pb_image",
                        trigger_start: "middle",
                        trigger_end: "middle",
                    },
                ],
                tablet: [
                    {
                        id: ".et_pb_image_7",
                        start: 0,
                        midStart: 50,
                        midEnd: 50,
                        end: 100,
                        startValue: 2,
                        midValue: 0,
                        endValue: -2,
                        resolver: "translateY",
                        module_type: "et_pb_image",
                        trigger_start: "middle",
                        trigger_end: "middle",
                    },
                ],
                phone: [
                    {
                        id: ".et_pb_image_7",
                        start: 0,
                        midStart: 50,
                        midEnd: 50,
                        end: 100,
                        startValue: 2,
                        midValue: 0,
                        endValue: -2,
                        resolver: "translateY",
                        module_type: "et_pb_image",
                        trigger_start: "middle",
                        trigger_end: "middle",
                    },
                ],
            };
            var et_pb_sticky_elements = [];
            /* ]]> */
        </script>
      
       @include('homepage.style')
        @include('public.script')
    </body>

    <!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/

Page Caching using disk: enhanced (DONOTCACHEPAGE constant is defined) 

Served from: staging.dndcleaners.ca @ 2022-12-27 15:14:12 by W3 Total Cache
--></html>
