<!DOCTYPE html>
<html dir="ltr" lang="vi" prefix="og: https://ogp.me/ns#">
    <head>
        
        <title>Cho thuê áo dài cưới đẹp tại Vaughan, Ontario, Canada</title>

        <!-- All in One SEO 4.2.0 -->
        <meta
            name="description"
            content="Cho thuê áo dài cưới tại Vaughan, Ontario, Canada, địa điểm cho thuê áo dài cưới đẹp, sửa áo quần ở đâu tại Vauhan, Ontario, Canada"
        />
        <meta name="robots" content="noindex, nofollow, max-image-preview:large" />
        <link rel="canonical" href="/cho-thue-ao-dai-cuoi-dep-tai-vaughan-ontario-canada/" />
        <meta property="og:locale" content="vi_VN" />
        <meta property="og:site_name" content="DND Cleaners - Professional Alternations &amp; Repairs" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="Cho thuê áo dài cưới đẹp tại Vaughan, Ontario, Canada" />
        <meta
            property="og:description"
            content="Cho thuê áo dài cưới tại Vaughan, Ontario, Canada, địa điểm cho thuê áo dài cưới đẹp, sửa áo quần ở đâu tại Vauhan, Ontario, Canada"
        />
        <meta property="og:url" content="/cho-thue-ao-dai-cuoi-dep-tai-vaughan-ontario-canada/" />
        <meta property="article:published_time" content="2021-11-15T06:27:12+00:00" />
        <meta property="article:modified_time" content="2021-12-26T13:48:26+00:00" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="Cho thuê áo dài cưới đẹp tại Vaughan, Ontario, Canada" />
        <meta
            name="twitter:description"
            content="Cho thuê áo dài cưới tại Vaughan, Ontario, Canada, địa điểm cho thuê áo dài cưới đẹp, sửa áo quần ở đâu tại Vauhan, Ontario, Canada"
        />
        <script type="application/ld+json" class="aioseo-schema">
            {
                "@context": "https:\/\/schema.org",
                "@graph": [
                    {
                        "@type": "WebSite",
                        "@id": "httpsstaging.dndcleaners.ca\/#website",
                        "url": "httpsstaging.dndcleaners.ca\/",
                        "name": "DND Cleaners",
                        "description": "Professional Alternations & Repairs",
                        "inLanguage": "vi",
                        "publisher": { "@id": "httpsstaging.dndcleaners.ca\/#organization" }
                    },
                    {
                        "@type": "Organization",
                        "@id": "httpsstaging.dndcleaners.ca\/#organization",
                        "name": "DND Cleaners",
                        "url": "httpsstaging.dndcleaners.ca\/"
                    },
                    {
                        "@type": "BreadcrumbList",
                        "@id": "httpsstaging.dndcleaners.ca\/cho-thue-ao-dai-cuoi-dep-tai-vaughan-ontario-canada\/#breadcrumblist",
                        "itemListElement": [
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/#listItem",
                                "position": 1,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/",
                                    "name": "Home",
                                    "description": "D&D Cleaners chuyên may, cho thuê áo cưới, sửa áo quần, giặt ủi, bán dầu gió, thực phẩm chức năng tại Vaughan, Ontario, Canada",
                                    "url": "httpsstaging.dndcleaners.ca\/"
                                },
                                "nextItem": "httpsstaging.dndcleaners.ca\/cho-thue-ao-dai-cuoi-dep-tai-vaughan-ontario-canada\/#listItem"
                            },
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/cho-thue-ao-dai-cuoi-dep-tai-vaughan-ontario-canada\/#listItem",
                                "position": 2,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/cho-thue-ao-dai-cuoi-dep-tai-vaughan-ontario-canada\/",
                                    "name": "Cho thuê áo dài cưới đẹp tại Vaughan, Ontario, Canada",
                                    "description": "Cho thuê áo dài cưới tại Vaughan, Ontario, Canada, địa điểm cho thuê áo dài cưới đẹp, sửa áo quần ở đâu tại Vauhan, Ontario, Canada",
                                    "url": "httpsstaging.dndcleaners.ca\/cho-thue-ao-dai-cuoi-dep-tai-vaughan-ontario-canada\/"
                                },
                                "previousItem": "httpsstaging.dndcleaners.ca\/#listItem"
                            }
                        ]
                    },
                    {
                        "@type": "Person",
                        "@id": "httpsstaging.dndcleaners.ca\/author\/luanluan\/#author",
                        "url": "httpsstaging.dndcleaners.ca\/author\/luanluan\/",
                        "name": "luanluan",
                        "image": {
                            "@type": "ImageObject",
                            "@id": "httpsstaging.dndcleaners.ca\/cho-thue-ao-dai-cuoi-dep-tai-vaughan-ontario-canada\/#authorImage",
                            "url": "https:\/\/secure.gravatar.com\/avatar\/160f6bba04e65759c621e57bdb89648b?s=96&d=mm&r=g",
                            "width": 96,
                            "height": 96,
                            "caption": "luanluan"
                        }
                    },
                    {
                        "@type": "WebPage",
                        "@id": "httpsstaging.dndcleaners.ca\/cho-thue-ao-dai-cuoi-dep-tai-vaughan-ontario-canada\/#webpage",
                        "url": "httpsstaging.dndcleaners.ca\/cho-thue-ao-dai-cuoi-dep-tai-vaughan-ontario-canada\/",
                        "name": "Cho thuê áo dài cưới đẹp tại Vaughan, Ontario, Canada",
                        "description": "Cho thuê áo dài cưới tại Vaughan, Ontario, Canada, địa điểm cho thuê áo dài cưới đẹp, sửa áo quần ở đâu tại Vauhan, Ontario, Canada",
                        "inLanguage": "vi",
                        "isPartOf": { "@id": "httpsstaging.dndcleaners.ca\/#website" },
                        "breadcrumb": {
                            "@id": "httpsstaging.dndcleaners.ca\/cho-thue-ao-dai-cuoi-dep-tai-vaughan-ontario-canada\/#breadcrumblist"
                        },
                        "author": "httpsstaging.dndcleaners.ca\/author\/luanluan\/#author",
                        "creator": "httpsstaging.dndcleaners.ca\/author\/luanluan\/#author",
                        "image": {
                            "@type": "ImageObject",
                            "@id": "httpsstaging.dndcleaners.ca\/#mainImage",
                            "url": "httpsstaging.dndcleaners.ca\/wp-content\/uploads\/2021\/03\/dnd-cleaners-gallery-shop-2.jpg",
                            "width": 1536,
                            "height": 1024
                        },
                        "primaryImageOfPage": {
                            "@id": "httpsstaging.dndcleaners.ca\/cho-thue-ao-dai-cuoi-dep-tai-vaughan-ontario-canada\/#mainImage"
                        },
                        "datePublished": "2021-11-15T06:27:12+00:00",
                        "dateModified": "2021-12-26T13:48:26+00:00"
                    },
                    {
                        "@type": "BlogPosting",
                        "@id": "httpsstaging.dndcleaners.ca\/cho-thue-ao-dai-cuoi-dep-tai-vaughan-ontario-canada\/#blogposting",
                        "name": "Cho thuê áo dài cưới đẹp tại Vaughan, Ontario, Canada",
                        "description": "Cho thuê áo dài cưới tại Vaughan, Ontario, Canada, địa điểm cho thuê áo dài cưới đẹp, sửa áo quần ở đâu tại Vauhan, Ontario, Canada",
                        "inLanguage": "vi",
                        "headline": "Cho thuê áo dài cưới đẹp tại Vaughan, Ontario, Canada",
                        "author": { "@id": "httpsstaging.dndcleaners.ca\/author\/luanluan\/#author" },
                        "publisher": { "@id": "httpsstaging.dndcleaners.ca\/#organization" },
                        "datePublished": "2021-11-15T06:27:12+00:00",
                        "dateModified": "2021-12-26T13:48:26+00:00",
                        "articleSection": "Tin tức, cho thuê áo dài cưới tại Vaughan, dịch vụ cho thuê áo dài cưới tại Canada, thuê áo dài cưới tại Canada, Tiếng Việt",
                        "mainEntityOfPage": {
                            "@id": "httpsstaging.dndcleaners.ca\/cho-thue-ao-dai-cuoi-dep-tai-vaughan-ontario-canada\/#webpage"
                        },
                        "isPartOf": {
                            "@id": "httpsstaging.dndcleaners.ca\/cho-thue-ao-dai-cuoi-dep-tai-vaughan-ontario-canada\/#webpage"
                        },
                        "image": {
                            "@type": "ImageObject",
                            "@id": "httpsstaging.dndcleaners.ca\/#articleImage",
                            "url": "httpsstaging.dndcleaners.ca\/wp-content\/uploads\/2021\/03\/dnd-cleaners-gallery-shop-2.jpg",
                            "width": 1536,
                            "height": 1024
                        }
                    }
                ]
            }
        </script>
        <!-- All in One SEO -->

        @include('public.header')
    </head>
    {{$current='blog-detail'}}
    <body
        class="post-template-default single single-post postid-774 single-format-standard theme-Divi woocommerce-no-js et_pb_button_helper_class et_fixed_nav et_show_nav et_pb_show_title et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_header_style_left et_pb_footer_columns4 et_cover_background et_pb_gutter et_pb_gutters3 et_smooth_scroll et_right_sidebar et_divi_theme et-db"
    >
        <div id="page-container">
            <header id="main-header" data-height-onload="66">
                <div class="container clearfix et_menu_container">
                    <div class="logo_container">
                        <span class="logo_helper"></span>
                        <a href="/">
                            <img
                                src="/wp-content/uploads/2021/03/DD-cleaners.jpg"
                                alt="DND Cleaners"
                                id="logo"
                                data-height-percentage="87"
                            />
                        </a>
                    </div>
                    <div id="et-top-navigation" data-height="66" data-fixed-height="40">
                        @include('public.navbar')

                        <a href="/cart/" class="et-cart-info">
                            <span></span>
                        </a>

                        <div id="et_mobile_nav_menu">
                            <div class="mobile_nav closed">
                                <span class="select_page">Chọn trang</span>
                                <span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
                            </div>
                        </div>
                    </div>
                    <!-- #et-top-navigation -->
                </div>
                <!-- .container -->
                <div class="et_search_outer">
                    <div class="container et_search_form_container">
                        <form
                            role="search"
                            method="get"
                            class="et-search-form"
                            action="https://staging.dndcleaners.ca/"
                        >
                            <input
                                type="search"
                                class="et-search-field"
                                placeholder="Tìm kiếm &hellip;"
                                value=""
                                name="s"
                                title="Tìm:"
                            />
                        </form>
                        <span class="et_close_search_field"></span>
                    </div>
                </div>
            </header>
            <!-- #main-header -->
            <div id="et-main-area">
                <div id="main-content">
                    <div class="container">
                        <div id="content-area" class="clearfix">
                            <div id="left-area">
                                <article
                                    id="post-774"
                                    class="et_pb_post post-774 post type-post status-publish format-standard has-post-thumbnail hentry category-blog tag-cho-thue-ao-dai-cuoi-tai-vaughan tag-dich-vu-cho-thue-ao-dai-cuoi-tai-canada tag-thue-ao-dai-cuoi-tai-canada"
                                >
                                    <div class="et_post_meta_wrapper">
                                        <h1 class="entry-title">
                                            Cho thuê áo dài cưới đẹp tại Vaughan, Ontario, Canada
                                        </h1>

                                        <p class="post-meta">
                                            bởi
                                            <span class="author vcard"
                                                ><a
                                                    href="/author/luanluan/"
                                                    title="Các bài đăng của luanluan"
                                                    rel="author"
                                                    >luanluan</a
                                                ></span
                                            >
                                            | <span class="published">Th11 15, 2021</span> |
                                            <a href="/category/blog/" rel="category tag">Tin tức</a> |
                                            <span class="comments-number"
                                                ><a href="/cho-thue-ao-dai-cuoi-dep-tai-vaughan-ontario-canada/#respond"
                                                    >0 Lời bình</a
                                                ></span
                                            >
                                        </p>
                                        <img
                                            src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-2-1080x675.jpg"
                                            alt=""
                                            class=""
                                            width="1080"
                                            height="675"
                                            srcset="
                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-2-980x653.jpg 980w,
                                                /wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-2-480x320.jpg 480w
                                            "
                                            sizes="(min-width: 0px) and (max-width: 480px) 480px, (min-width: 481px) and (max-width: 980px) 980px, (min-width: 981px) 1080px, 100vw"
                                        />
                                    </div>
                                    <!-- .et_post_meta_wrapper -->

                                    <div class="entry-content">
                                        <p>
                                            <em
                                                ><strong
                                                    >D&amp;DCleaner, may và cho thuê áo cưới đẹp ở Vaughan, Ontario,
                                                    Canada. Ngoài ra chúng tôi có dịch vụ cho thuê áo dài cưới đẹp, giá
                                                    cả phải chăng, mẫu mã đa dạng. Đặc biệt,
                                                    <a href="/">D&amp;D Cleaner</a> còn cung thêm dịch vụ cưới hỏi tại
                                                    Canada.</strong
                                                ></em
                                            >
                                        </p>
                                        <p>
                                            Cho dù bạn sống ở Canada nhưng muốn lưu giữ văn hoá Việt Nam, vì vậy, mỗi
                                            khi có sự kiện cưới hỏi, áo cưới, áo dài cưới là trang phục không thể thiếu.
                                            Áo cưới từ lâu đã là một trong những trang phục cưới được các cô dâu và chú
                                            rể yêu thích lựa chọn cho ngày trọng đại của mình.
                                        </p>
                                        <p>
                                            Áo cưới, áo dài cưới không chỉ là một nét đẹp văn hóa truyền thống của người
                                            Việt, những bộ áo dài cưới còn là biểu tượng cho sự may mắn và giúp tôn lên
                                            những vẻ đẹp của các cặp uyên ương. Chính vì vậy, việc thuê áo dài cưới ngày
                                            vu quy đã trở nên quen thuộc đối với các cặp đôi trẻ.
                                        </p>
                                        <p>
                                            Tuy nhiên, làm sao để thuê được một bộ áo dài cưới vừa đẹp vừa đúng với văn
                                            hoá truyền thống Việt Nam tại Canada thì không phải ai cũng biết.
                                        </p>
                                        <p>
                                            <img
                                                loading="lazy"
                                                class="size-medium wp-image-683 aligncenter"
                                                src="/wp-content/uploads/2021/03/dnd-cleaners-gallery-shop-10-300x200.jpg"
                                                alt=""
                                                width="300"
                                                height="200"
                                            />
                                        </p>
                                        <h3><strong>Tại sao nên thuê áo dài cưới</strong></h3>
                                        <p>
                                            Để may máo dài cưới thì chi phí thường không phải thấp. Đa phần các bộ áo
                                            dài cưới này chỉ để mặc trong ngày trọng đại nên sau dịp ấy còn rất hiếm các
                                            dịp nào khác để có thể diện chúng. Mặt khác, khi thuê áo dài cưới, bạn có
                                            thể lựa chọn nhiều mẫu mả đa dạng về thiết kế, màu sắc cũng như kiểu dáng
                                            phù hợp nhất với bản thân. Bởi vậy, việc thuê váy cưới trở nên đơn giản hơn
                                            nhiều và là sự lựa chọn tối ưu giúp các cặp đôi tiết kiệm được chi phí tổ
                                            chức đám cưới tối đa.
                                        </p>
                                        <p>
                                            Vì những lý do trên, việc cho thuê áo dài cưới<strong> </strong>dần trở nên
                                            phổ biến, đặc biệt là tại Vaughan, Canada. D&amp;DCleaner đã cho ra đời dịch
                                            vụ cho thuê áo dài cưới nhiều kiểu dáng, chất liệu đa dạng ngay tại Canada.
                                        </p>
                                        <h3>
                                            <strong
                                                >Tại sao nên lựa chọn dịch vụ cho thuê áo dài cưới tại D&amp;DCleaners ở
                                                Canada</strong
                                            >
                                        </h3>
                                        <p>
                                            D&amp;DCleaner là dịch vụ được ra đời nhằm mang đến dịch vụ tốt nhất cho
                                            người Việt và những ai sinh sống tại Canada khi muốn sử dụng những trang
                                            phục truyền thống Việt Nam. D&amp;DCleaner được nhiều khách hàng lựa chọn vì
                                            sự uy tín, chất lượng và khách hàng luôn hài lòng khi sử dụng dịch vụ tại
                                            đây.
                                        </p>
                                        <h4 style="text-align: center">
                                            Liên hệ với chúng tôi qua số điện thoại (905) 738-9991 hoặc địa chỉ: 3255
                                            Rutherford Road, unit J19, Vaughan, ON<br />
                                            Website: https://staging.dndcleaners.ca/contact-us/
                                        </h4>
                                        <blockquote>
                                            <p>Xêm thêm:</p>
                                            <p>
                                                https://staging.dndcleaners.ca/cach-sua-ao-dai-bi-chat-tai-vaughan-ontario-canada/
                                            </p>
                                        </blockquote>
                                    </div>
                                    <!-- .entry-content -->
                                    <div class="et_post_meta_wrapper">
                                        <!-- You can start editing here. -->

                                        <section id="comment-wrap">
                                            <div id="comment-section" class="nocomments">
                                                <!-- If comments are open, but there are no comments. -->
                                            </div>
                                            <div id="respond" class="comment-respond">
                                                <h3 id="reply-title" class="comment-reply-title">
                                                    <span>Gửi Lời bình</span>
                                                    <small
                                                        ><a
                                                            rel="nofollow"
                                                            id="cancel-comment-reply-link"
                                                            href="/cho-thue-ao-dai-cuoi-dep-tai-vaughan-ontario-canada/#respond"
                                                            style="display: none"
                                                            >Hủy</a
                                                        ></small
                                                    >
                                                </h3>
                                                <form
                                                    action="https://staging.dndcleaners.ca/wp-comments-post.php"
                                                    method="post"
                                                    id="commentform"
                                                    class="comment-form"
                                                >
                                                    <p class="comment-notes">
                                                        <span id="email-notes"
                                                            >Email của bạn sẽ không được hiển thị công khai.</span
                                                        >
                                                        <span class="required-field-message" aria-hidden="true"
                                                            >Các trường bắt buộc được đánh dấu
                                                            <span class="required" aria-hidden="true">*</span></span
                                                        >
                                                    </p>
                                                    <p class="comment-form-comment">
                                                        <label for="comment"
                                                            >Bình luận
                                                            <span class="required" aria-hidden="true">*</span></label
                                                        >
                                                        <textarea
                                                            id="comment"
                                                            name="comment"
                                                            cols="45"
                                                            rows="8"
                                                            maxlength="65525"
                                                            required="required"
                                                        ></textarea>
                                                    </p>
                                                    <p class="comment-form-author">
                                                        <label for="author"
                                                            >Tên
                                                            <span class="required" aria-hidden="true">*</span></label
                                                        >
                                                        <input
                                                            id="author"
                                                            name="author"
                                                            type="text"
                                                            value=""
                                                            size="30"
                                                            maxlength="245"
                                                            required="required"
                                                        />
                                                    </p>
                                                    <p class="comment-form-email">
                                                        <label for="email"
                                                            >Email
                                                            <span class="required" aria-hidden="true">*</span></label
                                                        >
                                                        <input
                                                            id="email"
                                                            name="email"
                                                            type="text"
                                                            value=""
                                                            size="30"
                                                            maxlength="100"
                                                            aria-describedby="email-notes"
                                                            required="required"
                                                        />
                                                    </p>
                                                    <p class="comment-form-url">
                                                        <label for="url">Trang web</label>
                                                        <input
                                                            id="url"
                                                            name="url"
                                                            type="text"
                                                            value=""
                                                            size="30"
                                                            maxlength="200"
                                                        />
                                                    </p>
                                                    <p class="comment-form-cookies-consent">
                                                        <input
                                                            id="wp-comment-cookies-consent"
                                                            name="wp-comment-cookies-consent"
                                                            type="checkbox"
                                                            value="yes"
                                                        />
                                                        <label for="wp-comment-cookies-consent"
                                                            >Lưu tên của tôi, email, và trang web trong trình duyệt này
                                                            cho lần bình luận kế tiếp của tôi.</label
                                                        >
                                                    </p>
                                                    <p class="form-submit">
                                                        <input
                                                            name="submit"
                                                            type="submit"
                                                            id="submit"
                                                            class="submit et_pb_button"
                                                            value="Gửi Lời bình"
                                                        />
                                                        <input
                                                            type="hidden"
                                                            name="comment_post_ID"
                                                            value="774"
                                                            id="comment_post_ID"
                                                        />
                                                        <input
                                                            type="hidden"
                                                            name="comment_parent"
                                                            id="comment_parent"
                                                            value="0"
                                                        />
                                                    </p>
                                                </form>
                                            </div>
                                            <!-- #respond -->
                                        </section>
                                    </div>
                                    <!-- .et_post_meta_wrapper -->
                                </article>
                                <!-- .et_pb_post -->
                            </div>
                            <!-- #left-area -->

                            <div id="sidebar">
                                <div id="block-4" class="et_pb_widget widget_block">
                                    <h2>Tìm sản phẩm khác</h2>
                                </div>
                                <!-- end .et_pb_widget -->
                                <div id="block-2" class="et_pb_widget widget_block">
                                    <div
                                        data-block-name="woocommerce/product-search"
                                        data-label=""
                                        data-form-id="wc-block-product-search-2"
                                        class="wc-block-product-search wp-block-woocommerce-product-search"
                                    >
                                        <form role="search" method="get" action="https://staging.dndcleaners.ca/">
                                            <label
                                                for="wc-block-search__input-1"
                                                class="wc-block-product-search__label"
                                            ></label>
                                            <div class="wc-block-product-search__fields">
                                                <input
                                                    type="search"
                                                    id="wc-block-search__input-1"
                                                    class="wc-block-product-search__field"
                                                    placeholder="Tìm kiếm sản phẩm..."
                                                    name="s"
                                                /><button
                                                    type="submit"
                                                    class="wc-block-product-search__button"
                                                    aria-label="Tìm kiếm"
                                                >
                                                    <svg
                                                        aria-hidden="true"
                                                        role="img"
                                                        focusable="false"
                                                        class="dashicon dashicons-arrow-right-alt2"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                        width="20"
                                                        height="20"
                                                        viewbox="0 0 20 20"
                                                    >
                                                        <path d="M6 15l5-5-5-5 1-2 7 7-7 7z"></path>
                                                    </svg>
                                                </button>
                                                <input type="hidden" name="post_type" value="product" />
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- end .et_pb_widget -->
                                <div id="block-5" class="et_pb_widget widget_block">
                                    <h2>Danh mục sản phẩm</h2>
                                </div>
                                <!-- end .et_pb_widget -->
                                <div id="block-3" class="et_pb_widget widget_block">
                                    <div
                                        data-block-name="woocommerce/product-categories"
                                        class="wp-block-woocommerce-product-categories wc-block-product-categories is-list"
                                    >
                                        <ul
                                            class="wc-block-product-categories-list wc-block-product-categories-list--depth-0"
                                        >
                                            <li class="wc-block-product-categories-list-item">
                                                <a href="/product-category/san-pham/">Sản phẩm</a>
                                                <span class="wc-block-product-categories-list-item-count"
                                                    ><span aria-hidden="true">40</span
                                                    ><span class="screen-reader-text">40 sản phẩm</span></span
                                                >
                                                <ul
                                                    class="wc-block-product-categories-list wc-block-product-categories-list--depth-1"
                                                >
                                                    <li class="wc-block-product-categories-list-item">
                                                        <a href="/product-category/san-pham/ao-dai/">Áo dài</a>
                                                        <span class="wc-block-product-categories-list-item-count"
                                                            ><span aria-hidden="true">33</span
                                                            ><span class="screen-reader-text">33 sản phẩm</span></span
                                                        >
                                                    </li>
                                                    <li class="wc-block-product-categories-list-item">
                                                        <a href="/product-category/san-pham/my-pham/">Mỹ phẩm</a>
                                                        <span class="wc-block-product-categories-list-item-count"
                                                            ><span aria-hidden="true">7</span
                                                            ><span class="screen-reader-text">7 sản phẩm</span></span
                                                        >
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- end .et_pb_widget -->
                            </div>
                            <!-- end #sidebar -->
                        </div>
                        <!-- #content-area -->
                    </div>
                    <!-- .container -->
                </div>
                <!-- #main-content -->

                <span class="et_pb_scroll_top et-pb-icon"></span>

                <footer id="main-footer">
                    <div id="footer-bottom">
                        <div class="container clearfix">
                            <div id="footer-info">D&amp;D CLEANERS | Copyright © 2021. All Rights Reserved.</div>
                        </div>
                        <!-- .container -->
                    </div>
                </footer>
                <!-- #main-footer -->
            </div>
            <!-- #et-main-area -->
        </div>
        <!-- #page-container -->

        
        <script type="text/javascript" id="et-builder-modules-script-js-extra">
            /* <![CDATA[ */
            var et_frontend_scripts = { builderCssContainerPrefix: "#et-boc", builderCssLayoutPrefix: "#et-boc .et-l" };
            var et_pb_custom = {
                ajaxurl: "httpsstaging.dndcleaners.ca\/wp-admin\/admin-ajax.php",
                images_uri: "httpsstaging.dndcleaners.ca\/wp-content\/themes\/Divi\/images",
                builder_images_uri: "httpsstaging.dndcleaners.ca\/wp-content\/themes\/Divi\/includes\/builder\/images",
                et_frontend_nonce: "4e47d1f20d",
                subscription_failed: "Làm ơn, kiểm tra đồng dưới chắc chắn để các ông gõ đúng thông tin.",
                et_ab_log_nonce: "0f432c9a55",
                fill_message: "Xin vui lòng, điền vào các lĩnh vực sau đây:",
                contact_error_message: "Xin hãy sửa những lỗi sau:",
                invalid: "Không hợp lệ email",
                captcha: "Captcha",
                prev: "Trước",
                previous: "Trước",
                next: "Tiếp theo",
                wrong_captcha: "Bạn đã nhập sai số captcha.",
                wrong_checkbox: "Checkbox",
                ignore_waypoints: "no",
                is_divi_theme_used: "1",
                widget_search_selector: ".widget_search",
                ab_tests: [],
                is_ab_testing_active: "",
                page_id: "774",
                unique_test_id: "",
                ab_bounce_rate: "5",
                is_cache_plugin_active: "no",
                is_shortcode_tracking: "",
                tinymce_uri: "",
            };
            var et_pb_box_shadow_elements = [];
            var et_pb_motion_elements = { desktop: [], tablet: [], phone: [] };
            var et_pb_sticky_elements = [];
            /* ]]> */
        </script>
        @include('public.script')
    </body>

    <!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/

Page Caching using disk: enhanced 

Served from: staging.dndcleaners.ca @ 2022-12-27 15:13:55 by W3 Total Cache
--></html>
