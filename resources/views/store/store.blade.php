<!DOCTYPE html>
<html dir="ltr" lang="vi" prefix="og: https://ogp.me/ns#">
    <head>
       
        <title>Sản phẩm - DND Cleaners</title>

        <!-- All in One SEO 4.2.0 -->
        <meta
            name="description"
            content="Hiển thị 1–12 của 40 kết quả Thứ tự mặc định Thứ tự theo mức độ phổ biến Mới nhất Thứ tự theo giá: thấp đến cao Thứ tự theo giá: cao xuống thấp Áo dài đen hoa văn nổi Áo dài đỏ họa tiết 3D Áo dài đỏ hoa văn 3D Áo dài đỏ"
        />
        <meta name="robots" content="noindex, nofollow, max-image-preview:large" />
        <link rel="canonical" href="/store/" />
        <meta property="og:locale" content="vi_VN" />
        <meta property="og:site_name" content="DND Cleaners - Professional Alternations &amp; Repairs" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="Sản phẩm - DND Cleaners" />
        <meta
            property="og:description"
            content="Hiển thị 1–12 của 40 kết quả Thứ tự mặc định Thứ tự theo mức độ phổ biến Mới nhất Thứ tự theo giá: thấp đến cao Thứ tự theo giá: cao xuống thấp Áo dài đen hoa văn nổi Áo dài đỏ họa tiết 3D Áo dài đỏ hoa văn 3D Áo dài đỏ"
        />
        <meta property="og:url" content="/store/" />
        <meta property="og:image" content="/wp-content/uploads/2021/11/househouse.jpg" />
        <meta property="og:image:secure_url" content="/wp-content/uploads/2021/11/househouse.jpg" />
        <meta property="og:image:width" content="1024" />
        <meta property="og:image:height" content="700" />
        <meta property="article:published_time" content="2022-04-04T08:53:08+00:00" />
        <meta property="article:modified_time" content="2022-05-23T04:29:37+00:00" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="Sản phẩm - DND Cleaners" />
        <meta
            name="twitter:description"
            content="Hiển thị 1–12 của 40 kết quả Thứ tự mặc định Thứ tự theo mức độ phổ biến Mới nhất Thứ tự theo giá: thấp đến cao Thứ tự theo giá: cao xuống thấp Áo dài đen hoa văn nổi Áo dài đỏ họa tiết 3D Áo dài đỏ hoa văn 3D Áo dài đỏ"
        />
        <meta name="twitter:image" content="/wp-content/uploads/2021/11/househouse.jpg" />
        <script type="application/ld+json" class="aioseo-schema">
            {
                "@context": "https:\/\/schema.org",
                "@graph": [
                    {
                        "@type": "WebSite",
                        "@id": "httpsstaging.dndcleaners.ca\/#website",
                        "url": "httpsstaging.dndcleaners.ca\/",
                        "name": "DND Cleaners",
                        "description": "Professional Alternations & Repairs",
                        "inLanguage": "vi",
                        "publisher": { "@id": "httpsstaging.dndcleaners.ca\/#organization" }
                    },
                    {
                        "@type": "Organization",
                        "@id": "httpsstaging.dndcleaners.ca\/#organization",
                        "name": "DND Cleaners",
                        "url": "httpsstaging.dndcleaners.ca\/"
                    },
                    {
                        "@type": "BreadcrumbList",
                        "@id": "httpsstaging.dndcleaners.ca\/store\/#breadcrumblist",
                        "itemListElement": [
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/#listItem",
                                "position": 1,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/",
                                    "name": "Home",
                                    "description": "D&D Cleaners chuyên may, cho thuê áo cưới, sửa áo quần, giặt ủi, bán dầu gió, thực phẩm chức năng tại Vaughan, Ontario, Canada",
                                    "url": "httpsstaging.dndcleaners.ca\/"
                                },
                                "nextItem": "httpsstaging.dndcleaners.ca\/store\/#listItem"
                            },
                            {
                                "@type": "ListItem",
                                "@id": "httpsstaging.dndcleaners.ca\/store\/#listItem",
                                "position": 2,
                                "item": {
                                    "@type": "WebPage",
                                    "@id": "httpsstaging.dndcleaners.ca\/store\/",
                                    "name": "Sản phẩm",
                                    "description": "Hiển thị 1–12 của 40 kết quả Thứ tự mặc định Thứ tự theo mức độ phổ biến Mới nhất Thứ tự theo giá: thấp đến cao Thứ tự theo giá: cao xuống thấp Áo dài đen hoa văn nổi Áo dài đỏ họa tiết 3D Áo dài đỏ hoa văn 3D Áo dài đỏ",
                                    "url": "httpsstaging.dndcleaners.ca\/store\/"
                                },
                                "previousItem": "httpsstaging.dndcleaners.ca\/#listItem"
                            }
                        ]
                    },
                    {
                        "@type": "WebPage",
                        "@id": "httpsstaging.dndcleaners.ca\/store\/#webpage",
                        "url": "httpsstaging.dndcleaners.ca\/store\/",
                        "name": "Sản phẩm - DND Cleaners",
                        "description": "Hiển thị 1–12 của 40 kết quả Thứ tự mặc định Thứ tự theo mức độ phổ biến Mới nhất Thứ tự theo giá: thấp đến cao Thứ tự theo giá: cao xuống thấp Áo dài đen hoa văn nổi Áo dài đỏ họa tiết 3D Áo dài đỏ hoa văn 3D Áo dài đỏ",
                        "inLanguage": "vi",
                        "isPartOf": { "@id": "httpsstaging.dndcleaners.ca\/#website" },
                        "breadcrumb": { "@id": "httpsstaging.dndcleaners.ca\/store\/#breadcrumblist" },
                        "image": {
                            "@type": "ImageObject",
                            "@id": "httpsstaging.dndcleaners.ca\/#mainImage",
                            "url": "httpsstaging.dndcleaners.ca\/wp-content\/uploads\/2021\/11\/househouse.jpg",
                            "width": 1024,
                            "height": 700
                        },
                        "primaryImageOfPage": { "@id": "httpsstaging.dndcleaners.ca\/store\/#mainImage" },
                        "datePublished": "2022-04-04T08:53:08+00:00",
                        "dateModified": "2022-05-23T04:29:37+00:00"
                    }
                ]
            }
        </script>
        <!-- All in One SEO -->

        <link rel="alternate" href="/store/" hreflang="vi" />
        <link rel="alternate" href="/en/store/" hreflang="en" />
        @include('public.header')
    </head>
    {{$current='store'}}
    <body
        class="page-template-default page page-id-1186 theme-Divi woocommerce-no-js et_pb_button_helper_class et_fixed_nav et_show_nav et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_header_style_left et_pb_footer_columns4 et_cover_background et_pb_gutter et_pb_gutters3 et_pb_pagebuilder_layout et_smooth_scroll et_no_sidebar et_divi_theme et-db"
    >
        <div id="page-container">
            <header id="main-header" data-height-onload="66">
                <div class="container clearfix et_menu_container">
                    <div class="logo_container">
                        <span class="logo_helper"></span>
                        <a href="/">
                            <img
                                src="/wp-content/uploads/2021/03/DD-cleaners.jpg"
                                alt="DND Cleaners"
                                id="logo"
                                data-height-percentage="87"
                            />
                        </a>
                    </div>
                    <div id="et-top-navigation" data-height="66" data-fixed-height="40">
                        @include('public.navbar')
                        <a href="/cart/" class="et-cart-info">
                            <span></span>
                        </a>

                        <div id="et_mobile_nav_menu">
                            <div class="mobile_nav closed">
                                <span class="select_page">Chọn trang</span>
                                <span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
                            </div>
                        </div>
                    </div>
                    <!-- #et-top-navigation -->
                </div>
                <!-- .container -->
                <div class="et_search_outer">
                    <div class="container et_search_form_container">
                        <form
                            role="search"
                            method="get"
                            class="et-search-form"
                            action="https://staging.dndcleaners.ca/"
                        >
                            <input
                                type="search"
                                class="et-search-field"
                                placeholder="Tìm kiếm &hellip;"
                                value=""
                                name="s"
                                title="Tìm:"
                            />
                        </form>
                        <span class="et_close_search_field"></span>
                    </div>
                </div>
            </header>
            <!-- #main-header -->
            <div id="et-main-area">
                <div id="main-content">
                    <article id="post-1186" class="post-1186 page type-page status-publish has-post-thumbnail hentry">
                        <div class="entry-content">
                            <div id="et-boc" class="et-boc">
                                <div class="et-l et-l--post">
                                    <div class="et_builder_inner_content et_pb_gutters3">
                                        <div class="et_pb_section et_pb_section_2 et_section_regular">
                                            <div class="et_pb_row et_pb_row_2">
                                                <div
                                                    class="et_pb_column et_pb_column_4_4 et_pb_column_2 et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div class="et_pb_module lwp_image_carousel lwp_image_carousel_1">
                                                        <div class="et_pb_module_inner">
                                                            <section
                                                                class="lwp-slick-slider slider small-slider lwp-default-slider lwp-side-center"
                                                                data-slick='{ "vertical":false, "slidesToShow": 1, "slidesToScroll": 1, "dots":false, "arrows":false, "infinite":false, "autoplay":true, "autoplaySpeed":2000, "pauseOnHover":true, "adaptiveHeight":true, "speed":300, "responsive": [ { "breakpoint": 980, "settings": { "slidesToShow": 1, "slidesToScroll": 1, "arrows":false,"dots":false } } ,{ "breakpoint": 767, "settings": { "slidesToShow": 1, "slidesToScroll": 1, "arrows":false,"dots":false } } ] }'
                                                            >
                                                                <div>
                                                                    <img
                                                                        src="/wp-content/uploads/2022/05/Slider-2.png"
                                                                        alt=""
                                                                        title="Slider (2)"
                                                                    />
                                                                </div>
                                                                <div>
                                                                    <img
                                                                        src="/wp-content/uploads/2022/05/Slider-1.png"
                                                                        alt=""
                                                                        title="Slider (1)"
                                                                    />
                                                                </div>
                                                            </section>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                        </div>
                                        <!-- .et_pb_section -->
                                        <div class="et_pb_section et_pb_section_3 pro et_section_regular">
                                            <div class="et_pb_row et_pb_row_3">
                                                <div
                                                    class="et_pb_column et_pb_column_4_4 et_pb_column_3 et_pb_css_mix_blend_mode_passthrough et-last-child"
                                                >
                                                    <div
                                                        class="et_pb_with_border et_pb_module et_pb_shop et_pb_shop_1 et_pb_shop_grid"
                                                        data-shortcode_index="1"
                                                    >
                                                        <div class="woocommerce columns-4">
                                                            <div class="woocommerce-notices-wrapper"></div>
                                                            <p class="woocommerce-result-count">
                                                                Hiển thị 1&ndash;12 của 40 kết quả
                                                            </p>
                                                            <form class="woocommerce-ordering" method="get">
                                                                <select
                                                                    name="orderby"
                                                                    class="orderby"
                                                                    aria-label="Đơn hàng của cửa hàng"
                                                                >
                                                                    <option value="menu_order" selected>
                                                                        Thứ tự mặc định
                                                                    </option>
                                                                    <option value="popularity">
                                                                        Thứ tự theo mức độ phổ biến
                                                                    </option>
                                                                    <option value="date">Mới nhất</option>
                                                                    <option value="price">
                                                                        Thứ tự theo giá: thấp đến cao
                                                                    </option>
                                                                    <option value="price-desc">
                                                                        Thứ tự theo giá: cao xuống thấp
                                                                    </option>
                                                                </select>
                                                                <input type="hidden" name="paged" value="1" />
                                                            </form>
                                                            <ul class="products columns-4">
                                                                <li
                                                                    class="product type-product post-1745 status-publish first instock product_cat-ao-dai has-post-thumbnail shipping-taxable product-type-simple"
                                                                >
                                                                    <a
                                                                        href="/product/ao-dai-den-hoa-van-noi/"
                                                                        class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                                                        ><span class="et_shop_image"
                                                                            ><img
                                                                                width="300"
                                                                                height="300"
                                                                                src="/wp-content/uploads/2022/05/Ao-dai-den-hoa-van-noi-300x300.jpg"
                                                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                alt=""
                                                                                loading="lazy" /><span
                                                                                class="et_overlay"
                                                                            ></span
                                                                        ></span>
                                                                        <h2 class="woocommerce-loop-product__title">
                                                                            Áo dài đen hoa văn nổi
                                                                        </h2>
                                                                    </a>
                                                                </li>
                                                                <li
                                                                    class="product type-product post-1747 status-publish instock product_cat-ao-dai has-post-thumbnail shipping-taxable product-type-simple"
                                                                >
                                                                    <a
                                                                        href="/product/ao-dai-do-hoa-tiet-3d/"
                                                                        class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                                                        ><span class="et_shop_image"
                                                                            ><img
                                                                                width="300"
                                                                                height="300"
                                                                                src="/wp-content/uploads/2022/05/Ao-dai-do-hoa-tiet-3D-300x300.jpg"
                                                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                alt=""
                                                                                loading="lazy" /><span
                                                                                class="et_overlay"
                                                                            ></span
                                                                        ></span>
                                                                        <h2 class="woocommerce-loop-product__title">
                                                                            Áo dài đỏ họa tiết 3D
                                                                        </h2>
                                                                    </a>
                                                                </li>
                                                                <li
                                                                    class="product type-product post-1749 status-publish instock product_cat-ao-dai has-post-thumbnail shipping-taxable product-type-simple"
                                                                >
                                                                    <a
                                                                        href="/product/ao-dai-do-hoa-van-3d/"
                                                                        class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                                                        ><span class="et_shop_image"
                                                                            ><img
                                                                                width="300"
                                                                                height="300"
                                                                                src="/wp-content/uploads/2022/05/Ao-dai-do-hoa-van-3D-300x300.jpg"
                                                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                alt=""
                                                                                loading="lazy" /><span
                                                                                class="et_overlay"
                                                                            ></span
                                                                        ></span>
                                                                        <h2 class="woocommerce-loop-product__title">
                                                                            Áo dài đỏ hoa văn 3D
                                                                        </h2>
                                                                    </a>
                                                                </li>
                                                                <li
                                                                    class="product type-product post-1751 status-publish last instock product_cat-ao-dai has-post-thumbnail shipping-taxable product-type-simple"
                                                                >
                                                                    <a
                                                                        href="/product/ao-dai-do-hoat-tiet-chim-hac/"
                                                                        class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                                                        ><span class="et_shop_image"
                                                                            ><img
                                                                                width="300"
                                                                                height="300"
                                                                                src="/wp-content/uploads/2022/05/Ao-dai-do-hoat-tiet-chim-hac-300x300.jpg"
                                                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                alt=""
                                                                                loading="lazy" /><span
                                                                                class="et_overlay"
                                                                            ></span
                                                                        ></span>
                                                                        <h2 class="woocommerce-loop-product__title">
                                                                            Áo dài đỏ hoạt tiết chim hạc
                                                                        </h2>
                                                                    </a>
                                                                </li>
                                                                <li
                                                                    class="product type-product post-1753 status-publish first instock product_cat-ao-dai has-post-thumbnail shipping-taxable product-type-simple"
                                                                >
                                                                    <a
                                                                        href="/product/ao-dai-do-in-hoa-van-3d/"
                                                                        class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                                                        ><span class="et_shop_image"
                                                                            ><img
                                                                                width="300"
                                                                                height="300"
                                                                                src="/wp-content/uploads/2022/05/Ao-dai-do-in-hoa-van-3D-300x300.jpg"
                                                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                alt=""
                                                                                loading="lazy" /><span
                                                                                class="et_overlay"
                                                                            ></span
                                                                        ></span>
                                                                        <h2 class="woocommerce-loop-product__title">
                                                                            Áo dài đỏ in hoa văn 3D
                                                                        </h2>
                                                                    </a>
                                                                </li>
                                                                <li
                                                                    class="product type-product post-1755 status-publish instock product_cat-ao-dai has-post-thumbnail shipping-taxable product-type-simple"
                                                                >
                                                                    <a
                                                                        href="/product/ao-dai-hoa-hong-hien-dai/"
                                                                        class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                                                        ><span class="et_shop_image"
                                                                            ><img
                                                                                width="300"
                                                                                height="300"
                                                                                src="/wp-content/uploads/2022/05/Ao-dai-hoa-hong-hien-dai-300x300.jpg"
                                                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                alt=""
                                                                                loading="lazy" /><span
                                                                                class="et_overlay"
                                                                            ></span
                                                                        ></span>
                                                                        <h2 class="woocommerce-loop-product__title">
                                                                            Áo dài hoa hồng hiện đại
                                                                        </h2>
                                                                    </a>
                                                                </li>
                                                                <li
                                                                    class="product type-product post-1757 status-publish instock product_cat-ao-dai has-post-thumbnail shipping-taxable product-type-simple"
                                                                >
                                                                    <a
                                                                        href="/product/ao-dai-hoa-sen-in-3d/"
                                                                        class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                                                        ><span class="et_shop_image"
                                                                            ><img
                                                                                width="300"
                                                                                height="300"
                                                                                src="/wp-content/uploads/2022/05/Ao-dai-hoa-sen-in-3D-300x300.jpg"
                                                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                alt=""
                                                                                loading="lazy" /><span
                                                                                class="et_overlay"
                                                                            ></span
                                                                        ></span>
                                                                        <h2 class="woocommerce-loop-product__title">
                                                                            Áo dài hoa sen in 3D
                                                                        </h2>
                                                                    </a>
                                                                </li>
                                                                <li
                                                                    class="product type-product post-1759 status-publish last instock product_cat-ao-dai has-post-thumbnail shipping-taxable product-type-simple"
                                                                >
                                                                    <a
                                                                        href="/product/ao-dai-hoa-tiet-canh-co/"
                                                                        class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                                                        ><span class="et_shop_image"
                                                                            ><img
                                                                                width="300"
                                                                                height="300"
                                                                                src="/wp-content/uploads/2022/05/Ao-dai-hoa-tiet-canh-co-300x300.jpg"
                                                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                alt=""
                                                                                loading="lazy" /><span
                                                                                class="et_overlay"
                                                                            ></span
                                                                        ></span>
                                                                        <h2 class="woocommerce-loop-product__title">
                                                                            Áo dài hoạ tiết cánh cò
                                                                        </h2>
                                                                    </a>
                                                                </li>
                                                                <li
                                                                    class="product type-product post-1761 status-publish first instock product_cat-ao-dai has-post-thumbnail shipping-taxable product-type-simple"
                                                                >
                                                                    <a
                                                                        href="/product/ao-dai-hoa-tiet-canh-quat/"
                                                                        class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                                                        ><span class="et_shop_image"
                                                                            ><img
                                                                                width="300"
                                                                                height="300"
                                                                                src="/wp-content/uploads/2022/05/Ao-dai-hoa-tiet-canh-quat-300x300.jpg"
                                                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                alt=""
                                                                                loading="lazy" /><span
                                                                                class="et_overlay"
                                                                            ></span
                                                                        ></span>
                                                                        <h2 class="woocommerce-loop-product__title">
                                                                            Áo dài hoạ tiết cánh quạt
                                                                        </h2>
                                                                    </a>
                                                                </li>
                                                                <li
                                                                    class="product type-product post-1763 status-publish instock product_cat-ao-dai has-post-thumbnail shipping-taxable product-type-simple"
                                                                >
                                                                    <a
                                                                        href="/product/ao-dai-hoa-tiet-chim-cong/"
                                                                        class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                                                        ><span class="et_shop_image"
                                                                            ><img
                                                                                width="300"
                                                                                height="300"
                                                                                src="/wp-content/uploads/2022/05/Ao-dai-hoa-tiet-chim-cong-300x300.jpg"
                                                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                alt=""
                                                                                loading="lazy" /><span
                                                                                class="et_overlay"
                                                                            ></span
                                                                        ></span>
                                                                        <h2 class="woocommerce-loop-product__title">
                                                                            Áo dài họa tiết chim công
                                                                        </h2>
                                                                    </a>
                                                                </li>
                                                                <li
                                                                    class="product type-product post-1765 status-publish instock product_cat-ao-dai has-post-thumbnail shipping-taxable product-type-simple"
                                                                >
                                                                    <a
                                                                        href="/product/ao-dai-hoa-tiet-da-beo/"
                                                                        class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                                                        ><span class="et_shop_image"
                                                                            ><img
                                                                                width="300"
                                                                                height="300"
                                                                                src="/wp-content/uploads/2022/05/Ao-dai-hoa-tiet-da-beo-300x300.jpg"
                                                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                alt=""
                                                                                loading="lazy" /><span
                                                                                class="et_overlay"
                                                                            ></span
                                                                        ></span>
                                                                        <h2 class="woocommerce-loop-product__title">
                                                                            Áo dài họa tiết da beo
                                                                        </h2>
                                                                    </a>
                                                                </li>
                                                                <li
                                                                    class="product type-product post-1767 status-publish last instock product_cat-ao-dai has-post-thumbnail shipping-taxable product-type-simple"
                                                                >
                                                                    <a
                                                                        href="/product/ao-dai-hoa-tiet-duoi-cong/"
                                                                        class="woocommerce-LoopProduct-link woocommerce-loop-product__link"
                                                                        ><span class="et_shop_image"
                                                                            ><img
                                                                                width="300"
                                                                                height="300"
                                                                                src="/wp-content/uploads/2022/05/Ao-dai-hoa-tiet-duoi-cong-300x300.jpg"
                                                                                class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail"
                                                                                alt=""
                                                                                loading="lazy" /><span
                                                                                class="et_overlay"
                                                                            ></span
                                                                        ></span>
                                                                        <h2 class="woocommerce-loop-product__title">
                                                                            Áo dài họa tiết đuôi công
                                                                        </h2>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                            <nav class="woocommerce-pagination">
                                                                <ul class="page-numbers">
                                                                    <li>
                                                                        <span
                                                                            aria-current="page"
                                                                            class="page-numbers current"
                                                                            >1</span
                                                                        >
                                                                    </li>
                                                                    <li>
                                                                        <a
                                                                            class="page-numbers"
                                                                            href="/store/?product-page=2"
                                                                            >2</a
                                                                        >
                                                                    </li>
                                                                    <li>
                                                                        <a
                                                                            class="page-numbers"
                                                                            href="/store/?product-page=3"
                                                                            >3</a
                                                                        >
                                                                    </li>
                                                                    <li>
                                                                        <a
                                                                            class="page-numbers"
                                                                            href="/store/?product-page=4"
                                                                            >4</a
                                                                        >
                                                                    </li>
                                                                    <li>
                                                                        <a
                                                                            class="next page-numbers"
                                                                            href="/store/?product-page=2"
                                                                            >&rarr;</a
                                                                        >
                                                                    </li>
                                                                </ul>
                                                            </nav>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- .et_pb_column -->
                                            </div>
                                            <!-- .et_pb_row -->
                                        </div>
                                        <!-- .et_pb_section -->
                                    </div>
                                    <!-- .et_builder_inner_content -->
                                </div>
                                <!-- .et-l -->
                            </div>
                            <!-- #et-boc -->
                        </div>
                        <!-- .entry-content -->
                    </article>
                    <!-- .et_pb_post -->
                </div>
                <!-- #main-content -->

                <span class="et_pb_scroll_top et-pb-icon"></span>

                <footer id="main-footer">
                    <div id="footer-bottom">
                        <div class="container clearfix">
                            <div id="footer-info">D&amp;D CLEANERS | Copyright © 2021. All Rights Reserved.</div>
                        </div>
                        <!-- .container -->
                    </div>
                </footer>
                <!-- #main-footer -->
            </div>
            <!-- #et-main-area -->
        </div>
        <!-- #page-container -->

        
        
        <script type="text/javascript" id="et-builder-modules-script-js-extra">
            /* <![CDATA[ */
            var et_frontend_scripts = { builderCssContainerPrefix: "#et-boc", builderCssLayoutPrefix: "#et-boc .et-l" };
            var et_pb_custom = {
                ajaxurl: "httpsstaging.dndcleaners.ca\/wp-admin\/admin-ajax.php",
                images_uri: "httpsstaging.dndcleaners.ca\/wp-content\/themes\/Divi\/images",
                builder_images_uri: "httpsstaging.dndcleaners.ca\/wp-content\/themes\/Divi\/includes\/builder\/images",
                et_frontend_nonce: "4e47d1f20d",
                subscription_failed: "Làm ơn, kiểm tra đồng dưới chắc chắn để các ông gõ đúng thông tin.",
                et_ab_log_nonce: "0f432c9a55",
                fill_message: "Xin vui lòng, điền vào các lĩnh vực sau đây:",
                contact_error_message: "Xin hãy sửa những lỗi sau:",
                invalid: "Không hợp lệ email",
                captcha: "Captcha",
                prev: "Trước",
                previous: "Trước",
                next: "Tiếp theo",
                wrong_captcha: "Bạn đã nhập sai số captcha.",
                wrong_checkbox: "Checkbox",
                ignore_waypoints: "no",
                is_divi_theme_used: "1",
                widget_search_selector: ".widget_search",
                ab_tests: [],
                is_ab_testing_active: "",
                page_id: "1186",
                unique_test_id: "",
                ab_bounce_rate: "5",
                is_cache_plugin_active: "no",
                is_shortcode_tracking: "",
                tinymce_uri: "",
            };
            var et_pb_box_shadow_elements = [];
            var et_pb_motion_elements = { desktop: [], tablet: [], phone: [] };
            var et_pb_sticky_elements = [];
            /* ]]> */
        </script>
        @include('public.script')
        @include('store.style')
    </body>

    <!--
Performance optimized by W3 Total Cache. Learn more: https://www.boldgrid.com/w3-total-cache/

Page Caching using disk: enhanced 

Served from: staging.dndcleaners.ca @ 2022-12-27 15:12:45 by W3 Total Cache
--></html>
