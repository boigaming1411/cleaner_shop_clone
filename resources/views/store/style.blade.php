<style id="et-builder-module-design-1186-cached-inline-styles">
            .et_pb_section_0.et_pb_section {
                padding-top: 0px;
                padding-bottom: 0px;
                margin-top: 0px;
                margin-bottom: 0px;
            }
            .et_pb_section_2.et_pb_section {
                padding-top: 0px;
                padding-bottom: 0px;
                margin-top: 0px;
                margin-bottom: 0px;
            }
            .et_pb_row_2.et_pb_row {
                padding-top: 0px !important;
                padding-bottom: 0px !important;
                margin-top: 0px !important;
                margin-bottom: 0px !important;
                padding-top: 0px;
                padding-bottom: 0px;
            }
            .et_pb_row_0.et_pb_row {
                padding-top: 0px !important;
                padding-bottom: 0px !important;
                margin-top: 0px !important;
                margin-bottom: 0px !important;
                padding-top: 0px;
                padding-bottom: 0px;
            }
            .et_pb_row_2,
            body #page-container .et-db #et-boc .et-l .et_pb_row_2.et_pb_row,
            body.et_pb_pagebuilder_layout.single #page-container #et-boc .et-l .et_pb_row_2.et_pb_row,
            body.et_pb_pagebuilder_layout.single.et_full_width_page
                #page-container
                #et-boc
                .et-l
                .et_pb_row_2.et_pb_row {
                width: 100%;
                max-width: 2560px;
            }
            .et_pb_row_0,
            body #page-container .et-db #et-boc .et-l .et_pb_row_0.et_pb_row,
            body.et_pb_pagebuilder_layout.single #page-container #et-boc .et-l .et_pb_row_0.et_pb_row,
            body.et_pb_pagebuilder_layout.single.et_full_width_page
                #page-container
                #et-boc
                .et-l
                .et_pb_row_0.et_pb_row {
                width: 100%;
                max-width: 2560px;
            }
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product h3,
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product h1,
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product h2,
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product h4,
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product h5,
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product h6 {
                font-size: 18px;
                color: #000000 !important;
                line-height: 1.5em;
                transition: color 300ms ease 0ms;
            }
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product h3,
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product h1,
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product h2,
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product h4,
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product h5,
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product h6 {
                font-size: 18px;
                color: #000000 !important;
                line-height: 1.5em;
                transition: color 300ms ease 0ms;
            }
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product h3:hover,
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product h1:hover,
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product h2:hover,
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product h4:hover,
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product h5:hover,
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product h6:hover,
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product h1.hover,
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product h2.hover,
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product h3.hover,
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product h4.hover,
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product h5.hover,
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product h6.hover {
                color: #0c71c3 !important;
            }
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product h3:hover,
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product h1:hover,
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product h2:hover,
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product h4:hover,
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product h5:hover,
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product h6:hover,
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product h1.hover,
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product h2.hover,
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product h3.hover,
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product h4.hover,
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product h5.hover,
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product h6.hover {
                color: #0c71c3 !important;
            }
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product .price,
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product .price .amount {
                font-weight: 700;
                font-size: 25px;
                color: #e02b20 !important;
            }
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product .price,
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product .price .amount {
                font-weight: 700;
                font-size: 25px;
                color: #e02b20 !important;
            }
            .et_pb_shop_1.et_pb_shop .woocommerce ul.products li.product .onsale {
                font-weight: 700 !important;
                text-transform: uppercase !important;
                font-size: 12px;
                line-height: 1em !important;
            }
            .et_pb_shop_0.et_pb_shop .woocommerce ul.products li.product .onsale {
                font-weight: 700 !important;
                text-transform: uppercase !important;
                font-size: 12px;
                line-height: 1em !important;
            }
            .et_pb_shop_1.et_pb_shop .et_shop_image > img,
            .et_pb_shop_1.et_pb_shop .et_shop_image .et_overlay {
                border-radius: 10px 10px 10px 10px;
                overflow: hidden;
            }
            .et_pb_shop_0.et_pb_shop .et_shop_image > img,
            .et_pb_shop_0.et_pb_shop .et_shop_image .et_overlay {
                border-radius: 10px 10px 10px 10px;
                overflow: hidden;
            }
            .et_pb_shop_0.et_pb_shop .et_shop_image > img {
                border-color: #f221a5 !important;
            }
            .et_pb_shop_1.et_pb_shop .et_shop_image > img {
                border-color: #f221a5 !important;
            }
            .et_pb_shop_0 span.onsale {
                background-color: #e02b20 !important;
            }
            .et_pb_shop_1 span.onsale {
                background-color: #e02b20 !important;
            }
            .et_pb_shop_0 .et_overlay:before {
                color: #ffffff !important;
            }
            .et_pb_shop_1 .et_overlay:before {
                color: #ffffff !important;
            }
            .et_pb_shop_0 .et_overlay {
                background-color: rgba(0, 0, 0, 0.3) !important;
                border-color: rgba(0, 0, 0, 0.3) !important;
            }
            .et_pb_shop_1 .et_overlay {
                background-color: rgba(0, 0, 0, 0.3) !important;
                border-color: rgba(0, 0, 0, 0.3) !important;
            }
            .et_pb_shop_0 ul.products li.product .star-rating {
                width: calc(5.4em + (0px * 4));
            }
            .et_pb_shop_1 ul.products li.product .star-rating {
                width: calc(5.4em + (0px * 4));
            }
        </style>